import { Obj } from './obj';

export class Spinner extends Obj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'Spinner';
  }
  public SetOnTouch(callback: any) {
    prompt(this.id, 'Spn.SetOnClick(' + _Cbm(callback));
  }
  public SetOnChange(callback: any) {
    prompt(this.id, 'Spn.SetOnClick(' + _Cbm(callback));
  }
  public SetText(txt: string) {
    prompt(this.id, 'Spn.SetText(' + txt);
  }
  public SelectItem(item: string) {
    prompt(this.id, 'Spn.SetText(' + item);
  }
  public GetText() {
    return prompt(this.id, 'Spn.GetText(');
  }
  public SetTextColor(clr: string) {
    prompt(this.id, 'Spn.SetTextColor(' + clr);
  }
  public SetTextSize(size: string, mode: string) {
    prompt(this.id, 'Spn.SetTextSize(\f' + size + '\f' + mode);
  }
  public GetTextSize(mode: string) {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Spn.GetTextSize(\f' + mode));
  }
  public SetList(list: string, delim: string) {
    prompt(this.id, 'Spn.SetList(\f' + list + '\f' + delim);
  }
  public AdjustColor(hue: string, sat: string, bright: string, cont: string) {
    prompt(
      this.id,
      'Spn.AdjustColor(\f' + hue + '\f' + sat + '\f' + bright + '\f' + cont
    );
  }
}
