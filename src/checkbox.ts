import { Obj } from './obj';

export class CheckBox extends Obj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'CheckBox';
  }
  public SetOnTouch(callback: any) {
    prompt(this.id, 'Chk.SetOnClick(' + _Cbm(callback));
  }
  public SetText(text: string) {
    prompt(this.id, 'Chk.SetText(' + text);
  }
  public GetText() {
    return prompt(this.id, 'Chk.GetText(');
  }
  public SetTextColor(clr: string) {
    prompt(this.id, 'Chk.SetTextColor(' + clr);
  }
  public SetTextSize(size: string, mode: string) {
    prompt(this.id, 'Chk.SetTextSize(\f' + size + '\f' + mode);
  }
  public GetTextSize(mode: string) {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Chk.GetTextSize(\f' + mode));
  }
  public SetChecked(checked: string) {
    prompt(this.id, 'Chk.SetChecked(' + checked);
  }
  public GetChecked() {
    return prompt(this.id, 'Chk.GetChecked(') === 'true';
  }
  public SetColorFilter(clr: string, mode: string) {
    prompt(this.id, 'Chk.SetColorFilter(\f' + clr + '\f' + mode);
  }
  public AdjustColor(hue: string, sat: string, bright: string, cont: string) {
    prompt(
      this.id,
      'Chk.AdjustColor(\f' + hue + '\f' + sat + '\f' + bright + '\f' + cont
    );
  }
}
