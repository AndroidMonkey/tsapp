import { SObj } from './sobj';

export class BluetoothList extends SObj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'BluetoothList';
  }
  public SetOnTouch(callback: any) {
    prompt(this.id, 'Btl.SetOnClick(' + _Cbm(callback));
  }
  public SetOnTouchEx(callback: string) {
    prompt(this.id, 'Btl.SetOnClick(' + callback);
  }
}
