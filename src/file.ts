import { SObj } from './sobj';

export class File extends SObj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'File';
  }
  public Close() {
    prompt(this.id, 'Fil.Close(');
  }
  public ReadText(type: string) {
    return prompt(this.id, 'Fil.ReadText(\f' + type);
  }
  public ReadTextAsync(type: string, callback: any) {
    return prompt(
      this.id,
      'Fil.ReadTextAsync(\f' + type + '\f' + _Cbm(callback)
    );
  }
  public WriteText(data: string, type: string) {
    prompt(this.id, 'Fil.WriteText(\f' + data + '\f' + type);
  }
  public ReadNumber(type: string) {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Fil.ReadNumber(\f' + type));
  }
  public WriteNumber(data: string, type: string) {
    prompt(this.id, 'Fil.WriteNumber(\f' + data + '\f' + type);
  }
  public ReadData(len: string, mode: string) {
    // @ts-ignore TS2345
    return JSON.parse(prompt(this.id, 'Fil.ReadData(\f' + len + '\f' + mode));
  }
  public WriteData(data: string, mode: string) {
    prompt(this.id, 'Fil.WriteData(\f' + data + '\f' + mode);
  }
  public Seek(offset: string) {
    prompt(this.id, 'Fil.Seek(\f' + offset);
  }
  public Skip(bytes: string) {
    prompt(this.id, 'Fil.Skip(\f' + bytes);
  }
  public GetPointer() {
    // @ts-ignore TS2345
    return parseInt(prompt(this.id, 'Fil.GetPointer('), 10);
  }
  public GetLength() {
    // @ts-ignore TS2345
    return parseInt(prompt(this.id, 'Fil.GetLength('), 10);
  }
  public SetLength(len: string) {
    prompt(this.id, 'Fil.SetLength(\f' + len);
  }
}
