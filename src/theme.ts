import { SObj } from './sobj';

export class Theme extends SObj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'Theme';
  }
  public AdjustColor(hue: string, sat: string, bright: string, cont: string) {
    prompt(
      this.id,
      'Thm.AdjustColor(\f' + hue + '\f' + sat + '\f' + bright + '\f' + cont
    );
  }
  public SetBackColor(clr: string) {
    prompt(this.id, 'Thm.SetBackColor(\f' + clr);
  }
  public SetBackground(file: string, options: string) {
    prompt(this.id, 'Thm.SetBackground(\f' + file + '\f' + options);
  }
  public SetTextColor(clr: string) {
    prompt(this.id, 'Thm.SetTextColor(\f' + clr);
  }
  public SetBtnTextColor(clr: string) {
    prompt(this.id, 'Thm.SetBtnTextColor(\f' + clr);
  }
  public SetButtonOptions(options: string) {
    prompt(this.id, 'Thm.SetButtonOptions(\f' + options);
  }
  public SetButtonStyle(
    clr1: string,
    clr2: string,
    radius: string,
    strokeClr: string,
    strokeWidth: string,
    shadow: string,
    checkClr: string
  ) {
    prompt(
      this.id,
      'Thm.SetButtonStyle(\f' +
        clr1 +
        '\f' +
        clr2 +
        '\f' +
        radius +
        '\f' +
        strokeClr +
        '\f' +
        strokeWidth +
        '\f' +
        shadow +
        '\f' +
        checkClr
    );
  }
  public SetCheckBoxOptions(options: string) {
    prompt(this.id, 'Thm.SetCheckBoxOptions(\f' + options);
  }
  public SetTextEditOptions(options: string) {
    prompt(this.id, 'Thm.SetTextEditOptions(\f' + options);
  }
  public SetProgressBarOptions(options: string) {
    prompt(this.id, 'Thm.SetProgressBarOptions(\f' + options);
  }
  public SetProgressBackColor(clr: string) {
    prompt(this.id, 'Thm.SetProgressBackColor(\f' + clr);
  }
  public SetProgressTextColor(clr: string) {
    prompt(this.id, 'Thm.SetProgressTextColor(\f' + clr);
  }
  public SetTitleHeight(height: string, options: string) {
    prompt(this.id, 'Thm.SetTitleHeight(\f' + height + '\f' + options);
  }
  public SetTitleColor(clr: string) {
    prompt(this.id, 'Thm.SetTitleColor(\f' + clr);
  }
  public SetTitleDividerColor(clr: string) {
    prompt(this.id, 'Thm.SetTitleDividerColor(\f' + clr);
  }
  public SetDialogColor(clr: string) {
    prompt(this.id, 'Thm.SetDialogColor(\f' + clr);
  }
  public SetDialogBtnColor(clr: string) {
    prompt(this.id, 'Thm.SetDialogBtnColor(\f' + clr);
  }
  public SetDialogBtnTxtColor(clr: string) {
    prompt(this.id, 'Thm.SetDialogBtnTxtColor(\f' + clr);
  }
  public SetDimBehind(dim: string) {
    prompt(this.id, 'Thm.SetDimBehind(\f' + dim);
  }
}
