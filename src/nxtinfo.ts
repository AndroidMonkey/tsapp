import { SObj } from './sobj';

export class NxtInfo extends SObj {
  constructor(id) {
    super(id);
  }
  GetType() {
    return 'NxtInfo';
  }
  GetName() {
    return prompt(this.id, 'Inf.GetName(');
  }
  GetHandle() {
    return parseInt(prompt(this.id, 'Inf.GetHandle('), 10);
  }
  GetSize() {
    return parseInt(prompt(this.id, 'Inf.GetSize('), 10);
  }
}
