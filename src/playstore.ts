import { SObj } from './sobj';

export class PlayStore extends SObj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'PlayStore';
  }
  public GetBillingInfo(prodIDs: string, callback: any, options: string) {
    ret = prompt(
      this.id,
      'Ply.GetBillingInfo(\f' + prodIDs + '\f' + _Cbm(callback) + '\f' + options
    );
  }
  public Purchase(
    prodID: string,
    token: string,
    callback: any,
    options: string
  ) {
    ret = prompt(
      this.id,
      'Ply.Purchase(\f' +
        prodID +
        '\f' +
        token +
        '\f' +
        _Cbm(callback) +
        '\f' +
        options
    );
  }
  public GetPurchases(callback: any, options: string) {
    ret = prompt(
      this.id,
      'Ply.GetPurchases(\f' + _Cbm(callback) + '\f' + options
    );
  }
}
