import { SObj } from './sobj';

export class SMS extends SObj {
  constructor(id: string) {
    super(id);
  }
  public GetType(): string {
    return 'SMS';
  }
  public Send(num: string, msg: string, options: string): void {
    prompt(this.id, 'SMS.Send(\f' + num + '\f' + msg + '\f' + options);
  }
  public SetOnStatus(callback: any): void {
    prompt(this.id, 'SMS.SetOnStatus(' + _Cbm(callback));
  }
  public SetOnMessage(callback: any): void {
    prompt(this.id, 'SMS.SetOnMessage(' + _Cbm(callback));
  }
}
