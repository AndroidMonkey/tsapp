import { SObj } from './sobj';

export class YesNoDialog extends SObj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'YesNoDialog';
  }
  public Show() {
    prompt(this.id, 'Ynd.Show(');
  }
  public Hide() {
    prompt(this.id, 'Ynd.Hide(');
  }
  public Dismiss() {
    prompt(this.id, 'Ynd.Dismiss(');
  }
  public SetOnTouch(callback: any) {
    prompt(this.id, 'Ynd.SetOnClick(' + _Cbm(callback));
  }
  public SetBackColor(clr: string) {
    prompt(this.id, 'Ynd.SetBackColor(\f' + clr);
  }
  public SetBackground(file: string, options: string) {
    prompt(this.id, 'Ynd.SetBackground(\f' + file + '\f' + options);
  }
  public SetSize(width: string, height: string, options: string) {
    prompt(this.id, 'Ynd.SetSize(\f' + width + '\f' + height + '\f' + options);
  }
  public AdjustColor(hue: string, sat: string, bright: string, cont: string) {
    prompt(
      this.id,
      'Ynd.AdjustColor(\f' + hue + '\f' + sat + '\f' + bright + '\f' + cont
    );
  }
  public SetButtonText(yes: string, no: string) {
    prompt(this.id, 'Ynd.SetButtonText(\f' + yes + '\f' + no);
  }
}
