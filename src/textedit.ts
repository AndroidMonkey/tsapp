import { Obj } from './obj';

export class TextEdit extends Obj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'TextEdit';
  }
  public SetText(txt: string) {
    prompt(this.id, 'Txe.SetText(' + txt);
  }
  public SetHtml(html: string) {
    prompt(this.id, 'Txe.SetHtml(' + html);
  }
  public GetHtml() {
    return prompt(this.id, 'Txe.GetHtml(');
  }
  public SetHint(text: string) {
    prompt(this.id, 'Txe.SetHint(' + text);
  }
  public InsertText(text: string, start: string) {
    prompt(this.id, 'Txe.InsertText(\f' + text + '\f' + start);
  }
  public ReplaceText(text: string, start: string, end: string) {
    prompt(this.id, 'Txe.ReplaceText(\f' + text + '\f' + start + '\f' + end);
  }
  public GetText() {
    return prompt(this.id, 'Txe.GetText(');
  }
  public SetOnChange(callback: any) {
    prompt(this.id, 'Txe.SetOnChange(\f' + _Cbm(callback));
  }
  public SetOnTouch(callback: any) {
    prompt(this.id, 'Txe.SetOnTouch(\f' + _Cbm(callback));
  }
  public SetOnEnter(callback: any) {
    prompt(this.id, 'Txe.SetOnEnter(\f' + _Cbm(callback));
  }
  public SetOnFocus(callback: any) {
    prompt(this.id, 'Txe.SetOnFocus(\f' + _Cbm(callback));
  }
  public SetTextColor(color: string) {
    prompt(this.id, 'Txe.SetTextColor(' + color);
  }
  public SetTextSize(size: string, mode: string) {
    prompt(this.id, 'Txe.SetTextSize(\f' + size + '\f' + mode);
  }
  public GetTextSize(mode: string) {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Txe.GetTextSize(\f' + mode));
  }
  public GetLineCount() {
    // @ts-ignore TS2345
    return parseInt(prompt(this.id, 'Txe.GetLineCount('), 10);
  }
  public GetMaxLines() {
    // @ts-ignore TS2345
    return parseInt(prompt(this.id, 'Txe.GetMaxLines('), 10);
  }
  public GetLineTop(line: string) {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Txe.GetLineTop(' + line));
  }
  public GetLineStart(line: string) {
    // @ts-ignore TS2345
    return parseInt(prompt(this.id, 'Txe.GetLineStart(' + line), 10);
  }
  public SetCursorColor(color: string) {
    prompt(this.id, 'Txe.SetCursorColor(\f' + color);
  }
  public SetCursorPos(pos: string) {
    prompt(this.id, 'Txe.SetCursorPos(' + pos);
  }
  public GetCursorPos() {
    // @ts-ignore TS2345
    return parseInt(prompt(this.id, 'Txe.GetCursorPos('), 10);
  }
  public GetCursorLine() {
    // @ts-ignore TS2345
    return parseInt(prompt(this.id, 'Txe.GetCursorLine('), 10);
  }
  public SetSelection(start: string, stop: string) {
    prompt(this.id, 'Txe.SetSelection(\f' + start + '\f' + stop);
  }
  public GetSelectedText() {
    return prompt(this.id, 'Txe.GetSelectedText(');
  }
  public GetSelectionStart() {
    // @ts-ignore TS2345
    return parseInt(prompt(this.id, 'Txe.GetSelectionStart('), 10);
  }
  public GetSelectionEnd() {
    // @ts-ignore TS2345
    return parseInt(prompt(this.id, 'Txe.GetSelectionEnd('), 10);
  }
  public Undo() {
    prompt(this.id, 'Txe.Undo(');
  }
  public Redo() {
    prompt(this.id, 'Txe.Redo(');
  }
  public ClearHistory() {
    prompt(this.id, 'Txe.ClearHistory(');
  }
}
