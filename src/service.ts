import { SObj } from './sobj';

export class Service extends SObj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'Service';
  }
  public Stop() {
    prompt(this.id, 'Svc.Stop(');
  }
  public _Send(
    cmd: string,
    p1: string,
    p2: string,
    p3: string,
    p4: string,
    p5: string,
    p6: string,
    p7: string
  ) {
    prompt(
      this.id,
      'Svc.Send(\f' +
        cmd +
        '\f' +
        typeof p1 +
        '\f' +
        p1 +
        '\f' +
        typeof p2 +
        '\f' +
        p2 +
        '\f' +
        typeof p3 +
        '\f' +
        p3 +
        '\f' +
        typeof p4 +
        '\f' +
        p4 +
        '\f' +
        typeof p5 +
        '\f' +
        p5 +
        '\f' +
        typeof p6 +
        '\f' +
        p6 +
        '\f' +
        typeof p7 +
        '\f' +
        p7
    );
  }
  public _SendImg(cmd: string, img: { id: any }) {
    prompt(this.id, 'Svc.SendImg(\f' + cmd + '\f' + (img ? img.id : null));
  }
  public SendMessage(msg: string) {
    prompt(this.id, 'Svc.SendMsg(\f' + msg);
  }
  public SetOnMessage(callback: any) {
    prompt(this.id, 'Svc.SetOnMessage(\f' + _Cbm(callback));
  }
  public SetInForeground(
    title: string,
    text: string,
    largeIcon: string,
    smallIcon: string,
    importance: string
  ) {
    prompt(
      this.id,
      'Svc.SetInForeground(\f' +
        title +
        '\f' +
        text +
        '\f' +
        largeIcon +
        '\f' +
        smallIcon +
        '\f' +
        importance
    );
  }
  public SetInBackground() {
    prompt(this.id, 'Svc.SendMsg(\f' + '_background');
  }
}
