import { SObj } from './sobj';

export class Wallpaper extends SObj {
  constructor(id: string) {
    super(id);
  }
  public GetType(): string {
    return 'Wallpaper';
  }
  public IsVisible(): boolean {
    return prompt(this.id, 'Wpr.IsVisible\f') === 'true';
  }
}
