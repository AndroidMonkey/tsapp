export function _WebSock(
  id: string,
  ip: string,
  port: number = 8080,
  options?: any
) {
  let m_OnMessage: ((arg0: any) => void) | null = null;
  let m_sock: WebSocket | null = null;
  let m_timer = null;
  let m_OnOpen: ((arg0: any) => void) | null = null;
  let m_OnClose: ((arg0: any) => void) | null = null;
  let m_IsOpen: boolean | null = null;

  console.log('Opening web socket:' + id);
  m_sock = new WebSocket('ws://' + ip + ':' + port);
  m_sock.onopen = OnOpen;
  m_sock.onmessage = OnMessage;
  m_sock.onclose = OnClose;
  m_sock.onerror = OnError;
  m_timer = setInterval(CheckSocket, 7000);

  function OnOpen() {
    console.log('Socket Open: ' + id);
    if (m_OnOpen) {
      m_OnOpen(id);
    }
    m_IsOpen = true;
  }

  function CheckSocket() {
    if (m_sock && m_sock.readyState !== 1) {
      console.log('Re-opening web socket:' + id);
      m_sock = new WebSocket('ws://' + ip + ':' + port);
    }
  }

  function OnClose() {
    console.log('Socket Closed: ' + id);
    if (m_OnClose) {
      m_OnClose(id);
    }
    m_IsOpen = false;
  }

  function OnError(e: any) {
    console.log('Socket Error: ' + e.data);
  }
  function OnMessage(msg: { data: any }) {
    if (m_OnMessage) {
      m_OnMessage(msg.data);
    }
  }

  this.Close = () => {
    m_sock.close();
  };
  this.GetSocket = () => m_sock;
  this.SetOnMessage = (callback: any) => {
    m_OnMessage = callback;
  };

  this.SetOnOpen = (callback: any) => {
    m_OnOpen = callback;
  };
  this.SetOnClose = (callback: any) => {
    m_OnClose = callback;
  };
  this.IsOpen = () => m_IsOpen;

  this.Send = (msg: any) => {
    if (m_sock.readyState !== 1) {
      console.log('Socket not ready:' + m_sock);
    } else {
      m_sock.send(msg);
    }
  };
}
