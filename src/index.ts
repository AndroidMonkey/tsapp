import { AudioRecorder as Rec } from './audiorecorder';
import { BluetoothList as Btl } from './bluetoothlist';
import { BluetoothSerial as Bts } from './bluetoothserial';
import { Button as Btn } from './button';
import { CameraView as Cam } from './cameraview';
import { CheckBox as Chk } from './checkbox';
import { CodeEdit as Cde } from './codeedit';
import { cordova } from './cordova';
import { Crypt as Crp } from './crypt';
import { Dialog as Dlg } from './dialog';
import { Downloader as Dwn } from './downloader';
import { EMAIL } from './email';
import { File as Fil } from './file';
import { Image as Img } from './image';
import { Layout as Lay } from './layout';
import { List as Lst } from './list';
import { ListDialog as Ldg } from './listdialog';
import { ListView as Lvw } from './listview';
import { Locator as Loc } from './locator';
import { MediaPlayer as Aud } from './mediaplayer';
import { MediaStore as Med } from './mediastore';
import { NetClient as Net } from './netclient';
import { Notification as Not } from './notification';
// import { NxtInfo as Inf } from './nxtinfo';
import { Overlay as Ovl } from './overlay';
import { PhoneState as Pst } from './phonestate';
import { PlayStore as Ply } from './playstore';
import { Scroller as Scr } from './scroller';
import { SeekBar as Skb } from './seekbar';
import { Sensor as Sns } from './sensor';
import { Service as Svc } from './service';
import { SMS } from './sms';
import { SObj } from './sobj';
import { SpeechRec as Spr } from './speechrec';
import { Spinner as Spn } from './spinner';
import { Synth as Syn } from './synth';
import { SysProc as Sys } from './sysproc';
import { Text as Txt } from './text';
import { TextEdit as Txe } from './textedit';
import { Theme as Thm } from './theme';
import { Toggle as Tgl } from './toggle';
import { USBSerial as Usb } from './usbserial';
import { VideoView as Vid } from './videoview';
import { Wallpaper as Wpr } from './wallpaper';
import { WebServer as Wbs } from './webserver';
import { WebView as Web } from './webview';
import { YesNoDialog as Ynd } from './yesnodialog';
import { ZipUtil as Zip } from './ziputil';

_dbg = true;
_map = [];
_scripts = [];
_languages = null;
_curLang = 'en';
_started = false;
_cbMap = [];
_cbId = 0;
_docs = false;
_busy = false;
_isV8 = false;
_btl = null;
_lvw = null;
_ldg = null;
_ynd = null;
_nxt = null;
_smw = null;
_inf = null;
_rec = null;
_pst = null;
_sms = null;
_eml = null;
_wpr = null;
_crp = null;
_spr = null;

export class App {
  private _anim_t = 0;
  public GetType(): string {
    return 'App';
  }
  public GetObjects() {
    return _map;
  }
  public IsStarted(): boolean {
    return _started;
  }
  public Exit(kill: string): void {
    prompt('#', 'App.Exit(' + kill);
  }
  public ToBack(): void {
    prompt('#', 'App.ToBack(');
  }
  public ToFront(): void {
    prompt('#', 'App.ToFront(');
  }
  public Execute(js: string): void {
    prompt('#', 'App.Execute(' + js);
  }
  public StartApp(file: string, options: string, intent: string): void {
    prompt('#', 'App.StartApp(\f' + file + '\f' + options + '\f' + intent);
  }
  public StopApp(name: string): void {
    prompt('#', 'App.StopApp(' + name);
  }
  public SetScreenMode(mode: string): void {
    prompt('#', 'App.SetScreenMode(\f' + mode);
  }
  public SetOptions(options: string) {
    prompt('#', 'App.SetOptions(\f' + options);
  }
  public SetTheme(theme: { id: any }) {
    prompt('#', 'App.SetTheme(\f' + (theme ? theme.id : null));
  }
  public SetDensity(dpi: string) {
    prompt('#', 'App.SetDensity(\f' + dpi);
  }
  public SetPosition(
    left: string,
    top: string,
    width: string,
    height: string,
    options: string
  ) {
    prompt(
      '#',
      'App.SetPosition(\f' +
        left +
        '\f' +
        top +
        '\f' +
        width +
        '\f' +
        height +
        '\f' +
        options
    );
  }
  public SetBackColor(clr: string) {
    prompt('#', 'App.SetBackColor(\f' + clr);
  }
  public StartService(packageName: string, className: string) {
    prompt('#', 'App.StartService(\f' + packageName + '\f' + className);
  }
  public StopService() {
    prompt('#', 'App.StopService(');
  }
  public StartDebugServer() {
    prompt('#', 'App.StartDebugServer(');
  }
  public StopDebugServer() {
    prompt('#', 'App.StopDebugServer(');
  }
  public SendIntent(
    packageName: string,
    className: string,
    action: string,
    category: string,
    uri: string,
    type: string,
    extras: string,
    options: string,
    callback: any
  ) {
    prompt(
      '#',
      'App.SendIntent(\f' +
        packageName +
        '\f' +
        className +
        '\f' +
        action +
        '\f' +
        category +
        '\f' +
        uri +
        '\f' +
        type +
        '\f' +
        extras +
        '\f' +
        options +
        '\f' +
        _Cbm(callback)
    );
  }
  public BroadcastIntent(
    action: string,
    category: string,
    data: string,
    type: string,
    extras: string,
    options: string
  ) {
    prompt(
      '#',
      'App.BroadcastIntent(\f' +
        action +
        '\f' +
        category +
        '\f' +
        data +
        '\f' +
        type +
        '\f' +
        extras +
        '\f' +
        options
    );
  }
  public SendMessage(msg: string) {
    prompt('#', 'App.SendMessage(\f' + msg);
  }
  public SetInForeground(
    title: string,
    text: string,
    largeIcon: string,
    smallIcon: string,
    importance: string
  ) {
    prompt(
      '#',
      'App.SetInForeground(\f' +
        title +
        '\f' +
        text +
        '\f' +
        largeIcon +
        '\f' +
        smallIcon +
        '\f' +
        importance
    );
  }
  public SetInBackground() {
    prompt('#', 'App.SetInBackground(\f');
  }
  public Script(file: any) {
    _LoadScriptSync(file);
  }
  public LoadScript(url: any, callback: any) {
    _LoadScript(url, callback);
  }
  public LoadPlugin(url: any) {
    _LoadPlugin(url);
  }
  public SysExec(
    cmd: string,
    options: string,
    maxRead: string,
    timeout: string
  ) {
    return prompt(
      '#',
      'App.SysExec(\f' + cmd + '\f' + options + '\f' + maxRead + '\f' + timeout
    );
  }
  public ExtExec(name: string, file: string, args: string, options: string) {
    return prompt(
      '#',
      'App.ExtExec(\f' + name + '\f' + file + '\f' + args + '\f' + options
    );
  }
  public SetPriority(level: string) {
    prompt('#', 'App.SetPriority(\f' + level);
  }
  public Odroid(p1: string, p2: string, p3: string) {
    return prompt('#', 'App.Odroid(\f' + p1 + '\f' + p2 + '\f' + p3);
  }
  public GetName() {
    return prompt('#', 'App.GetName(');
  }
  public GetPath() {
    return prompt('#', 'App.GetPath(');
  }
  public GetAppName() {
    return prompt('#', 'App.GetAppName(');
  }
  public GetAppPath() {
    return prompt('#', 'App.GetAppPath(');
  }
  public GetVersion() {
    // @ts-ignore TS2345
    return parseFloat(prompt('#', 'App.GetVersion('));
  }
  public GetDSVersion() {
    // @ts-ignore TS2345
    return parseFloat(prompt('#', 'App.GetDSVersion('));
  }
  public IsNewVersion() {
    return prompt('#', 'App.IsNewVersion(') === 'true';
  }
  public InIDE() {
    return prompt('#', 'App.InIDE(') === 'true';
  }
  public IsAPK() {
    return prompt('#', 'App.IsAPK(') === 'true';
  }
  public IsService() {
    return prompt('#', 'App.IsService(') === 'true';
  }
  public IsPremium() {
    return prompt('#', 'App.IsPremium(') === 'true';
  }
  public IsEngine() {
    return prompt('#', 'App.IsEngine(') === 'true';
  }
  public GetPackageName() {
    return prompt('#', 'App.GetPackageName(');
  }
  public CheckLicense(key: string) {
    prompt('#', 'App.CheckLicense(\f' + key);
  }
  public GetAccounts() {
    return prompt('#', 'App.GetAccounts(');
  }
  public GetUser() {
    return prompt('#', 'App.GetUser(');
  }
  public GetDeviceId() {
    return prompt('#', 'App.GetDeviceId(');
  }
  public GetCountryCode() {
    return prompt('#', 'App.GetCountryCode(');
  }
  public GetLanguageCode() {
    return prompt('#', 'App.GetLanguageCode(');
  }
  public GetCountry() {
    return prompt('#', 'App.GetCountry(');
  }
  public GetLanguage() {
    return prompt('#', 'App.GetLanguage(');
  }
  public GetOptions() {
    return prompt('#', 'App.GetOptions(');
  }
  public GetSharedText(index: string) {
    return prompt('#', 'App.GetSharedText(' + index);
  }
  public GetSharedFiles() {
    const s = prompt('#', 'App.GetSharedFiles(');
    if (s && s.length) {
      return s.split(',');
    } else {
      return null;
    }
  }
  public GetActivities(options: string) {
    // @ts-ignore TS2345
    return JSON.parse(prompt('#', 'App.GetActivities(\f' + options));
  }
  public IsAppInstalled(packageName: string) {
    return prompt('#', 'App.IsAppInstalled(\f' + packageName) === 'true';
  }
  public GetInstalledApps() {
    // @ts-ignore TS2345
    return JSON.parse(prompt('#', 'App.GetInstalledApps(\f'));
  }
  public GetRunningApps() {
    // @ts-ignore TS2345
    return JSON.parse(prompt('#', 'App.GetRunningApps(\f'));
  }
  public GetRunningServices() {
    // @ts-ignore TS2345
    return JSON.parse(prompt('#', 'App.GetRunningServices(\f'));
  }
  public GetMemoryInfo() {
    // @ts-ignore TS2345
    return JSON.parse(prompt('#', 'App.GetMemoryInfo(\f'));
  }
  public GetIntent() {
    const s = prompt('#', 'App.GetIntent(');
    if (s && s.length) {
      return JSON.parse(s);
    } else {
      return null;
    }
  }
  public GetNotifyId() {
    return prompt('#', 'App.GetNotifyId(');
  }
  public SetSharedApp(name: string) {
    prompt('#', 'App.SetSharedApp(' + name);
  }
  public GetMediaFile(appName: string, ext: string) {
    return prompt('#', 'App.GetMediaFile(\f' + appName + '\f' + ext);
  }
  public KillApp(procId: string) {
    prompt('#', 'App.KillApp(' + procId);
  }
  public CreateShortcut(
    name: string,
    iconFile: string,
    file: string,
    options: string
  ) {
    prompt(
      '#',
      'App.CreateShortcut(\f' +
        name +
        '\f' +
        iconFile +
        '\f' +
        file +
        '\f' +
        options
    );
  }
  public GetBuildNum() {
    // @ts-ignore TS2345
    return parseInt(prompt('#', 'App.GetBuildNum('), 10);
  }
  public GetOSVersion() {
    // @ts-ignore TS2345
    return parseInt(prompt('#', 'App.GetBuildNum('), 10);
  }
  public GetModel() {
    return prompt('#', 'App.GetModel(');
  }
  public IsTablet() {
    return prompt('#', 'App.IsTablet(') === 'true';
  }
  public IsChrome() {
    return prompt('#', 'App.IsChrome(') === 'true';
  }
  public IsThings() {
    return prompt('#', 'App.IsThings(') === 'true';
  }
  public SetOnError(callback: any) {
    prompt('#', 'App.SetOnError(\f' + _Cbm(callback));
  }
  public SetOnDebug(callback: any) {
    prompt('#', 'App.SetOnDebug(\f' + _Cbm(callback));
  }
  public SetOnKey(callback: any) {
    prompt('#', 'App.SetOnKey(\f' + _Cbm(callback));
  }
  public SetOnShowKeyboard(callback: any) {
    prompt('#', 'App.SetOnShowKeyboard(\f' + _Cbm(callback));
  }
  public SetOnWifiChange(callback: any) {
    prompt('#', 'App.SetOnWifiChange(\f' + _Cbm(callback));
  }
  public WifiScan(callback: any) {
    prompt('#', 'App.WifiScan(\f' + _Cbm(callback));
  }
  public DisableKeys(keyList: string) {
    prompt('#', 'App.DisableKeys(\f' + keyList);
  }
  public DisableTouch(disable: string) {
    prompt('#', 'App.DisableTouch(\f' + disable);
  }
  public GetIPAddress() {
    return prompt('#', 'App.GetIPAddress(');
  }
  public GetMacAddress() {
    return prompt('#', 'App.GetMacAddress(');
  }
  public GetSSID() {
    return prompt('#', 'App.GetSSID(');
  }
  public GetRSSI() {
    // @ts-ignore TS2345
    return parseInt(prompt('#', 'App.GetRSSI('), 10);
  }
  public Broadcast(type: string, msg: string) {
    prompt('#', 'App.Broadcast(' + type + '\f' + msg);
  }
  public SetOnBroadcast(callback: any) {
    prompt('#', 'App.SetOnBroadcast(' + _Cbm(callback));
  }
  public SetData(name: string, value: string) {
    prompt('#', 'App.SetData(\f' + name + '\f' + value);
  }
  public GetData(name: string) {
    return prompt('#', 'App.GetData(\f' + name);
  }
  public SetClipboardText(txt: string) {
    prompt('#', 'App.SetClipboardText(' + txt);
  }
  public GetClipboardText() {
    return prompt('#', 'App.GetClipboardText(');
  }
  public EnableBackKey(enable: string) {
    prompt('#', 'App.EnableBackKey(' + enable);
  }
  public Wait(secs: string) {
    prompt('#', 'App.Wait(' + secs);
  }
  public Alert(msg: string, title: string, options: string, hue: string) {
    prompt(
      '#',
      'App.Alert(\f' + msg + '\f' + title + '\f' + options + '\f' + hue
    );
  }
  public HideKeyboard(hide: string) {
    prompt('#', 'App.HideKeyboard(' + hide);
  }
  public ShowKeyboard(obj: { id: string }) {
    return prompt('#', 'App.ShowKeyboard(\f' + obj.id) === 'true';
  }
  public IsKeyboardShown() {
    return prompt('#', 'App.IsKeyboardShown(') === 'true';
  }
  public GetKeyboardHeight() {
    return prompt('#', 'App.GetKeyboardHeight(');
  }
  public TextToSpeech(
    text: string,
    pitch: string,
    rate: string,
    callback: any,
    stream: string,
    locale: string,
    engine: string
  ) {
    prompt(
      '#',
      'App.TextToSpeech(\f' +
        text +
        '\f' +
        pitch +
        '\f' +
        rate +
        '\f' +
        _Cbm(callback) +
        '\f' +
        stream +
        '\f' +
        locale +
        '\f' +
        engine
    );
  }
  public Debug(msg: string) {
    prompt('#', 'App.Debug(\f' + msg);
  }
  public Error(msg: string, line: string, file: string, quit: string) {
    prompt('#', 'App.Error(\f' + msg + '\f' + line + '\f' + file + '\f' + quit);
  }
  public SetDebugEnabled(enable: boolean) {
    prompt('#', 'App.SetDebugEnabled(' + enable);
    _dbg = enable;
  }
  public IsDebugEnabled() {
    return _dbg;
  }
  public CreateDebug() {
    prompt('#', 'App.CreateDebug(');
  }
  public ShowDebug(show: string) {
    prompt('#', 'App.ShowDebug(' + show);
  }
  public SendMail(
    address: string,
    subject: string,
    body: string,
    attach: string,
    type: string,
    options: string
  ) {
    prompt(
      '#',
      'App.SendMail(\f' +
        address +
        '\f' +
        subject +
        '\f' +
        body +
        '\f' +
        attach +
        '\f' +
        type +
        '\f' +
        options
    );
  }
  public SendFile(file: string, subject: string, text: string, choose: string) {
    prompt(
      '#',
      'App.SendFile(\f' + file + '\f' + subject + '\f' + text + '\f' + choose
    );
  }
  public SendText(text: string, subject: string, choose: string) {
    prompt('#', 'App.SendText(\f' + text + '\f' + subject + '\f' + choose);
  }
  // this._Extract ( p1 ) { prompt( "#", "App._Extract("+p1 ); }
  public ExtractAssets(src: string, dest: string, overwrite: string) {
    prompt('#', 'App.ExtractAssets(\f' + src + '\f' + dest + '\f' + overwrite);
  }
  public ExtractPlugins() {
    prompt('#', 'App.ExtractPlugins(\f');
  }
  public GetResourceId(name: string, options: string) {
    return parseInt(
      // @ts-ignore TS2345
      prompt('#', 'App.GetResourceId(\f' + name + '\f' + options),
      10
    );
  }
  public Vibrate(pattern: string) {
    prompt('#', 'App.Vibrate(' + pattern);
  }
  public ShowPopup(msg: string, options: string) {
    prompt('#', 'App.ShowPopup(' + msg + '\f' + options);
  }
  public ShowProgress(msg: string, options: string) {
    prompt('#', 'App.ShowProgress(\f' + msg + '\f' + options);
  }
  public HideProgress() {
    prompt('#', 'App.HideProgress(');
  }
  public ShowProgressBar(title: string, percent: string, options: string) {
    prompt(
      '#',
      'App.ShowProgressBar(\f' + title + '\f' + percent + '\f' + options
    );
  }
  public UpdateProgressBar(percent: string) {
    prompt('#', 'App.UpdateProgressBar(\f' + percent);
  }
  public HideProgressBar() {
    prompt('#', 'App.HideProgressBar(');
  }
  public LoadText(name: string, dflt: string, file: string) {
    return prompt('#', 'App.LoadText(' + name + '\f' + dflt + '\f' + file);
  }
  public LoadNumber(name: string, dflt: string, file: string) {
    return parseFloat(
      // @ts-ignore TS2345
      prompt('#', 'App.LoadNumber(' + name + '\f' + dflt + '\f' + file)
    );
  }
  public LoadBoolean(name: string, dflt: string, file: string) {
    return (
      prompt('#', 'App.LoadBoolean(' + name + '\f' + dflt + '\f' + file) ===
      'true'
    );
  }
  public SaveText(name: string, value: string, file: string) {
    prompt('#', 'App.SaveText(' + name + '\f' + value + '\f' + file);
  }
  public SaveNumber(name: string, value: string, file: string) {
    prompt('#', 'App.SaveNumber(' + name + '\f' + value + '\f' + file);
  }
  public SaveBoolean(name: string, value: string, file: string) {
    prompt('#', 'App.SaveBoolean(' + name + '\f' + value + '\f' + file);
  }
  public ClearData(file: string) {
    prompt('#', 'App.ClearData(\f' + file);
  }
  public ClearValue(name: string, file: string) {
    prompt('#', 'App.ClearValue(\f' + name + '\f' + file);
  }
  public GetTop() {
    // @ts-ignore TS2345
    return parseFloat(prompt('#', 'App.GetTop('));
  }
  public HasSoftNav() {
    return prompt('#', 'App.HasSoftNav(') === 'true';
  }
  public GetScreenWidth(options: string) {
    // @ts-ignore TS2345
    return parseFloat(prompt('#', 'App.GetScreenWidth(\f' + options));
  }
  public GetScreenHeight(options: string) {
    // @ts-ignore TS2345
    return parseFloat(prompt('#', 'App.GetScreenHeight(\f' + options));
  }
  public GetScreenDensity() {
    // @ts-ignore TS2345
    return parseFloat(prompt('#', 'App.GetScreenDensity('));
  }
  public GetDisplayWidth() {
    // @ts-ignore TS2345
    return parseFloat(prompt('#', 'App.GetDisplayWidth('));
  }
  public GetDisplayHeight() {
    // @ts-ignore TS2345
    return parseFloat(prompt('#', 'App.GetDisplayHeight('));
  }
  public GetDefaultOrientation() {
    return prompt('#', 'App.GetDefaultOrientation(');
  }
  public GetOrientation() {
    return prompt('#', 'App.GetOrientation(');
  }
  public SetOrientation(orient: string, callback: any) {
    prompt('#', 'App.SetOrientation(\f' + orient + '\f' + _Cbm(callback));
  }
  public GetRotation() {
    // @ts-ignore TS2345
    return parseInt(prompt('#', 'App.GetRotation('), 10);
  }
  public GetBatteryLevel() {
    // @ts-ignore TS2345
    return parseFloat(prompt('#', 'App.GetBatteryLevel(\f'));
  }
  public IsCharging() {
    return prompt('#', 'App.IsCharging(') === 'true';
  }
  public GetChargeType() {
    return prompt('#', 'App.GetChargeType(\f');
  }
  public PreventScreenLock(mode: string) {
    prompt('#', 'App.PreventScreenLock(' + mode);
  }
  public PreventWifiSleep() {
    prompt('#', 'App.PreventWifiSleep(');
  }
  public SetWifiEnabled(enable: string) {
    prompt('#', 'App.SetWifiEnabled(\f' + enable);
  }
  public IsWifiEnabled() {
    return prompt('#', 'App.IsWifiEnabled(') === 'true';
  }
  public SetWifiApEnabled(enable: string, ssid: string, key: string) {
    prompt('#', 'App.SetWifiApEnabled(\f' + enable + '\f' + ssid + '\f' + key);
  }
  public IsWifiApEnabled() {
    return prompt('#', 'App.IsWifiApEnabled(') === 'true';
  }
  public WifiConnect(ssid: string, key: string) {
    prompt('#', 'App.WifiConnect(\f' + ssid + '\f' + key);
  }
  public IsConnected() {
    return prompt('#', 'App.IsConnected(') === 'true';
  }
  public SetBluetoothEnabled(enable: string) {
    prompt('#', 'App.SetBluetoothEnabled(\f' + enable);
  }
  public IsBluetoothEnabled() {
    return prompt('#', 'App.IsBluetoothEnabled(') === 'true';
  }
  public GetPairedBtDevices() {
    // @ts-ignore TS2345
    return JSON.parse(prompt('#', 'App.GetPairedBTDevices(\f'));
  }
  public IsBtDevicePaired(name: string) {
    return prompt('#', 'App.IsBtDevicePaired(\f' + name) === 'true';
  }
  public DiscoverBtDevices(filter: string, onFound: any, onComplete: any) {
    prompt(
      '#',
      'App.DiscoverBtDevices(\f' +
        filter +
        '\f' +
        _Cbm(onFound) +
        '\f' +
        _Cbm(onComplete)
    );
  }
  public PairBtDevice(address: string, callback: any) {
    prompt('#', 'App.PairBtDevice(\f' + address + '\f' + _Cbm(callback));
  }
  public UnpairBtDevice(address: string, callback: any) {
    prompt('#', 'App.UnpairBtDevice(\f' + address + '\f' + _Cbm(callback));
  }
  public GetBtProfileState(type: string) {
    return prompt('#', 'App.GetBtProfileState(\f' + type);
  }
  public GetBluetoothName() {
    return prompt('#', 'App.GetBluetoothName(');
  }
  public GetBluetoothAddress() {
    return prompt('#', 'App.GetBluetoothAddress(');
  }
  public IsLocationEnabled(types: string) {
    return prompt('#', 'App.IsLocationEnabled(\f' + types) === 'true';
  }
  public PlayRingtone(type: string) {
    prompt('#', 'App.PlayRingtone(\f' + type);
  }
  public SetRingerMode(mode: string) {
    prompt('#', 'App.SetRingerMode(\f' + mode);
  }
  public GetRingerMode() {
    return prompt('#', 'App.GetRingerMode(');
  }
  public SetSpeakerPhone(on: string) {
    prompt('#', 'App.SetSpeakerPhone(\f' + on);
  }
  public GetSpeakerPhone() {
    return prompt('#', 'App.GetSpeakerPhone(') === 'true';
  }
  public SetVolume(stream: string, level: string, options: string) {
    prompt('#', 'App.SetVolume(\f' + stream + '\f' + level + '\f' + options);
  }
  public GetVolume(stream: string) {
    // @ts-ignore TS2345
    return parseFloat(prompt('#', 'App.GetVolume(\f' + stream));
  }
  public SetTitle(title: string) {
    prompt('#', 'App.SetTitle(' + title);
  }
  public SetMenu(list: string, iconPath: string) {
    prompt('#', 'App.SetMenu(' + list + '\f' + iconPath);
  }
  public ShowMenu() {
    prompt('#', 'App.ShowMenu(');
  }
  public AddLayout(layout: { id: string }) {
    prompt('#', 'App.AddLayout(' + layout.id);
  }
  public RemoveLayout(layout: { id: string }) {
    prompt('#', 'App.RemoveLayout(' + layout.id);
  }
  public DestroyLayout(layout: { id: string }) {
    prompt('#', 'App.DestroyLayout(' + layout.id);
  }
  public AddDrawer(
    layout: { id: string },
    side: string,
    width: string,
    grabWidth: string
  ) {
    prompt(
      '#',
      'App.AddDrawer(\f' +
        layout.id +
        '\f' +
        side +
        '\f' +
        width +
        '\f' +
        grabWidth
    );
  }
  public RemoveDrawer(side: string) {
    prompt('#', 'App.RemoveDrawer(\f' + side);
  }
  public OpenDrawer(side: string) {
    prompt('#', 'App.OpenDrawer(\f' + side);
  }
  public CloseDrawer(side: string) {
    prompt('#', 'App.CloseDrawer(\f' + side);
  }
  public LockDrawer(side: string) {
    prompt('#', 'App.LockDrawer(\f' + side);
  }
  public UnlockDrawer(side: string) {
    prompt('#', 'App.UnlockDrawer(\f' + side);
  }
  public GetDrawerState(side: string) {
    return prompt('#', 'App.GetDrawerState(\f' + side);
  }
  public MakeFolder(fldr: string) {
    prompt('#', 'App.MakeFolder(' + fldr);
  }
  public GetPrivateFolder(name: string) {
    return prompt('#', 'App.GetPrivateFolder(\f' + name);
  }
  public GetDatabaseFolder() {
    return prompt('#', 'App.GetDatabaseFolder(');
  }
  public DeleteDatabase(name: string) {
    prompt('#', 'App.DeleteDatabase(\f' + name);
  }
  public FolderExists(fldr: string) {
    return prompt('#', 'App.FolderExists(' + fldr) === 'true';
  }
  public FileExists(file: string) {
    return prompt('#', 'App.FileExists(' + file) === 'true';
  }
  public IsFolder(fldr: string) {
    return prompt('#', 'App.IsFolder(' + fldr) === 'true';
  }
  public ListFolder(
    path: string,
    filter: string,
    limit: string,
    options: string
  ) {
    return JSON.parse(
      // @ts-ignore TS2345
      prompt(
        '#',
        'App.ListFolder(\f' +
          path +
          '\f' +
          filter +
          '\f' +
          limit +
          '\f' +
          options
      )
    );
  }
  public GetExternalFolder() {
    return prompt('#', 'App.GetExternalFolder(');
  }
  public GetInternalFolder() {
    return prompt('#', 'App.GetInternalFolder(');
  }
  public GetSpecialFolder(name: string) {
    return prompt('#', 'App.GetSpecialFolder(\f' + name);
  }
  public GetEnv(name: string) {
    return prompt('#', 'App.GetEnv(\f' + name);
  }
  public GetPermission(type: string, callback: any) {
    prompt('#', 'App.GetPermission(\f' + type + '\f' + _Cbm(callback));
  }
  public CheckPermission(type: string) {
    return prompt('#', 'App.CheckPermission(\f' + type);
  }
  public ReadFile(file: string, encoding?: string) {
    return prompt('#', 'App.ReadFile(\f' + file + '\f' + encoding);
  }
  public WriteFile(file: string, text: string, mode: string, encoding: string) {
    prompt(
      '#',
      'App.WriteFile(\f' + file + '\f' + text + '\f' + mode + '\f' + encoding
    );
  }
  public OpenFile(file: string, type: string, choose: string) {
    prompt('#', 'App.OpenFile(\f' + file + '\f' + type + '\f' + choose);
  }
  public OpenUrl(url: string, type: string, choose: string) {
    prompt('#', 'App.OpenUrl(\f' + url + '\f' + type + '\f' + choose);
  }
  public DownloadFile(
    src: string,
    dest: string,
    title: string,
    desc: string,
    options: string
  ) {
    prompt(
      '#',
      'App.DownloadFile(\f' +
        src +
        '\f' +
        dest +
        '\f' +
        title +
        '\f' +
        desc +
        '\f' +
        options
    );
  }
  public ChooseFile(msg: string, type: string, callback: any) {
    prompt(
      '#',
      'App.ChooseFile(\f' + msg + '\f' + type + '\f' + _Cbm(callback)
    );
  }
  public ChooseContact(type: string, callback: any) {
    prompt('#', 'App.ChooseContact(\f' + type + '\f' + _Cbm(callback));
  }
  public ChooseImage(options: string, callback: any) {
    prompt('#', 'App.ChooseImage(\f' + options + '\f' + _Cbm(callback));
  }
  public ChooseAccount(callback: any) {
    prompt('#', 'App.ChooseAccount(\f' + _Cbm(callback));
  }
  public DeleteFile(file: string) {
    prompt('#', 'App.DeleteFile(' + file);
  }
  public CopyFile(src: string, dest: string) {
    prompt('#', 'App.CopyFile(' + src + '\f' + dest);
  }
  public CopyFolder(
    src: string,
    dest: string,
    overwrite: string,
    filter: string
  ) {
    prompt(
      '#',
      'App.CopyFolder(\f' + src + '\f' + dest + '\f' + overwrite + '\f' + filter
    );
  }
  public DeleteFolder(fldr: string) {
    prompt('#', 'App.DeleteFolder(' + fldr);
  }
  public RenameFile(src: string, dest: string) {
    prompt('#', 'App.RenameFile(\f' + src + '\f' + dest);
  }
  public RenameFolder(src: string, dest: string) {
    prompt('#', 'App.RenameFile(\f' + src + '\f' + dest);
  }
  public ReplaceInFile(
    file: string,
    txt: string,
    rep: string,
    options: string
  ) {
    prompt(
      '#',
      'App.ReplaceInFile(\f' + file + '\f' + txt + '\f' + rep + '\f' + options
    );
  }
  public UnzipFile(src: string, dest: string) {
    prompt('#', 'App.UnzipFile(\f' + src + '\f' + dest);
  }
  public ZipFile(src: string, dest: string) {
    prompt('#', 'App.ZipFile(\f' + src + '\f' + dest);
  }
  public ZipFolder(src: string, dest: string) {
    prompt('#', 'App.ZipFile(\f' + src + '\f' + dest);
  }
  public GetFreeSpace(mode: string) {
    // @ts-ignore TS2345
    return parseFloat(prompt('#', 'App.GetFreeSpace(\f' + mode));
  }
  public GetFileDate(file: string) {
    // @ts-ignore TS2345
    const d = parseInt(prompt('#', 'App.GetFileDate(\f' + file), 10);
    return d ? new Date(d) : null;
  }
  public GetFileSize(file: string) {
    // @ts-ignore TS2345
    return parseInt(prompt('#', 'App.GetFileSize(\f' + file), 10);
  }
  public GetThumbnail(
    src: string,
    dest: string,
    width: string,
    height: string
  ) {
    prompt(
      '#',
      'App.GetThumbnail(\f' + src + '\f' + dest + '\f' + width + '\f' + height
    );
  }
  public ScanFile(file: string) {
    prompt('#', 'App.ScanFile(\f' + file);
  }
  public GetLastButton() {
    const ret = prompt('#', 'App.GetLastButton(');
    if (ret) {
      return _map[ret];
    } else {
      return null;
    }
  }
  public GetLastToggle() {
    const ret = prompt('#', 'App.GetLastToggle(');
    if (ret) {
      return _map[ret];
    } else {
      return null;
    }
  }
  public GetLastCheckBox() {
    const ret = prompt('#', 'App.GetLastCheckBox(');
    if (ret) {
      return _map[ret];
    } else {
      return null;
    }
  }
  public GetLastImage() {
    const ret = prompt('#', 'App.GetLastImage(');
    if (ret) {
      return _map[ret];
    } else {
      return null;
    }
  }
  public IsBluetoothOn() {
    return prompt('#', 'App.IsBluetoothOn(') === 'true';
  }
  public IsScreenOn() {
    return prompt('#', 'App.IsScreenOn(') === 'true';
  }
  public WakeUp() {
    prompt('#', 'App.WakeUp(');
  }
  public GoToSleep() {
    prompt('#', 'App.GoToSleep(');
  }
  public Unlock() {
    prompt('#', 'App.Unlock(');
  }
  public Lock() {
    prompt('#', 'App.Lock(');
  }
  public SetScreenBrightness(level: string) {
    prompt('#', 'App.SetScreenBrightness(\f' + level);
  }
  public SetKioskMode(mode: string, enable: string, options: string) {
    prompt('#', 'App.SetKioskMode(\f' + mode + '\f' + enable + '\f' + options);
  }
  public GetMetadata(file: string, keys: string) {
    return prompt('#', 'App.GetMetadata(\f' + file + '\f' + keys);
  }
  public SetAlarm(
    type: string,
    id: string,
    callback: any,
    time: string,
    interval: string,
    options: string
  ) {
    return prompt(
      '#',
      'App.SetAlarm(\f' +
        type +
        '\f' +
        id +
        '\f' +
        _Cbm(callback) +
        '\f' +
        time +
        '\f' +
        interval +
        '\f' +
        options
    );
  }
  public Call(num: string) {
    prompt('#', 'App.Call(\f' + num);
  }
  public SimulateTouch(obj: { id: string }, x: string, y: string, dir: string) {
    prompt(
      '#',
      'App.SimulateTouch(\f' + obj.id + '\f' + x + '\f' + y + '\f' + dir
    );
  }
  public SimulateDrag(
    obj: { id: string },
    x1: string,
    y1: string,
    x2: string,
    y2: string,
    step: string,
    pause: string
  ) {
    prompt(
      '#',
      'App.SimulateDrag(\f' +
        obj.id +
        '\f' +
        x1 +
        '\f' +
        y1 +
        '\f' +
        x2 +
        '\f' +
        y2 +
        '\f' +
        step +
        '\f' +
        pause
    );
  }
  public SimulateScroll(
    obj: { id: string },
    x: string,
    y: string,
    dx: string,
    dy: string,
    count: string,
    fling: string
  ) {
    prompt(
      '#',
      'App.SimulateScroll(\f' +
        obj.id +
        '\f' +
        x +
        '\f' +
        y +
        '\f' +
        dx +
        '\f' +
        dy +
        '\f' +
        count +
        '\f' +
        fling
    );
  }
  public SimulateKey(
    obj: { id: string },
    keyName: string,
    modifiers: string,
    pause: string
  ) {
    prompt(
      '#',
      'App.SimulateKey(\f' +
        obj.id +
        '\f' +
        keyName +
        '\f' +
        modifiers +
        '\f' +
        pause
    );
  }
  public GetJoystickState(id: string, key: string) {
    // @ts-ignore TS2345
    return parseFloat(prompt('#', 'App.GetJoyState(\f' + id + '\f' + key));
  }
  public GetJoystickName(id: string) {
    return prompt('#', 'App.GetJoyName(\f' + id);
  }
  public SetJoystickOptions(options: string) {
    prompt('#', 'App.SetJoystickOptions(\f' + options);
  }
  public SetAutoBoot(auto: string) {
    prompt('#', 'App.SetAutoBoot(\f' + auto);
  }
  public SetAutoWifi(auto: string) {
    prompt('#', 'App.SetAutoWifi(\f' + auto);
  }
  public SetAutoStart(appName: string) {
    prompt('#', 'App.SetAutoStart(\f' + appName);
  }
  public HttpRequest(
    type: string,
    baseUrl: string,
    path: string,
    params: string,
    callback: any,
    headers: string
  ) {
    prompt(
      '#',
      'App.HttpRequest(\f' +
        type +
        '\f' +
        baseUrl +
        '\f' +
        path +
        '\f' +
        params +
        '\f' +
        _Cbm(callback) +
        '\f' +
        headers
    );
  }
  public UploadFile(url: string, file: string, name: string, callback: any) {
    prompt(
      '#',
      'App.UploadFile(\f' +
        url +
        '\f' +
        file +
        '\f' +
        name +
        '\f' +
        _Cbm(callback)
    );
  }
  public SaveCookies() {
    prompt('#', 'App.SaveCookies(');
  }
  public ClearCookies(session: string) {
    prompt('#', 'App.ClearCookies(\f' + session);
  }
  public SetUserAgent(agent: string) {
    prompt('#', 'App.SetUserAgent(\f' + agent);
  }
  public SetUserCreds(name: string, password: string) {
    prompt('#', 'App.SetUserCreds(\f' + name + '\f' + password);
  }
  public QueryContent(
    uri: string,
    columns: string,
    select: string,
    args: string,
    sort: string
  ) {
    return JSON.parse(
      // @ts-ignore TS2345
      prompt(
        '#',
        'App.QueryContent(\f' +
          uri +
          '\f' +
          columns +
          '\f' +
          select +
          '\f' +
          args +
          '\f' +
          sort
      )
    );
  }
  public Uri2Path(uri: string) {
    return prompt('#', 'App.Uri2Path(\f' + uri);
  }
  public ScreenShot(fileName: string, quality: string) {
    prompt('#', 'App.ScreenShot(\f' + fileName + '\f' + quality);
  }
  public InstallWallpaper(packageName: string, className: string) {
    prompt('#', 'App.InstallWallpaper\f' + packageName + '\f' + className);
  }

  // These objects auto-release when layout is destroyed.
  public CreateLayout(type: string, options: string) {
    const ret = prompt('#', 'App.CreateLayout(' + type + '\f' + options);
    if (ret) {
      return new Lay(ret);
    } else {
      return null;
    }
  }
  public CreateImage(
    file: string,
    width: string,
    height: string,
    options: string,
    w: string,
    h: string
  ) {
    const ret = prompt(
      '#',
      'App.CreateImage(' +
        file +
        '\f' +
        width +
        '\f' +
        height +
        '\f' +
        options +
        '\f' +
        w +
        '\f' +
        h
    );
    if (ret) {
      return new Img(ret);
    } else {
      return null;
    }
  }
  public CreateButton(
    text: string,
    width: string,
    height: string,
    options: string
  ) {
    const ret = prompt(
      '#',
      'App.CreateButton(' + text + '\f' + width + '\f' + height + '\f' + options
    );
    return ret ? new Btn(ret) : null;
  }
  public CreateToggle(
    text: string,
    width: string,
    height: string,
    options: string
  ) {
    const ret = prompt(
      '#',
      'App.CreateToggle(' + text + '\f' + width + '\f' + height + '\f' + options
    );
    return ret ? new Tgl(ret) : null;
  }
  public CreateCheckBox(
    text: string,
    width: string,
    height: string,
    options: string
  ) {
    const ret = prompt(
      '#',
      'App.CreateCheckBox(' +
        text +
        '\f' +
        width +
        '\f' +
        height +
        '\f' +
        options
    );
    return ret ? new Chk(ret) : null;
  }
  public CreateSpinner(
    list: string,
    width: string,
    height: string,
    options: string
  ) {
    const ret = prompt(
      '#',
      'App.CreateSpinner(' +
        list +
        '\f' +
        width +
        '\f' +
        height +
        '\f' +
        options
    );
    return ret ? new Spn(ret) : null;
  }
  public CreateSeekBar(width: string, height: string, options: string) {
    const ret = prompt(
      '#',
      'App.CreateSeekBar(' + width + '\f' + height + '\f' + options
    );
    return ret ? new Skb(ret) : null;
  }
  public CreateText(
    text: string,
    width: string,
    height: string,
    options: string
  ) {
    const ret = prompt(
      '#',
      'App.CreateText(' + text + '\f' + width + '\f' + height + '\f' + options
    );
    return ret ? new Txt(ret) : null;
  }
  public CreateTextEdit(
    text: string,
    width: string,
    height: string,
    options: string
  ) {
    const ret = prompt(
      '#',
      'App.CreateTextEdit(' +
        text +
        '\f' +
        width +
        '\f' +
        height +
        '\f' +
        options
    );
    return ret ? new Txe(ret) : null;
  }
  public CreateList(
    list: string,
    width: string,
    height: string,
    options: string,
    delim: string
  ) {
    const ret = prompt(
      '#',
      'App.CreateList(\f' +
        list +
        '\f' +
        width +
        '\f' +
        height +
        '\f' +
        options +
        '\f' +
        delim
    );
    return ret ? new Lst(ret) : null;
  }
  public CreateWebView(
    width: string,
    height: string,
    options: string,
    zoom: string
  ) {
    const ret = prompt(
      '#',
      'App.CreateWeb(\f' + width + '\f' + height + '\f' + options + '\f' + zoom
    );
    return ret ? new Web(ret) : null;
  }
  public CreateScroller(width: string, height: string, options: string) {
    const ret = prompt(
      '#',
      'App.CreateScroller(' + width + '\f' + height + '\f' + options
    );
    return ret ? new Scr(ret) : null;
  }
  public CreateCameraView(width: string, height: string, options: string) {
    const ret = prompt(
      '#',
      'App.CreateCameraView(' + width + '\f' + height + '\f' + options
    );
    return ret ? new Cam(ret) : null;
  }
  public CreateVideoView(width: string, height: string, options: string) {
    const ret = prompt(
      '#',
      'App.CreateVideoView(\f' + width + '\f' + height + '\f' + options
    );
    return ret ? new Vid(ret) : null;
  }
  public CreateGameView(width: string, height: string, options: string) {
    const ret = prompt(
      '#',
      'App.CreateGameView(\f' + width + '\f' + height + '\f' + options
    );
    return ret ? new WGL(ret) : null;
  }
  public CreateCodeEdit(
    text: string,
    width: string,
    height: string,
    options: string
  ) {
    const ret = prompt(
      '#',
      'App.CreateCodeEdit(\f' +
        text +
        '\f' +
        width +
        '\f' +
        height +
        '\f' +
        options
    );
    return ret ? new Cde(ret) : null;
  }
  public CreateTheme(baseTheme: string) {
    const ret = prompt('#', 'App.CreateTheme(\f' + baseTheme);
    return ret ? new Thm(ret) : null;
  }

  // These objects auto-release (ie. single instance)
  public CreateYesNoDialog(msg: string, options: string) {
    if (_ynd) {
      _ynd.Release();
    }
    const ret = prompt('#', 'App.CreateYesNoDialog(\f' + msg + '\f' + options);
    _ynd = ret ? new Ynd(ret) : null;
    return _ynd;
  }
  public CreateListDialog(title: string, list: string, options: string) {
    if (_ldg) {
      _ldg.Release();
    }
    const ret = prompt(
      '#',
      'App.CreateListDialog(\f' + title + '\f' + list + '\f' + options
    );
    _ldg = ret ? new Ldg(ret) : null;
    return _ldg;
  }
  public CreateListView(list: string, title: string, options: string) {
    if (_lvw) {
      _lvw.Release();
    }
    const ret = prompt(
      '#',
      'App.CreateListView(\f' + list + '\f' + title + '\f' + options
    );
    _lvw = ret ? new Lvw(ret) : null;
    return _lvw;
  }
  public CreateBluetoothList(filter: string): Btl | null {
    if (_btl) {
      _btl.Release();
    }
    const ret = prompt('#', 'App.CreateBluetoothList(' + filter);
    _btl = ret ? new Btl(ret) : null;
    return _btl;
  }
  public CreateAudioRecorder() {
    if (_rec) {
      _rec.Release();
    }
    const ret = prompt('#', 'App.CreateAudioRecorder(');
    _rec = ret ? new Rec(ret) : null;
    return _rec;
  }
  public CreateSMS() {
    if (_sms) {
      _sms.Release();
    }
    const ret = prompt('#', 'App.CreateSMS(');
    _sms = ret ? new SMS(ret) : null;
    return _sms;
  }
  public CreateEmail(account: string, password: string) {
    if (_eml) {
      _eml.Release();
    }
    const ret = prompt('#', 'App.CreateEmail(' + account + '\f' + password);
    _eml = ret ? new EMAIL(ret) : null;
    return _eml;
  }
  public CreateSmartWatch(type: string) {
    if (_smw) {
      _smw.Release();
    }
    const ret = prompt('#', 'App.CreateSmartWatch(\f' + type);
    _smw = ret ? new SMW(ret) : null;
    return _smw;
  }
  public CreateCrypt(options: string) {
    if (_crp) {
      _crp.Release();
    }
    const ret = prompt('#', 'App.CreateCrypt(\f' + options);
    _crp = ret ? new Crp(ret) : null;
    return _crp;
  }
  public CreateSpeechRec(options: string) {
    if (_spr) {
      _spr.Release();
    }
    const ret = prompt('#', 'App.CreateSpeechRec(\f' + options);
    _spr = ret ? new Spr(ret) : null;
    return _spr;
  }
  public CreatePhoneState(types: string) {
    if (_pst) {
      _pst.Release();
    }
    const ret = prompt('#', 'App.CreatePhoneState(\f' + types);
    _pst = ret ? new Pst(ret) : null;
    return _pst;
  }
  public CreateWallpaper(options: string) {
    if (_wpr) {
      _wpr.Release();
    }
    const ret = prompt('#', 'App.CreateWallpaper(\f' + options);
    _wpr = ret ? new Wpr(ret) : null;
    return _wpr;
  }

  // These objects need releasing manually.
  public CreateDialog(title: string, options: string) {
    const ret = prompt('#', 'App.CreateDialog(\f' + title + '\f' + options);
    return ret ? new Dlg(ret) : null;
  }
  public CreateOverlay(options: string) {
    const ret = prompt('#', 'App.CreateOverlay(\f' + options);
    return ret ? new Ovl(ret) : null;
  }
  public CreateMediaPlayer() {
    const ret = prompt('#', 'App.CreateMediaPlayer(');
    return ret ? new Aud(ret) : null;
  }
  public CreateSensor(type: string, options: string) {
    const ret = prompt('#', 'App.CreateSensor(' + type + '\f' + options);
    return ret ? new Sns(ret) : null;
  }
  public CreateLocator(type: string, options: string) {
    const ret = prompt('#', 'App.CreateLocator(' + type + '\f' + options);
    return ret ? new Loc(ret) : null;
  }
  public CreateNetClient(type: string) {
    const ret = prompt('#', 'App.CreateNetClient(' + type);
    return ret ? new Net(ret) : null;
  }
  public CreateNxtRemote() {
    const ret = prompt('#', 'App.CreateNxtRemote(');
    return ret ? new Nxt(ret, null) : null;
  }
  public CreateWebServer(port: string, options: string) {
    const ret = prompt('#', 'App.CreateWebServer(' + port + '\f' + options);
    return ret ? new Wbs(ret) : null;
  }
  public CreateUSBSerial(
    baudRate: string,
    dataBits: string,
    stopBits: string,
    parity: string,
    device: string
  ) {
    const ret = prompt(
      '#',
      'App.CreateUSBSerial(\f' +
        baudRate +
        '\f' +
        dataBits +
        '\f' +
        stopBits +
        '\f' +
        parity +
        '\f' +
        device
    );
    return ret ? new Usb(ret) : null;
  }
  public CreateSysProc(cmd: string, env: string, dir: string, options: string) {
    const ret = prompt(
      '#',
      'App.CreateSysProc(\f' + cmd + '\f' + env + '\f' + dir + '\f' + options
    );
    return ret ? new Sys(ret) : null;
  }
  public CreateService(
    packageName: string,
    className: string,
    callback: any,
    options: string
  ) {
    const ret = prompt(
      '#',
      'App.CreateService(\f' +
        packageName +
        '\f' +
        className +
        '\f' +
        options +
        '\f' +
        _Cbm(callback)
    );
    return ret ? new Svc(ret) : null;
  }
  public CreateSynth(type: string) {
    const ret = prompt('#', 'App.CreateSynth(' + type);
    return ret ? new Syn(ret) : null;
  }
  public CreateBluetoothSerial(mode: string) {
    const ret = prompt('#', 'App.CreateBluetoothSerial(\f' + mode);
    return ret ? new Bts(ret) : null;
  }
  public CreateZipUtil(mode: string) {
    const ret = prompt('#', 'App.CreateZipUtil(\f' + mode);
    return ret ? new Zip(ret) : null;
  }
  public CreateDownloader(options: string) {
    const ret = prompt('#', 'App.CreateDownloader(\f' + options);
    return ret ? new Dwn(ret) : null;
  }
  public CreateMediaStore() {
    const ret = prompt('#', 'App.CreateMediaStore(');
    return ret ? new Med(ret) : null;
  }
  public CreatePlayStore() {
    const ret = prompt('#', 'App.CreatePlayStore(');
    return ret ? new Ply(ret) : null;
  }
  public CreateNotification(options: string) {
    const ret = prompt('#', 'App.CreateNotification(\f' + options);
    return ret ? new Not(ret) : null;
  }
  public CreateFile(file: string, mode: string) {
    const ret = prompt('#', 'App.CreateFile(\f' + file + '\f' + mode);
    return ret ? new Fil(ret) : null;
  }

  // Special methods.
  public Start() {
    if (typeof OnStart === 'function') {
      OnStart();
      prompt('#', '_Start');
      _started = true;
    }
  }

  public CreateObject(name: string, type: string) {
    if (!type) {
      try {
        return JSON.parse('new ' + name + '()');
      } catch (e) {
        return null;
      }
    } else {
      const ret = prompt('#', '_Obj(\f' + type + '\f' + name);
      if (ret) {
        return new SObj(ret);
      } else {
        return null;
      }
    }
  }

  public GA(cmd: { toLowerCase: () => string }) {
    try {
      if (app.FileExists('/Sys/ga.js')) {
        if (cmd.toLowerCase() === 'create') {
          _LoadScriptSync('/Sys/ga.js');
          window.ga =
            window.ga ||
            function() {
              (ga.q = ga.q || []).push(arguments);
            };
          ga.l = +new Date();
          const dbg = _dbg;
          _UseDbg(false);
          ga('create', arguments[1], {
            storage: 'none',
            clientId: app.GetDeviceId(),
          });
          ga('set', { checkProtocolTask: null, checkStorageTask: null });
          _UseDbg(dbg);
        } else ga.apply(this, arguments);
      }
    } catch (e) {}
  }

  public _animatev8() {
    if (_cbAnimate) {
      const t = new Date().getTime();
      _cbAnimate(t, t - this._anim_t);
      this._anim_t = t;
    }
  }
  public _animate() {
    if (_cbAnimate) {
      const t = new Date().getTime();
      _cbAnimate(t, t - this._anim_t);
      this._anim_t = t;
      requestAnimationFrame(_animate);
    }
  }
  public Animate(callback: any, fps: any) {
    _cbAnimate = callback;
    this._anim_t = new Date().getTime();
    if (_isV8) {
      _fps = fps ? fps : 30;
      if (_cbAnimate) {
        _tmAnimate = setInterval(_animatev8, 1000 / _fps);
      } else if (_tmAnimate) {
        clearInterval(_tmAnimate);
      }
    } else {
      window._fps = fps ? fps : 30;
      requestAnimationFrame(_animate);
    }
  }

  public Language2Code(name: { toLowerCase: () => string | number }) {
    if (name) {
      return _languages.codes[name.toLowerCase()];
    } else {
      return _curLang;
    }
  }

  public SetLanguage(name: { toLowerCase: () => string | number }) {
    const json = this.ReadFile('lang.json');
    _languages = JSON.parse(json);
    _curLang = _languages.codes[name.toLowerCase()];
  }

  // Helper classes.
  public CreateNxt() {
    var nxtHelp = new _NxtHelp();
    return nxtHelp.nxt_CreateNxt();
  }
  public CreateTabs(list: any, width: any, height: any, options: any) {
    return new _Tabs(list, width, height, options);
  }
  public CreateWebSocket(id: any, ip: any, port: any, options: any) {
    return new _WebSock(id, ip, port, options);
  }
  public CreateWizard(title: any, width: any, height: any, callback: any) {
    return new _Wizard(title, width, height, callback);
  }

  // Externally defined methods.
  public ShowTextDialog(title: any, deflt: any, callback: any) {
    _ShowTextDialog(title, deflt, callback);
  }
  public ShowTip(msg: any, left: any, top: any, timeOut: any, options: any) {
    _ShowTip(msg, left, top, timeOut, options);
  }
  public PlaySound(file: any) {
    _PlaySound(file);
  }

  // Hybrid objects.
  public CreateGLView(
    width: string | number,
    height: string | number,
    options: string
  ) {
    var glv = null;
    if (options.toLowerCase().indexOf('fast2d') > -1) {
      _LoadScriptSync('/Sys/cp.js');
      _LoadScriptSync('/Sys/gl.js');
      glv = new GLV(
        prompt(
          '#',
          'App.CreateGLView(\f' + width + '\f' + height + '\f' + options
        )
      );
      glv.canvas = FastCanvas.create();
      glv.ctx = glv.canvas.getContext('2d');
      glv.width = Math.round(app.GetDisplayWidth() * width);
      glv.height = Math.round(app.GetDisplayHeight() * height);
      glv.aspect = glv.width / glv.height;
      glv.GetType = () => 'GLView';
    }
    return glv;
  }

  public OpenDatabase(name: any) {
    _LoadScriptSync('/Sys/cp.js');
    _LoadScriptSync('/Sys/sql.js');
    _CreateCP('sqliteplugin');

    const db = sqlitePlugin.openDatabase(name);
    db.name = name;

    db.GetType = () => 'Database';
    db.GetName = () => db.name;
    db.ExecuteSql = (
      sql: any,
      params: any,
      success: { apply: (arg0: any, arg1: any[]) => void } | null,
      error: { (msg: string): void; apply?: any }
    ) => {
      if (!success) {
        success = null;
      }
      if (!error) {
        error = _Err;
      }

      db.transaction(function(tx: {
        executeSql: (
          arg0: any,
          arg1: any,
          arg2: (tx: any, res: any) => void,
          arg3: (t: any, e: any) => void
        ) => void;
      }) {
        tx.executeSql(
          sql,
          params,
          function(tx: any, res: any) {
            if (success) {
              success.apply(db, [res]);
            }
          },
          function(t: any, e: { message: any }) {
            error.apply(db, [e.message]);
          }
        );
      },
      error);
    };
    db.Close = () => {
      db.close(_Log, _Err);
    };
    db.Delete = () => {
      sqlitePlugin.deleteDatabase(db.name, _Log, _Err);
    };
    return db;
  }
}
/*end of App*/

// @ts-ignore TS6133
function _Try(p1: string, p2: string, p3: string, p4: string) {
  return prompt('#', 'App.Try(\f' + p1 + '\f' + p2 + '\f' + p3 + '\f' + p4);
}
// @ts-ignore TS6133
function _Call(
  id: string | number,
  func: { apply: (arg0: any, arg1: any) => void },
  params: any
) {
  if (func) {
    func.apply(_map[id], params);
  }
}
// @ts-ignore TS6133
function _Cb(obj: any, func: any) {
  return new _ObjCb(obj, func);
}
function _ObjCb(obj: any, func: string) {
  _cbMap[++_cbId] = obj;
  this.name = "_cbMap['" + _cbId + "']." + func;
}

export function _Cbm(func: () => any): StringOrNull {
  return func ? (func.name ? func.name : new _ObjCbm(func).name) : null;
}
function _ObjCbm(func: () => any) {
  const hs = _hash(func.toString());
  _cbMap[hs] = func;
  this.name = "_cbMap['" + hs + "']";
}

function _hash(str: string) {
  let hash = 5381;
  let i = str.length;
  while (i) {
    // tslint:disable-next-line:no-bitwise
    hash = (hash * 33) ^ str.charCodeAt(--i);
  }
  // tslint:disable-next-line:no-bitwise
  return hash >>> 0;
}

function _UseDbg(b: boolean) {
  prompt('#', '_UseDbg(\f' + b);
  _dbg = b;
}
function _ExecV8(file: string) {
  return prompt('#', '_ExecV8(\f' + file);
}
// @ts-ignore TS6133
function _Thread(file: string) {
  return prompt('#', '_Thread(\f' + file);
}
// @ts-ignore TS6133
function _DoEvents(ms: string) {
  return prompt('#', '_DoEvents(\f' + ms);
}
function _CreateCP(service: string) {
  return prompt('#', '_CreateCP(\f' + service);
}
// @ts-ignore TS6133
function _ExecCP(
  callbackId: string,
  service: string,
  action: string,
  argsJson: string
) {
  return prompt(
    '#',
    '_ExecCP(\f' + callbackId + '\f' + service + '\f' + action + '\f' + argsJson
  );
}

function _LoadScript(url: string, callback: any) {
  if (_scripts[url]) {
    if (callback) {
      callback();
    }
    return;
  }
  if (url.slice(-4) === '.dsj') {
    url += '.js';
  }
  const dbg = _dbg;
  _UseDbg(false);
  if (url.indexOf(':') < 0 && !this.FileExists(url)) {
    alert(
      'Error: ' +
        url +
        ' not found!' +
        (this.IsAPK ? '\n\n(Note: Assets are case sensitive)' : '')
    );
  }
  _UseDbg(dbg);
  if (_isV8) {
    _ExecV8(url);
  } else {
    const head = document.getElementsByTagName('head')[0];
    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;
    script.onload = callback;
    head.appendChild(script);
  }
  _scripts[url] = true;
}

function _LoadScriptSync(url: string) {
  if (_scripts[url]) {
    return;
  }
  if (url.slice(-4) === '.dsj') {
    url += '.js';
  }
  const dbg = _dbg;
  _UseDbg(false);
  if (url.indexOf(':') < 0 && !this.FileExists(url)) {
    alert(
      'Error: ' +
        url +
        ' not found!' +
        (app.IsAPK ? '\n\n(Note: Assets are case sensitive)' : '')
    );
  }
  _UseDbg(dbg);
  if (_isV8) {
    _ExecV8(url);
  } else {
    const head = document.getElementsByTagName('head')[0];
    const script = document.createElement('script');
    script.type = 'text/javascript';
    _UseDbg(false);
    script.text = this.ReadFile(url);
    _UseDbg(dbg);
    head.appendChild(script);
  }
  _scripts[url] = true;
}

function _LoadPlugin(name: string) {
  if (!name) {
    return;
  }
  const privDir = this.GetPrivateFolder('Plugins');
  _LoadScriptSync(privDir + '/' + name.toLowerCase() + '/' + name + '.inc');
}

// @ts-ignore TS6133
function _CreatePlugin(name: string, options: string) {
  const ret = prompt('#', 'App.CreatePlugin(' + name + '\f' + options);
  if (ret) {
    return new Plg(ret);
  } else {
    throw new Error('Failed to create plugin:' + name);
  }
}

// @ts-ignore TS6133
function _Run(s: string) {
  _busy = true;
  JSON.parse(s);
  _busy = false;
}

// @ts-ignore TS6133
function _SafeRun(s: string) {
  try {
    _busy = true;
    _UseDbg(false);
    JSON.parse(s);
    _UseDbg(true);
    _busy = false;
  } catch (e) {
    //
  }
}

// @ts-ignore TS6133
function T(id: string | number, lang: any) {
  let tr = _languages ? _languages.trans[id] : null;
  if (tr) {
    tr = tr[lang ? lang : _curLang];
  } else {
    return '[No Translation]';
  }
  return tr ? tr : _languages.trans[id]['en'];
}

// @ts-ignore TS6133
function OnCreate(extract: any, debug: any) {
  if (typeof _CheckFolderName === 'function') {
    _CheckFolderName();
  }
  // if( extract ) app._Extract( true );
  if (typeof OnStart === 'function') {
    OnStart();
    prompt('#', '_Start');
    _started = true;
  }
  if (debug) {
    this.CreateDebug();
  }
}

const app = new App();

function _Log(msg: any) {
  app.Debug(msg);
}
function _Err(msg: string) {
  app.Debug('ERROR: ' + msg);
}
// @ts-ignore TS6133
function _GetStack() {
  return new Error().stack;
}
// @ts-ignore TS6133
function _AddPermissions() {}
// @ts-ignore TS6133
function _RemovePermissions() {}
// @ts-ignore TS6133
function _AddOptions() {}

if (typeof navigator === 'undefined') {
  navigator = { userAgent: 'Android' };
}
if (navigator.userAgent.indexOf('Android') > -1) {
  // Provide toast popup on Remix.
  let _dlgPop = null;
  if (this.GetModel().indexOf('Remix') > -1) {
    this.ShowPopup = (msg: string, options: string = '') => {
      options = options.toLowerCase();
      if (_dlgPop === null) {
        _dlgPop = this.CreateDialog(null, 'NoDim,NoTouch,NoFocus');
        _dlgPop.SetBackColor('#cc000000');
        _dlgPop.SetPosition(-1, options.indexOf('bottom') > -1 ? 0.75 : 0.25);
        const lay = app.CreateLayout('linear', 'vcenter');
        lay.SetPadding(0.02, 0.02, 0.02, 0.02);
        _dlgPop.AddLayout(lay);
        _txtDlgPop = this.CreateText(msg);
        _txtDlgPop.SetTextSize(22);
        _txtDlgPop.SetTextColor('#ffffff');
        lay.AddChild(_txtDlgPop);
      } else {
        _txtDlgPop.SetText(msg);
      }
      _dlgPop.Show();
      if (_dlgPop.timer) {
        clearTimeout(_dlgPop.timer);
      }
      const time = options.indexOf('short') ? 2000 : 4000;
      _dlgPop.timer = setTimeout(() => {
        _dlgPop.Hide();
      }, time);
    };
  }

  // Init app.
  prompt('#', '_Init');
}
