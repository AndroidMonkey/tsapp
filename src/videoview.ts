import { Obj } from './obj';

export class VideoView extends Obj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'VideoView';
  }
  public SetFile(file: string) {
    prompt(this.id, 'Vid.SetFile(' + file);
  }
  public SetSubtitles(file: string) {
    prompt(this.id, 'Vid.SetSubtitles(\f' + file);
  }
  public Play() {
    prompt(this.id, 'Vid.Play(');
  }
  public Pause() {
    prompt(this.id, 'Vid.Pause(');
  }
  public Stop() {
    prompt(this.id, 'Vid.Stop(');
  }
  public IsReady() {
    return prompt(this.id, 'Vid.IsReady(') === 'true';
  }
  public IsPlaying() {
    return prompt(this.id, 'Vid.IsPlaying(') === 'true';
  }
  public SeekTo(secs: string) {
    prompt(this.id, 'Vid.SeekTo(' + secs);
  }
  public GetPosition() {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Vid.GetPosition('));
  }
  public GetDuration() {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Vid.GetDuration('));
  }
  public SetVolume(left: string, right: string) {
    prompt(this.id, 'Vid.SetVolume(\f' + left + '\f' + right);
  }
  public SetOnReady(callback: any) {
    prompt(this.id, 'Vid.SetOnReady(' + _Cbm(callback));
  }
  public SetOnComplete(callback: any) {
    prompt(this.id, 'Vid.SetOnComplete(' + _Cbm(callback));
  }
  public SetOnError(callback: any) {
    prompt(this.id, 'Vid.SetOnError(' + _Cbm(callback));
  }
  public SetOnSubtitle(callback: any) {
    prompt(this.id, 'Vid.SetOnSubtitle(\f' + _Cbm(callback));
  }
}
