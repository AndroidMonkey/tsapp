import { SObj } from './sobj';

export class Locator extends SObj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'Locator';
  }
  public SetOnChange(callback: any) {
    prompt(this.id, 'Loc.SetOnChange(' + _Cbm(callback));
  }
  public Start() {
    prompt(this.id, 'Loc.Start(');
  }
  public Stop() {
    prompt(this.id, 'Loc.Stop(');
  }
  public SetRate(rate: string) {
    prompt(this.id, 'Loc.SetRate(' + rate);
  }
  public GetDistanceTo(lat: string, lng: string) {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Loc.GetDistanceTo(' + lat + '\f' + lng));
  }
  public GetBearingTo(lat: string, lng: string) {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Loc.GetBearingTo(' + lat + '\f' + lng));
  }
}
