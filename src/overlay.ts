import { Layout } from './layout';
import { Obj } from './obj';

export class Overlay extends Obj {
  constructor(id: any) {
    super(id);
  }
  public GetType() {
    return 'Overlay';
  }
  public AddLayout(layout: Layout, left: string, top: string, options: string) {
    prompt(
      this.id,
      'Ovl.AddLayout(\f' +
        (layout ? layout.id : null) +
        '\f' +
        left +
        '\f' +
        top +
        '\f' +
        options
    );
  }
  public RemoveLayout(layout: Layout) {
    prompt(this.id, 'Ovl.RemoveLayout(\f' + (layout ? layout.id : null));
  }
  public SetPosition(
    layout: Layout,
    left: string,
    top: string,
    options: string
  ) {
    prompt(
      this.id,
      'Ovl.SetPosition(\f' +
        (layout ? layout.id : null) +
        '\f' +
        left +
        '\f' +
        top +
        '\f' +
        options
    );
  }
}
