import { Obj } from './obj';

export class Button extends Obj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'Button';
  }
  public SetEnabled(enable: string) {
    prompt(this.id, 'Btn.SetEnabled(\f' + enable);
  }
  public SetOnTouch(callback: any) {
    prompt(this.id, 'Btn.SetOnClick(' + _Cbm(callback));
  }
  public SetOnTouchEx(callback: any) {
    prompt(this.id, 'Btn.SetOnClick(' + _Cbm(callback));
  }
  public SetText(text: string) {
    prompt(this.id, 'Btn.SetText(' + text);
  }
  public SetHtml(html: string) {
    prompt(this.id, 'Btn.SetHtml(' + html);
  }
  public GetText() {
    return prompt(this.id, 'Btn.GetText(');
  }
  public SetTextColor(clr: string) {
    prompt(this.id, 'Btn.SetTextColor(' + clr);
  }
  public SetFontFile(file: string) {
    prompt(this.id, 'Btn.SetFontFile(\f' + file);
  }
  public SetTextShadow(radius: string, dx: string, dy: string, color: string) {
    prompt(
      this.id,
      'Btn.SetTextShadow(\f' + radius + '\f' + dx + '\f' + dy + '\f' + color
    );
  }
  public SetTextSize(size: string, mode: string) {
    prompt(this.id, 'Btn.SetTextSize(\f' + size + '\f' + mode);
  }
  public GetTextSize(mode: string) {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Btn.GetTextSize(\f' + mode));
  }
  public SetEllipsize(mode: string) {
    prompt(this.id, 'Btn.SetEllipsize(\f' + mode);
  }
  public SetBackColor(clr: string) {
    prompt(this.id, 'Btn.SetBackColor(\f' + clr);
  }
  public SetStyle(
    clr1: string,
    clr2: string,
    radius: string,
    strokeClr: string,
    strokeWidth: string,
    shadow: string
  ) {
    prompt(
      this.id,
      'Btn.SetStyle(\f' +
        clr1 +
        '\f' +
        clr2 +
        '\f' +
        radius +
        '\f' +
        strokeClr +
        '\f' +
        strokeWidth +
        '\f' +
        shadow
    );
  }
}
