import { Obj } from './obj';

export class Dialog extends Obj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'Dialog';
  }
  public SetOnTouch(callback: any) {
    prompt(this.id, 'Dlg.SetOnClick(' + _Cbm(callback));
  }
  public AddLayout(layout: { id: string }) {
    prompt(this.id, 'Dlg.AddLayout(' + layout.id);
  }
  public RemoveLayout(layout: { id: string }) {
    prompt(this.id, 'Dlg.RemoveLayout(' + layout.id);
  }
  public Show() {
    prompt(this.id, 'Dlg.Show(');
  }
  public Hide() {
    prompt(this.id, 'Dlg.Hide(');
  }
  public IsVisible() {
    return prompt(this.id, 'Dlg.IsVisible\f') === 'true';
  }
  public Dismiss() {
    prompt(this.id, 'Dlg.Dismiss(');
  }
  public SetTitle(title: string, options: string) {
    prompt(this.id, 'Dlg.SetTitle(\f' + title + '\f' + options);
  }
  public SetBackColor(clr: string) {
    prompt(this.id, 'Dlg.SetBackColor(\f' + clr);
  }
  public SetBackground(file: string, options: string) {
    prompt(this.id, 'Dlg.SetBackground(\f' + file + '\f' + options);
  }
  public AdjustColor(hue: string, sat: string, bright: string, cont: string) {
    prompt(
      this.id,
      'Dlg.AdjustColor(\f' + hue + '\f' + sat + '\f' + bright + '\f' + cont
    );
  }
  public SetSize(width: string, height: string, options: string) {
    prompt(this.id, 'Dlg.SetSize(\f' + width + '\f' + height + '\f' + options);
  }
  public SetPosition(
    left: string,
    top: string,
    width: string,
    height: string,
    options: string
  ) {
    prompt(
      this.id,
      'Dlg.SetPosition(\f' +
        left +
        '\f' +
        top +
        '\f' +
        width +
        '\f' +
        height +
        '\f' +
        options
    );
  }
  public SetOnCancel(callback: any) {
    prompt(this.id, 'Dlg.SetOnCancel(\f' + _Cbm(callback));
  }
  public EnableBackKey(enable: string) {
    prompt(this.id, 'Dlg.EnableBackKey(\f' + enable);
  }
  public SetOnBack(callback: any) {
    prompt(this.id, 'Dlg.SetOnBack(\f' + _Cbm(callback));
  }
}
