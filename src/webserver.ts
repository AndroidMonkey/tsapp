import { SObj } from './sobj';

export class WebServer extends SObj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'WebServer';
  }
  public Destroy() {
    prompt(this.id, 'Wbs.Destroy(');
    _map[this.id] = null;
  }
  public SetFolder(folder: string) {
    prompt(this.id, 'Wbs.SetFolder(\f' + folder);
  }
  public SetUploadFolder(folder: string) {
    prompt(this.id, 'Wbs.SetUploadFolder(\f' + folder);
  }
  public Start() {
    prompt(this.id, 'Wbs.Start(');
  }
  public Stop() {
    prompt(this.id, 'Wbs.Stop(');
  }
  public SetResponse(text: string) {
    prompt(this.id, 'Wbs.SetResponse(' + text);
  }
  public AddRedirect(pattern: string, location: string) {
    prompt(this.id, 'Wbs.AddRedirect(\f' + pattern + '\f' + location);
  }
  public AddServlet(path: string, callback: any) {
    prompt(this.id, 'Wbs.AddServlet(' + path + '\f' + _Cbm(callback));
  }
  public SendText(txt: string, ip: string, id: string) {
    prompt(this.id, 'Wbs.SendText(\f' + txt + '\f' + ip + '\f' + id);
  }
  public GetWebSockClients() {
    // @ts-ignore TS2345
    return JSON.parse(prompt(this.id, 'Wbs.GetWebSockClients('));
  }
  public SetOnReceive(callback: any) {
    prompt(this.id, 'Wbs.SetOnReceive(\f' + _Cbm(callback));
  }
  public SetOnUpload(callback: any) {
    prompt(this.id, 'Wbs.SetOnUpload(\f' + _Cbm(callback));
  }
}
