import { SObj } from './sobj';

export class Downloader extends SObj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'Downloader';
  }
  public Download(url: string, fldr: string, name: string, headers: string) {
    prompt(
      this.id,
      'Dwn.Download(\f' + url + '\f' + fldr + '\f' + name + '\f' + headers
    );
  }
  public IsComplete() {
    return prompt(this.id, 'Dwn.IsComplete(') === 'true';
  }
  public GetProgress() {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Dwn.GetProgress('));
  }
  public GetSize() {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Dwn.GetSize('));
  }
  public SetOnDownload(callback: any) {
    prompt(this.id, 'Dwn.SetOnDownload(\f' + _Cbm(callback));
  }
  public SetOnComplete(callback: any) {
    prompt(this.id, 'Dwn.SetOnComplete(\f' + _Cbm(callback));
  }
  public SetOnCancel(callback: any) {
    prompt(this.id, 'Dwn.SetOnCancel(\f' + _Cbm(callback));
  }
  public SetOnError(callback: any) {
    prompt(this.id, 'Dwn.SetOnError(\f' + _Cbm(callback));
  }
}
