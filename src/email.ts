import { SObj } from './sobj';

export class EMAIL extends SObj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'Email';
  }
  public SetSMTP(server: string, port: string) {
    prompt(this.id, 'EML.SetSMTP(' + server + '\f' + port);
  }
  public SetIMAP(server: string, port: string) {
    prompt(this.id, 'EML.SetIMAP(' + server + '\f' + port);
  }
  public Send(
    subject: string,
    body: string,
    sender: string,
    recipients: string,
    attach: string
  ) {
    prompt(
      this.id,
      'EML.Send(\f' +
        subject +
        '\f' +
        body +
        '\f' +
        sender +
        '\f' +
        recipients +
        '\f' +
        attach
    );
  }
  public Receive(folder: string, maxCount: string, filter: string) {
    prompt(this.id, 'EML.Receive(' + folder + '\f' + maxCount + '\f' + filter);
  }
  public SetOnStatus(callback: any) {
    prompt(this.id, 'EML.SetOnStatus(' + _Cbm(callback));
  }
  public SetOnMessage(callback: any) {
    prompt(this.id, 'EML.SetOnMessage(' + _Cbm(callback));
  }
}
