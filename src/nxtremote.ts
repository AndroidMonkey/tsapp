import { SObj } from './sobj';

export class NxtRemote extends SObj {
  constructor(id: string, nxtHelper) {
    super(id);
    var nxtHelp = nxtHelper;
  }
  public GetType() {
    return 'NxtRemote';
  }
  public Connect(name) {
    return prompt(this.id, 'Nxt.Connect(' + name) === 'true';
  }
  public SetOnConnect(callback) {
    prompt(this.id, 'Nxt.SetOnConnect(' + _Cbm(callback));
  }
  public SetOnConnectEx(callback) {
    prompt(this.id, 'Nxt.SetOnConnect(' + callback);
  }
  public Disconnect() {
    prompt(this.id, 'Nxt.Disconnect(');
  }
  public IsEnabled() {
    return prompt(this.id, 'Nxt.IsEnabled(') === 'true';
  }
  public IsBluetoothEnabled() {
    return prompt(this.id, 'Nxt.IsEnabled(') === 'true';
  }
  public RequestEnable() {
    prompt(this.id, 'Nxt.RequestEnable(');
  }
  public IsPaired(name) {
    return prompt(this.id, 'Nxt.IsPaired(' + name) === 'true';
  }
  public IsConnected() {
    return prompt(this.id, 'Nxt.IsConnected(') === 'true';
  }
  public IsMotorIdle(motor) {
    return prompt(this.id, 'Nxt.IsMotorIdle(' + motor) === 'true';
  }
  public GetRotationCount(motor) {
    return parseFloat(prompt(this.id, 'Nxt.GetRotationCount(\f' + motor));
  }
  public Beep(freq, dur) {
    prompt(this.id, 'Nxt.Beep(' + freq + '\f' + dur);
  }
  public SetInvert(invert) {
    prompt(this.id, 'Nxt.SetInvert(' + invert);
  }
  public Drive(motors, power, rotations, options) {
    prompt(
      this.id,
      'Nxt.Drive(' + motors + '\f' + power + '\f' + rotations + '\f' + options
    );
  }
  public Brake(motors) {
    prompt(this.id, 'Nxt.Brake(' + motors);
  }
  public Stop(motors) {
    prompt(this.id, 'Nxt.Stop(' + motors);
  }
  public Reset(motors) {
    prompt(this.id, 'Nxt.Reset(\f' + motors);
  }
  public SendMail(mailbox, type, msg) {
    prompt(this.id, 'Nxt.SendMail(' + mailbox + '\f' + type + '\f' + msg);
  }
  public ReadMail(mailbox, type, remove) {
    var ret = prompt(
      this.id,
      'Nxt.ReadMail(' + mailbox + '\f' + type + '\f' + remove
    );
    if (type.toLowerCase() === 'number') return parseFloat(ret);
    else if (type.toLowerCase() === 'logic') return ret == 'true';
    else return ret;
  }
  public SetLampColor(input, clr) {
    prompt(this.id, 'Nxt.SetLampColor(' + input + '\f' + clr);
  }
  public PlaySoundFile(file, repeat) {
    prompt(this.id, 'Nxt.PlaySoundFile(' + file + '\f' + repeat);
  }
  public ReadSoundSensor(input, mode) {
    return parseInt(
      prompt(this.id, 'Nxt.ReadSoundSensor(' + input + '\f' + mode)
    );
  }
  public ReadColorSensor(input, mode) {
    return parseInt(
      prompt(this.id, 'Nxt.ReadColorSensor(' + input + '\f' + mode)
    );
  }
  public ToColorName(val) {
    return prompt(this.id, 'Nxt.ToColorName(' + val);
  }
  public ReadLightSensor(input, active) {
    return parseInt(
      prompt(this.id, 'Nxt.ReadLightSensor(' + input + '\f' + active)
    );
  }
  public ReadTouchSensor(input) {
    return prompt(this.id, 'Nxt.ReadTouchSensor(' + input) === '1';
  }
  public ReadDistanceSensor(input) {
    return parseInt(prompt(this.id, 'Nxt.ReadDistanceSensor(' + input));
  }
  public GetCurrentProgram() {
    return prompt(this.id, 'Nxt.GetCurrentProgram(');
  }
  public StartProgram(name) {
    prompt(this.id, 'Nxt.StartProgram(' + name);
  }
  public StopProgram() {
    prompt(this.id, 'Nxt.StopProgram(');
  }
  public FileFindFirst(pattern) {
    if (_inf) _inf.Release();
    var ret = prompt(this.id, 'Nxt.FileFindFirst(' + pattern);
    if (ret) _inf = new Inf(ret);
    else _inf = null;
    return _inf;
  }
  public FileFindNext(handle) {
    if (_inf) _inf.Release();
    var ret = prompt(this.id, 'Nxt.FileFindNext(' + handle);
    if (ret) _inf = new Inf(ret);
    else _inf = null;
    return _inf;
  }

  //Helper functions.
  public ShowDevices() {
    nxtHelp.nxt_ShowDevices();
  }
  public CheckConnection() {
    return nxtHelp.nxt_CheckConnection();
  }
  public SetOnConnected(callback) {
    nxtHelp.nxt_OnConnected = callback;
  }
  public GetBtName() {
    return nxtHelp.nxtName;
  }
  public GetBtAddress() {
    return nxtHelp.nxtAddress;
  }
}
