import { SObj } from './sobj';

export class Crypt extends SObj {
  constructor(id: string) {
    super(id);
  }
  public GetType(): string {
    return 'Crypt';
  }
  public Hash(text: string, mode: string, options: string): StringOrNull {
    return prompt(this.id, 'Crp.Hash\f' + text + '\f' + mode + '\f' + options);
  }
  public Encrypt(text: string, password: string): StringOrNull {
    return prompt(this.id, 'Crp.Encrypt\f' + text + '\f' + password);
  }
  public Decrypt(text: string, password: string): StringOrNull {
    return prompt(this.id, 'Crp.Decrypt\f' + text + '\f' + password);
  }
}
