import { Obj } from './obj';

export class Scroller extends Obj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'Scroller';
  }
  public AddChild(child: { id: any }) {
    prompt(this.id, 'Scr.AddChild(\f' + (child ? child.id : null));
  }
  public RemoveChild(child: { id: any }) {
    prompt(this.id, 'Scr.RemoveChild(\f' + (child ? child.id : null));
  }
  public DestroyChild(child: { id: any }) {
    prompt(this.id, 'Scr.DestroyChild(\f' + (child ? child.id : null));
  }
  public ScrollTo(x: string, y: string) {
    prompt(this.id, 'Scr.ScrollTo\f' + x + '\f' + y);
  }
  public ScrollBy(x: string, y: string) {
    prompt(this.id, 'Scr.ScrollBy\f' + x + '\f' + y);
  }
  public GetScrollX() {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Scr.GetScrollX('));
  }
  public GetScrollY() {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Scr.GetScrollY('));
  }
}
