import { SObj } from './sobj';

export class AudioRecorder extends SObj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'AudioRecorder';
  }
  public SetFile(file: string) {
    prompt(this.id, 'Rec.SetFile(' + file);
  }
  public SetFrequency(freq: string) {
    prompt(this.id, 'Rec.SetFrequency(\f' + freq);
  }
  public Start() {
    prompt(this.id, 'Rec.Start(');
  }
  public Stop() {
    prompt(this.id, 'Rec.Stop(');
  }
  public Pause() {
    prompt(this.id, 'Rec.Pause(');
  }
  public GetRMS() {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Rec.GetRMS('));
  }
  public GetPeak() {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Rec.GetPeak('));
  }
  public GetData() {
    // @ts-ignore TS2345
    return JSON.parse(prompt(this.id, 'Rec.GetData('));
  }
}
