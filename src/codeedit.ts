import { Obj } from './obj';

export class CodeEdit extends Obj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'CodeEdit';
  }
  public GetText() {
    return prompt(this.id, 'Cde.GetText(');
  }
  public GetSelectedText() {
    return prompt(this.id, 'Cde.GetSelectedText(');
  }
  public SetText(txt: string) {
    prompt(this.id, 'Cde.SetText(\f' + txt);
  }
  public SetHtml(html: string) {
    prompt(this.id, 'Cde.SetText(\f' + html);
  }
  public Undo() {
    prompt(this.id, 'Cde.Undo(');
  }
  public Redo() {
    prompt(this.id, 'Cde.Redo(');
  }
  public Copy() {
    prompt(this.id, 'Cde.Copy(');
  }
  public Cut() {
    prompt(this.id, 'Cde.Cut(');
  }
  public Paste() {
    prompt(this.id, 'Cde.Paste(');
  }
  public SetSelectMode(onOff: string) {
    prompt(this.id, 'Cde.SetSelectMode(\f' + onOff);
  }
  public GetSelectMode() {
    return prompt(this.id, 'Cde.GetSelectMode(') === 'true';
  }
  public SelectAll() {
    prompt(this.id, 'Cde.SelectAll(');
  }
  public Search(
    text: string,
    dir: string,
    matchCase: string,
    wholeWord: string
  ) {
    prompt(
      this.id,
      'Cde.Search(\f' + text + '\f' + dir + '\f' + matchCase + '\f' + wholeWord
    );
  }
  public Replace(text: string) {
    prompt(this.id, 'Cde.Replace(\f' + text);
  }
  public ReplaceAll(
    text: string,
    newText: string,
    matchCase: string,
    wholeWord: string
  ) {
    prompt(
      this.id,
      'Cde.ReplaceAll(\f' +
        text +
        '\f' +
        newText +
        '\f' +
        matchCase +
        '\f' +
        wholeWord
    );
  }
  public SetUseKeyboard(onOff: string) {
    prompt(this.id, 'Cde.SetUseKeyboard(\f' + onOff);
  }
  public SetNavigationMethod(method: string) {
    prompt(this.id, 'Cde.SetNavigationMethod(\f' + method);
  }
  public ClearHistory() {
    prompt(this.id, 'Cde.ClearHistory(');
  }
  public SetTextSize(size: string, mode: string) {
    prompt(this.id, 'Cde.SetTextSize(\f' + size + '\f' + mode);
  }
  public SetTextColor(color: string) {
    prompt(this.id, 'Cde.SetTextColor(\f' + color);
  }
  public SetColorScheme(scheme: string) {
    prompt(this.id, 'Cde.SetColorScheme(\f' + scheme);
  }
  public GetCursorLine() {
    // @ts-ignore TS2345
    return parseInt(prompt(this.id, 'Cde.GetCursorLine('), 10);
  }
  public GetLineStart(line: string) {
    // @ts-ignore TS2345
    return parseInt(prompt(this.id, 'Cde.GetLineStart(' + line), 10);
  }
  public HighlightLine(line: string) {
    prompt(this.id, 'Cde.HighlightLine(\f' + line);
  }
  public SetCursorPos(pos: string) {
    prompt(this.id, 'Cde.SetCursorPos(' + pos);
  }
  public GetCursorPos() {
    // @ts-ignore TS2345
    return parseInt(prompt(this.id, 'Cde.GetCursorPos('), 10);
  }
  public InsertText(text: string, start: string) {
    prompt(this.id, 'Cde.InsertText(\f' + text + '\f' + start);
  }
  public ReplaceText(text: string, start: string, end: string) {
    prompt(this.id, 'Cde.ReplaceText(\f' + text + '\f' + start + '\f' + end);
  }
  public SetSelection(start: string, stop: string) {
    prompt(this.id, 'Cde.SetSelection(\f' + start + '\f' + stop);
  }
  public SetLanguage(ext: string) {
    prompt(this.id, 'Cde.SetLanguage(\f' + ext);
  }
  public SetOnChange(callback: any) {
    prompt(this.id, 'Cde.SetOnChange(\f' + _Cbm(callback));
  }
  public SetOnDoubleTap(callback: any) {
    prompt(this.id, 'Cde.SetOnDoubleTap(\f' + _Cbm(callback));
  }
  public SetOnKey(callback: any) {
    prompt(this.id, 'Cde.SetOnKey(\f' + _Cbm(callback));
  }
}
