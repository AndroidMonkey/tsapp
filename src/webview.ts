import { Obj } from './obj';

export class WebView extends Obj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'WebView';
  }
  public SetOnProgress(callback: any) {
    prompt(this.id, 'Web.SetOnProgress(' + _Cbm(callback));
  }
  public SetOnError(callback: any) {
    prompt(this.id, 'Web.SetOnError(\f' + _Cbm(callback));
  }
  public SetOnConsole(callback: any) {
    prompt(this.id, 'Web.SetOnConsole(\f' + _Cbm(callback));
  }
  public LoadHtml(html: string, base: string, options: string) {
    prompt(this.id, 'Web.LoadHtml(\f' + html + '\f' + base + '\f' + options);
  }
  public LoadUrl(url: string, options: string) {
    prompt(this.id, 'Web.LoadUrl(\f' + url + '\f' + options);
  }
  public Reload() {
    prompt(this.id, 'Web.Reload(');
  }
  public Back() {
    prompt(this.id, 'Web.Back(');
  }
  public Forward() {
    prompt(this.id, 'Web.Forward(');
  }
  public CanGoBack() {
    return prompt(this.id, 'Web.CanGoBack(') === 'true';
  }
  public CanGoForward() {
    return prompt(this.id, 'Web.CanGoForward(') === 'true';
  }
  public Execute(code: string, callback: any) {
    prompt(this.id, 'Web.Execute(\f' + code + '\f' + _Cbm(callback));
  }
  public ClearHistory() {
    prompt(this.id, 'Web.ClearHistory(');
  }
  public GetUrl() {
    return prompt(this.id, 'Web.GetUrl(');
  }
  public Capture(file: string) {
    prompt(this.id, 'Web.Capture(\f' + file);
  }
  public Print() {
    prompt(this.id, 'Web.Print(\f');
  }
  public SetUserAgent(agent: string) {
    prompt(this.id, 'Web.SetUserAgent(\f' + agent);
  }
  public SetUserCreds(name: string, password: string) {
    prompt(this.id, 'Web.SetUserCreds(\f' + name + '\f' + password);
  }
  public SimulateKey(keyName: string, modifiers: string, pause: string) {
    prompt(
      this.id,
      'Web.SimulateKey(\f' + keyName + '\f' + modifiers + '\f' + pause
    );
  }
  public SetRedirect(urlFrom: string, urlTo: string) {
    prompt(this.id, 'Web.SetRedirect(\f' + urlFrom + '\f' + urlTo);
  }
  public SetTouchMode(mode: string) {
    prompt(this.id, 'Web.SetTouchMode(\f' + mode);
  }
}
