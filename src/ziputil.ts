import { SObj } from './sobj';

export class ZipUtil extends SObj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'ZipUtil';
  }
  public Open(file: string) {
    prompt(this.id, 'Zip.Open(\f' + file);
  }
  public Create(file: string) {
    prompt(this.id, 'Zip.Create(\f' + file);
  }
  public Close() {
    prompt(this.id, 'Zip.Close(');
  }
  public List(path: string) {
    return prompt(this.id, 'Zip.List(\f' + path);
  }
  public Extract(name: string, file: string) {
    prompt(this.id, 'Zip.Extract(\f' + name + '\f' + file);
  }
  public AddFile(name: string, file: string) {
    prompt(this.id, 'Zip.AddFile(\f' + name + '\f' + file);
  }
  public AddText(name: string, text: string) {
    prompt(this.id, 'Zip.AddText(\f' + name + '\f' + text);
  }
  public CreateKey(file: string, pass: string, name: string, org: string) {
    prompt(
      this.id,
      'Zip.CreateKey(\f' + file + '\f' + pass + '\f' + name + '\f' + org
    );
  }
  public CreateDebugKey(file: string) {
    prompt(this.id, 'Zip.CreateDebugKey(\f' + file);
  }
  public Sign(fileIn: string, fileOut: string, keyStore: string, pass: string) {
    return (
      prompt(
        this.id,
        'Zip.Sign(\f' + fileIn + '\f' + fileOut + '\f' + keyStore + '\f' + pass
      ) === 'true'
    );
  }
  public UpdateManifest(
    fileIn: string,
    fileOut: string,
    packageName: string,
    appName: string,
    permissions: string,
    options: string
  ) {
    prompt(
      this.id,
      'Zip.UpdateManifest(\f' +
        fileIn +
        '\f' +
        fileOut +
        '\f' +
        packageName +
        '\f' +
        appName +
        '\f' +
        permissions +
        '\f' +
        options
    );
  }
}
