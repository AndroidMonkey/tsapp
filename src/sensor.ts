import { SObj } from './sobj';

export class Sensor extends SObj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'Sensor';
  }
  public SetOnChange(callback: any) {
    prompt(this.id, 'Sns.SetOnChange(' + _Cbm(callback));
  }
  public SetMinChange(min: string) {
    prompt(this.id, 'Sns.SetMinChange(' + min);
  }
  public SetMaxRate(rate: string) {
    prompt(this.id, 'Sns.SetMaxRate(\f' + rate);
  }
  public GetNames() {
    return prompt(this.id, 'Sns.GetNames(');
  }
  public Start() {
    prompt(this.id, 'Sns.Start(');
  }
  public Stop() {
    prompt(this.id, 'Sns.Stop(');
  }
  public GetAzimuth() {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Sns.GetAzimuth('));
  }
  public GetPitch() {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Sns.GetPitch('));
  }
  public GetRoll() {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Sns.GetRoll('));
  }
  public GetValues() {
    // @ts-ignore TS2345
    return JSON.parse(prompt(this.id, 'Sns.GetValues('));
  }
}
