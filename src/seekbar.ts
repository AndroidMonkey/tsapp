import { Obj } from './obj';

export class SeekBar extends Obj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'SeekBar';
  }
  public SetOnTouch(callback: any) {
    prompt(this.id, 'Skb.SetOnClick(' + _Cbm(callback));
  }
  public SetOnChange(callback: any) {
    prompt(this.id, 'Skb.SetOnClick(' + _Cbm(callback));
  }
  public GetValue() {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Skb.GetValue('));
  }
  public SetValue(val: string) {
    prompt(this.id, 'Skb.SetValue(' + val);
  }
  public SetRange(range: string) {
    prompt(this.id, 'Skb.SetRange(' + range);
  }
  public SetMaxRate(rate: string) {
    prompt(this.id, 'Skb.SetMaxRate(' + rate);
  }
  public SetColorFilter(clr: string, mode: string) {
    prompt(this.id, 'Skb.SetColorFilter(\f' + clr + '\f' + mode);
  }
  public AdjustColor(hue: string, sat: string, bright: string, cont: string) {
    prompt(
      this.id,
      'Skb.AdjustColor(\f' + hue + '\f' + sat + '\f' + bright + '\f' + cont
    );
  }
}
