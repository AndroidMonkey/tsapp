export class SObj {
  public data: {};
  constructor(public id: string) {
    _map[id] = this;
    this.data = {};
  }
  public Destroy(): void {
    prompt(this.id, 'SObj.Release(');
    _map[this.id] = null;
  }
  public Release(): void {
    prompt(this.id, 'SObj.Release(');
    _map[this.id] = null;
  }
  public Method(
    name: string,
    types: string,
    p1: string,
    p2: string,
    p3: string,
    p4: string
  ): string | null {
    return prompt(
      this.id,
      'SObj.Method(\f' +
        name +
        '\f' +
        types +
        '\f' +
        p1 +
        '\f' +
        p2 +
        '\f' +
        p3 +
        '\f' +
        p4
    );
  }
}
