export interface IControl {
  _parent: IControl;
  Destroy(): void;
  Release(): void;
}
