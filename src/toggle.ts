import { Obj } from './obj';

export class Toggle extends Obj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'Toggle';
  }
  public SetOnTouch(callback: any) {
    prompt(this.id, 'Tgl.SetOnClick(' + _Cbm(callback));
  }
  public SetText(text: string) {
    prompt(this.id, 'Tgl.SetText(' + text);
  }
  public GetText() {
    return prompt(this.id, 'Tgl.GetText(');
  }
  public SetTextColor(clr: string) {
    prompt(this.id, 'Tgl.SetTextColor(' + clr);
  }
  public SetTextSize(size: string, mode: string) {
    prompt(this.id, 'Tgl.SetTextSize(\f' + size + '\f' + mode);
  }
  public GetTextSize(mode: string) {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Tgl.GetTextSize(\f' + mode));
  }
  public SetChecked(checked: string) {
    prompt(this.id, 'Tgl.SetChecked(' + checked);
  }
  public GetChecked() {
    return prompt(this.id, 'Tgl.GetChecked(') === 'true';
  }
  public SetStyle(
    clr1: string,
    clr2: string,
    radius: string,
    strokeClr: string,
    strokeWidth: string,
    shadow: string,
    checkClr: string
  ) {
    prompt(
      this.id,
      'Tgl.SetStyle(\f' +
        clr1 +
        '\f' +
        clr2 +
        '\f' +
        radius +
        '\f' +
        strokeClr +
        '\f' +
        strokeWidth +
        '\f' +
        shadow +
        '\f' +
        checkClr
    );
  }
}
