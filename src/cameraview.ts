import { Obj } from './obj';

export class CameraView extends Obj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'CameraView';
  }
  public StartPreview() {
    prompt(this.id, 'Cam.StartPreview(');
  }
  public StopPreview() {
    prompt(this.id, 'Cam.StopPreview(');
  }
  public Focus() {
    prompt(this.id, 'Cam.Focus(');
  }
  public SetFocusMode(mode: string) {
    prompt(this.id, 'Cam.SetFocusMode(\f' + mode);
  }
  public SetZoom(level: string) {
    prompt(this.id, 'Cam.SetZoom(\f' + level);
  }
  public GetZoom() {
    // @ts-ignore TS2345
    return parseInt(prompt(this.id, 'Cam.GetZoom('), 10);
  }
  public GetMaxZoom() {
    // @ts-ignore TS2345
    return parseInt(prompt(this.id, 'Cam.GetMaxZoom('), 10);
  }
  public SetOrientation(angle: string) {
    prompt(this.id, 'Cam.SetOrientation(\f' + angle);
  }
  public SetPostRotation(angle: string) {
    prompt(this.id, 'Cam.SetPostRotation(\f' + angle);
  }
  public SetPictureSize(width: string, height: string) {
    prompt(this.id, 'Cam.SetPictureSize(\f' + width + '\f' + height);
  }
  public SetVideoSize(width: string, height: string) {
    prompt(this.id, 'Cam.SetVideoSize(\f' + width + '\f' + height);
  }
  public GetPictureSizes() {
    return prompt(this.id, 'Cam.GetPictureSizes(');
  }
  public SetColorEffect(effect: string) {
    prompt(this.id, 'Cam.SetColorEffect(\f' + effect);
  }
  public GetColorEffects() {
    return prompt(this.id, 'Cam.GetColorEffects(');
  }
  public TakePicture(file: string) {
    prompt(this.id, 'Cam.TakePicture(' + file);
  }
  public Record(file: string, seconds: string) {
    prompt(this.id, 'Cam.Record(\f' + file + '\f' + seconds);
  }
  public IsRecording() {
    return prompt(this.id, 'Cam.IsRecording(') === 'true';
  }
  public Stop() {
    prompt(this.id, 'Cam.Stop(');
  }
  public Stream(
    ip: string,
    port: string,
    quality: string,
    fps: string,
    mtu: string
  ) {
    prompt(
      this.id,
      'Cam.Stream(' +
        ip +
        '\f' +
        port +
        '\f' +
        quality +
        '\f' +
        fps +
        '\f' +
        mtu
    );
  }
  public HasFlash() {
    return prompt(this.id, 'Cam.HasFlash(') === 'true';
  }
  public SetFlash(onoff: string) {
    prompt(this.id, 'Cam.SetFlash(' + onoff);
  }
  public SetSound(onoff: string) {
    prompt(this.id, 'Cam.SetSound(' + onoff);
  }
  public GetImageWidth() {
    // @ts-ignore TS2345
    return parseInt(prompt(this.id, 'Cam.GetImageWidth('), 10);
  }
  public GetImageHeight() {
    // @ts-ignore TS2345
    return parseInt(prompt(this.id, 'Cam.GetImageHeight('), 10);
  }
  public GetCameraCount() {
    // @ts-ignore TS2345
    return parseInt(prompt(this.id, 'Cam.GetCameraCount('), 10);
  }
  public SetPreviewImage(img: { id: any }) {
    prompt(this.id, 'Cam.SetPreviewImage(' + (img ? img.id : null));
  }
  public SetDuplicateImage(img1: { id: any }, img2: { id: any }) {
    prompt(
      this.id,
      'Cam.SetDuplicateImage(\f' +
        (img1 ? img1.id : null) +
        '\f' +
        (img2 ? img2.id : null)
    );
  }
  public MotionMosaic(
    xtiles: string,
    ytiles: string,
    sensitivity: string,
    minPeriod: string,
    img: { id: any }
  ) {
    prompt(
      this.id,
      'Cam.MotionMosaic(' +
        xtiles +
        '\f' +
        ytiles +
        '\f' +
        sensitivity +
        '\f' +
        minPeriod +
        '\f' +
        (img ? img.id : null)
    );
  }
  public ReportColors(
    list: string,
    callback: any,
    sampSize: string,
    maxRate: string
  ) {
    prompt(
      this.id,
      'Cam.ReportColors(\f' +
        list +
        '\f' +
        _Cbm(callback) +
        '\f' +
        sampSize +
        '\f' +
        maxRate
    );
  }
  public AutoCapture(path: string, file: string, maxCount: string) {
    prompt(this.id, 'Cam.AutoCapture(' + path + '\f' + file + '\f' + maxCount);
  }
  public SetOnReady(callback: any) {
    prompt(this.id, 'Cam.SetOnReady(' + _Cbm(callback));
  }
  public SetOnMotion(callback: any) {
    prompt(this.id, 'Cam.SetOnMotion(' + _Cbm(callback));
  }
  public SetOnPicture(callback: any) {
    prompt(this.id, 'Cam.SetOnPicture\f' + _Cbm(callback));
  }
  public SetOnFocus(callback: any) {
    prompt(this.id, 'Cam.SetOnFocus\f' + _Cbm(callback));
  }
  public GetPixelData(
    format: string,
    left: string,
    top: string,
    width: string,
    height: string
  ) {
    return prompt(
      this.id,
      'Cam.GetPixelData(\f' +
        format +
        '\f' +
        left +
        '\f' +
        top +
        '\f' +
        width +
        '\f' +
        height
    );
  }
  public FindFaces(max: string) {
    // @ts-ignore TS2345
    return JSON.parse(prompt(this.id, 'Cam.FindFaces(\f' + max));
  }
  public GetParameters() {
    return prompt(this.id, 'Cam.GetParams(\f');
  }
  public SetParameter(name: string, value: string) {
    if (typeof value === 'string') {
      prompt(this.id, 'Cam.SetParam(\f' + name + '\f' + value);
    } else {
      prompt(this.id, 'Cam.SetParamNum(\f' + name + '\f' + value);
    }
  }
}
