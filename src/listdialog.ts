import { Obj } from './obj';

export class ListDialog extends Obj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'ListDialog';
  }
  public Show() {
    prompt(this.id, 'Ldg.Show(');
  }
  public Hide() {
    prompt(this.id, 'Ldg.Hide(');
  }
  public Dismiss() {
    prompt(this.id, 'Ldg.Dismiss(');
  }
  public SetOnTouch(callback: any) {
    prompt(this.id, 'Ldg.SetOnClick(' + _Cbm(callback));
  }
  public SetTitle(title: string) {
    prompt(this.id, 'Ldg.SetTitle(\f' + title);
  }
  public SetBackColor(clr: string) {
    prompt(this.id, 'Ldg.SetBackColor(\f' + clr);
  }
  public SetBackground(file: string, options: string) {
    prompt(this.id, 'Ldg.SetBackground(\f' + file + '\f' + options);
  }
  public SetTextColor(clr: string) {
    prompt(this.id, 'Ldg.SetTextColor(\f' + clr);
  }
  public AdjustColor(hue: string, sat: string, bright: string, cont: string) {
    prompt(
      this.id,
      'Ldg.AdjustColor(\f' + hue + '\f' + sat + '\f' + bright + '\f' + cont
    );
  }
  public SetSize(width: string, height: string, options: string) {
    prompt(this.id, 'Ldg.SetSize(\f' + width + '\f' + height + '\f' + options);
  }
}
