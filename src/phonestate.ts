import { SObj } from './sobj';

export class PhoneState extends SObj {
  constructor(id: string) {
    super(id);
  }
  public GetType(): string {
    return 'PhoneState';
  }
  public SetOnChange(callback: any): void {
    prompt(this.id, 'Pst.SetOnChange(' + _Cbm(callback));
  }
  public Start(): void {
    prompt(this.id, 'Pst.Start(');
  }
  public Stop(): void {
    prompt(this.id, 'Pst.Stop(');
  }
}
