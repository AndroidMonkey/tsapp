import { SObj } from './sobj';

export class Notification extends SObj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'Notification';
  }
  public SetMessage(ticker: string, title: string, text: string) {
    prompt(this.id, 'Not.SetMessage(\f' + ticker + '\f' + title + '\f' + text);
  }
  public Notify(id: string) {
    prompt(this.id, 'Not.Notify(\f' + id);
  }
  public Cancel(id: string) {
    prompt(this.id, 'Not.Cancel(\f' + id);
  }
  public SetLargeImage(image: any) {
    if (image.id) {
      prompt(this.id, 'Not.SetLargeImage(\f' + (image ? image.id : null));
    } else {
      prompt(this.id, 'Not.SetLargeImageFile(\f' + image);
    }
  }
  public SetSmallImage(image: any) {
    if (image.id) {
      prompt(this.id, 'Not.SetSmallImage(\f' + (image ? image.id : null));
    } else {
      prompt(this.id, 'Not.SetSmallImageFile(\f' + image);
    }
  }
  public SetLights(color: string, onMs: string, offMs: string) {
    prompt(this.id, 'Not.SetLights(\f' + color + '\f' + onMs + '\f' + offMs);
  }
}
