import { Obj } from './obj';

export class ListView extends Obj {
  constructor(id: string) {
    super(id);
  }
  public GetType(): string {
    return 'ListView';
  }
  public Show(): void {
    prompt(this.id, 'Lvw.Show(');
  }
  public SetOnTouch(callback: any): void {
    prompt(this.id, 'Lvw.SetOnClick(' + _Cbm(callback));
  }
}
