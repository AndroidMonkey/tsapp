import { SObj } from './sobj';

export class Synth extends SObj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'Synth';
  }
  public Start() {
    prompt(this.id, 'Syn.Start(');
  }
  public Stop() {
    prompt(this.id, 'Syn.Stop(');
  }
  public PlayTone(freq: string, dur: string) {
    prompt(this.id, 'Syn.PlayTone(' + freq + '\f' + dur);
  }
  public PlayNote(note: string) {
    prompt(this.id, 'Syn.PlayNote(' + note);
  }
  public PlayMidiTune(tune: string) {
    prompt(this.id, 'Syn.PlayMidiTune(' + tune);
  }
  public SetFrequency(freq: string) {
    prompt(this.id, 'Syn.SetFrequency(' + freq);
  }
  public SetWaveShape(shape: string) {
    prompt(this.id, 'Syn.SetWaveShape(' + shape);
  }
  public SetVolume(left: string, right: string) {
    prompt(this.id, 'Syn.SetVolume(' + left + '\f' + right);
  }
  public SetNoteLength(dur: string) {
    prompt(this.id, 'Syn.SetNoteLength(' + dur);
  }

  public SetVca(
    attack: string,
    decay: string,
    sustain: string,
    release: string
  ) {
    prompt(
      this.id,
      'Syn.SetVca\f' + attack + '\f' + decay + '\f' + sustain + '\f' + release
    );
  }
  public SetVcaAttack(attack: string) {
    prompt(this.id, 'Syn.SetVcaAttack(' + attack);
  }
  public SetVcaDecay(decay: string) {
    prompt(this.id, 'Syn.SetVcaDecay(' + decay);
  }
  public SetVcaSustain(sustain: string) {
    prompt(this.id, 'Syn.SetVcaSustain(' + sustain);
  }
  public SetVcaRelease(release: string) {
    prompt(this.id, 'Syn.SetVcaRelease(' + release);
  }
  public SetVcaEnabled(enable: string) {
    prompt(this.id, 'Syn.SetVcaEnabled(' + enable);
  }

  public SetVcf(
    attack: string,
    decay: string,
    sustain: string,
    release: string,
    cuttoff: string,
    resonance: string,
    depth: string
  ) {
    prompt(
      this.id,
      'Syn.SetVcf\f' +
        attack +
        '\f' +
        decay +
        '\f' +
        sustain +
        '\f' +
        release +
        '\f' +
        cuttoff +
        '\f' +
        resonance +
        '\f' +
        depth
    );
  }
  public SetVcfAttack(attack: string) {
    prompt(this.id, 'Syn.SetVcfAttack(' + attack);
  }
  public SetVcfDecay(decay: string) {
    prompt(this.id, 'Syn.SetVcfDecay(' + decay);
  }
  public SetVcfSustain(sustain: string) {
    prompt(this.id, 'Syn.SetVcfSustain(' + sustain);
  }
  public SetVcfRelease(release: string) {
    prompt(this.id, 'Syn.SetVcfRelease(' + release);
  }
  public SetVcfCutoff(cuttoff: string) {
    prompt(this.id, 'Syn.SetVcfCutoff(' + cuttoff);
  }
  public SetVcfResonance(resonance: string) {
    prompt(this.id, 'Syn.SetVcfResonance(' + resonance);
  }
  public SetVcfDepth(depth: string) {
    prompt(this.id, 'Syn.SetVcfDepth(' + depth);
  }
  public SetVcfEnabled(enable: string) {
    prompt(this.id, 'Syn.SetVcfEnabled(' + enable);
  }

  public SetPhaser(
    drywet: string,
    rate: string,
    range: string,
    feedback: string
  ) {
    prompt(
      this.id,
      'Syn.SetPhaser\f' + drywet + '\f' + rate + '\f' + range + '\f' + feedback
    );
  }
  public SetPhaserDryWet(drywet: string) {
    prompt(this.id, 'Syn.SetPhaserDryWet(' + drywet);
  }
  public SetPhaserRate(rate: string) {
    prompt(this.id, 'Syn.SetPhaserRate(' + rate);
  }
  public SetPhaserRange(range: string) {
    prompt(this.id, 'Syn.SetPhaserRange(' + range);
  }
  public SetPhaserFeedback(feedback: string) {
    prompt(this.id, 'Syn.SetPhaserFeedback(' + feedback);
  }
  public SetPhaserEnabled(enable: string) {
    prompt(this.id, 'Syn.SetPhaserEnabled(' + enable);
  }

  public SetDelay(ms: string) {
    prompt(this.id, 'Syn.SetDelay(' + ms);
  }
  public SetFeedback(feedback: string) {
    prompt(this.id, 'Syn.SetFeedback(' + feedback);
  }
  public SetDelayEnabled(enable: string) {
    prompt(this.id, 'Syn.SetDelayEnabled(' + enable);
  }
}
