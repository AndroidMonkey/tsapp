import { Obj } from './obj';

export class Text extends Obj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'Text';
  }
  public SetText(text: string) {
    prompt(this.id, 'Txt.SetText(' + text);
  }
  public SetHtml(html: string) {
    prompt(this.id, 'Txt.SetHtml(' + html);
  }
  public GetHtml() {
    return prompt(this.id, 'Txt.GetHtml(');
  }
  public Log(msg: string, options: string) {
    prompt(this.id, 'Txt.Log(\f' + msg + '\f' + options);
  }
  public SetLog(maxLines: string) {
    prompt(this.id, 'Txt.SetLog(\f' + maxLines);
  }
  public SetTextSize(size: string, mode: string) {
    prompt(this.id, 'Txt.SetTextSize(\f' + size + '\f' + mode);
  }
  public GetTextSize(mode: string) {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Txt.GetTextSize(\f' + mode));
  }
  public GetText() {
    return prompt(this.id, 'Txt.GetText(');
  }
  public SetTextColor(color: string) {
    prompt(this.id, 'Txt.SetTextColor(' + color);
  }
  public SetFontFile(file: string) {
    prompt(this.id, 'Txt.SetFontFile(\f' + file);
  }
  public GetLineCount() {
    // @ts-ignore TS2345
    return parseInt(prompt(this.id, 'Txt.GetLineCount('), 10);
  }
  public GetMaxLines() {
    // @ts-ignore TS2345
    return parseInt(prompt(this.id, 'Txt.GetMaxLines('), 10);
  }
  public GetLineTop(line: string) {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Txt.GetLineTop(' + line));
  }
  public GetLineStart(line: string) {
    // @ts-ignore TS2345
    return parseInt(prompt(this.id, 'Txt.GetLineStart(' + line), 10);
  }
  public SetEllipsize(mode: string) {
    prompt(this.id, 'Txt.SetEllipsize(\f' + mode);
  }
  public SetTextShadow(radius: string, dx: string, dy: string, color: string) {
    prompt(
      this.id,
      'Txt.SetTextShadow(\f' + radius + '\f' + dx + '\f' + dy + '\f' + color
    );
  }
  public SetOnTouch(callback: any) {
    prompt(this.id, 'Txt.SetOnTouch(' + _Cbm(callback));
  }
  public SetOnTouchUp(callback: any) {
    prompt(this.id, 'Txt.SetOnTouchUp(' + _Cbm(callback));
  }
  public SetOnTouchMove(callback: any) {
    prompt(this.id, 'Txt.SetOnTouchMove(' + _Cbm(callback));
  }
  public SetOnTouchDown(callback: any) {
    prompt(this.id, 'Txt.SetOnTouchDown(' + _Cbm(callback));
  }
  public SetOnLongTouch(callback: any) {
    prompt(this.id, 'Txt.SetOnLongTouch(' + _Cbm(callback));
  }
  public SetTouchable(touchable: string) {
    prompt(this.id, 'Txt.SetTouchable(' + touchable);
  }
}
