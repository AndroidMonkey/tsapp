import { SObj } from './sobj';

export class SpeechRec extends SObj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'SpeechRec';
  }
  public Destroy() {
    prompt(this.id, 'Spr.Destroy(');
    _map[this.id] = null;
  }
  public Recognize() {
    return prompt(this.id, 'Spr.Recognize(\f');
  }
  public Stop() {
    prompt(this.id, 'Spr.Stop(');
  }
  public Cancel() {
    prompt(this.id, 'Spr.Cancel(');
  }
  public GetRMS() {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Spr.GetRMS('));
  }
  public IsListening() {
    return prompt(this.id, 'Spr.IsListening(') === 'true';
  }
  public SetOnReady(callback: any) {
    prompt(this.id, 'Spr.SetOnReady(\f' + _Cbm(callback));
  }
  public SetOnResult(callback: any) {
    prompt(this.id, 'Spr.SetOnResult(\f' + _Cbm(callback));
  }
  public SetOnError(callback: any) {
    prompt(this.id, 'Spr.SetOnError(\f' + _Cbm(callback));
  }
}
