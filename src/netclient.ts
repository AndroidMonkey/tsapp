import { SObj } from './sobj';

export class NetClient extends SObj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'NetClient';
  }
  public Connect(address: string, port: string) {
    return prompt(this.id, 'Net.Connect(' + address + '\f' + port) === 'true';
  }
  public SetOnConnect(callback: any) {
    prompt(this.id, 'Net.SetOnConnect(' + _Cbm(callback));
  }
  public Disconnect() {
    prompt(this.id, 'Net.Disconnect(');
  }
  public Close() {
    prompt(this.id, 'Net.Disconnect(');
  }
  public IsEnabled() {
    return prompt(this.id, 'Net.IsEnabled(') === 'true';
  }
  public IsConnected() {
    return prompt(this.id, 'Net.IsConnected(') === 'true';
  }
  public SetTimeout(secs: string) {
    prompt(this.id, 'Net.SetTimeout(' + secs);
  }
  public SendText(text: string, mode: string) {
    prompt(this.id, 'Net.SendText(' + text + '\f' + mode);
  }
  public SendBytes(data: string, mode: string) {
    prompt(this.id, 'Net.SendBytes(\f' + data + '\f' + mode);
  }
  public ReceiveText(mode: string) {
    return prompt(this.id, 'Net.ReceiveText(' + mode);
  }
  // @ts-ignore TS2345
  public ReceiveBytes(mode: string) {
    return JSON.parse(prompt(this.id, 'Net.ReceiveBytes(\f' + mode));
  }
  public ReceiveFile(file: string, wait: string) {
    return prompt(this.id, 'Net.ReceiveFile(' + file + '\f' + wait);
  }
  public DownloadFile(file: string) {
    return prompt(this.id, 'Net.DownloadFile(' + file);
  }
  public SetOnDownload(callback: any) {
    prompt(this.id, 'Net.SetOnDownload(' + _Cbm(callback));
  }
  public GetBroadcastAddress() {
    return prompt(this.id, 'Net.GetBroadcastAddress(');
  }
  public SendDatagram(
    data: string,
    mode: string,
    address: string,
    port: string,
    options: string
  ) {
    prompt(
      this.id,
      'Net.SendDatagram(\f' +
        data +
        '\f' +
        mode +
        '\f' +
        address +
        '\f' +
        port +
        '\f' +
        options
    );
  }
  public ReceiveDatagram(mode: string, port: string, timeout: string) {
    return prompt(
      this.id,
      'Net.ReceiveDatagram(' + mode + '\f' + port + '\f' + timeout
    );
  }
  public ReceiveDatagrams(port: string, mode: string) {
    prompt(this.id, 'Net.ReceiveDatagrams(\f' + port + '\f' + mode);
  }
  public SetOnReceive(callback: any) {
    prompt(this.id, 'Net.SetOnReceive(' + _Cbm(callback));
  }
  public AutoReceive(server: string, port: string, mode: string) {
    return prompt(
      this.id,
      'Net.AutoReceive(' + server + '\f' + port + '\f' + mode
    );
  }
  public WakeOnLan(ip: string, mac: string) {
    prompt(this.id, 'Net.WakeOnLan(\f' + ip + '\f' + mac);
  }
}
