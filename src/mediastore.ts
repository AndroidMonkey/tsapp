import { SObj } from './sobj';

export class MediaStore extends SObj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'MediaStore';
  }
  public QueryMedia(filter: string, sort: string, options: string) {
    prompt(
      this.id,
      'Med.QueryMedia(\f' + filter + '\f' + sort + '\f' + options
    );
  }
  public SetOnMediaResult(callback: any) {
    prompt(this.id, 'Med.SetOnMediaResult(\f' + _Cbm(callback));
  }
  public QueryArtists(filter: string, sort: string, options: string) {
    prompt(
      this.id,
      'Med.QueryArtists(\f' + filter + '\f' + sort + '\f' + options
    );
  }
  public SetOnArtistsResult(callback: any) {
    prompt(this.id, 'Med.SetOnArtistsResult(\f' + _Cbm(callback));
  }
  public QueryAlbums(filter: string, sort: string, options: string) {
    prompt(
      this.id,
      'Med.QueryAlbums(\f' + filter + '\f' + sort + '\f' + options
    );
  }
  public SetOnAlbumsResult(callback: any) {
    prompt(this.id, 'Med.SetOnAlbumsResult(\f' + _Cbm(callback));
  }
  public GetAlbumArt(img: { id: any }, id: string, options: string) {
    return (
      prompt(
        this.id,
        'Med.GetAlbumArt(\f' +
          (img ? img.id : null) +
          '\f' +
          id +
          '\f' +
          options
      ) === 'true'
    );
  }
  public GetSongArt(img: { id: any }, id: string, options: string) {
    return (
      prompt(
        this.id,
        'Med.GetSongArt(\f' + (img ? img.id : null) + '\f' + id + '\f' + options
      ) === 'true'
    );
  }
}
