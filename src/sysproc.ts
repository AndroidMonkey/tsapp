import { SObj } from './sobj';

export class SysProc extends SObj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'SysProc';
  }
  public Destroy() {
    prompt(this.id, 'Sys.Destroy(');
    _map[this.id] = null;
  }
  public Out(cmd: string) {
    prompt(this.id, 'Sys.Out(\f' + cmd);
  }
  public In(maxLines: string, options: string) {
    return prompt(this.id, 'Sys.In(\f' + maxLines + '\f' + options);
  }
  public Err(maxLines: string, options: string) {
    return prompt(this.id, 'Sys.Err(\f' + maxLines + '\f' + options);
  }
  public ReadFileAsByte(file: string) {
    // @ts-ignore TS2345
    return parseInt(prompt(this.id, 'Sys.ReadFileAsByte(\f' + file), 10);
  }
  public WriteToFile(file: string, data: string) {
    prompt(this.id, 'Sys.WriteToFile(\f' + file + '\f' + data);
  }
  public SetOnInput(callback: any) {
    prompt(this.id, 'Sys.SetOnInput(\f' + _Cbm(callback));
  }
  public SetOnError(callback: any) {
    prompt(this.id, 'Sys.SetOnError(\f' + _Cbm(callback));
  }
}
