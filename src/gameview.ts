import { Obj } from './obj';

export class GameView extends Obj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'GameView';
  }
  public Destroy() {
    prompt(this.id, 'WGL.Destroy(');
    _map[this.id] = null;
  }
  public SetFile(file: string) {
    prompt(this.id, 'WGL.SetFile(\f' + file);
  }
  public SetFrameRate(fps: string) {
    prompt(this.id, 'WGL.SetFrameRate(\f' + fps);
  }
  public SetOnTouch(callback: any) {
    prompt(this.id, 'WGL.SetOnTouch(\f' + _Cbm(callback));
  }
  public SetOnTouchUp(callback: any) {
    prompt(this.id, 'WGL.SetOnTouchUp(\f' + _Cbm(callback));
  }
  public SetOnTouchMove(callback: any) {
    prompt(this.id, 'WGL.SetOnTouchMove(\f' + _Cbm(callback));
  }
  public SetOnTouchDown(callback: any) {
    prompt(this.id, 'WGL.SetOnTouchDown(\f' + _Cbm(callback));
  }
  public SetTouchable(touchable: string) {
    prompt(this.id, 'WGL.SetTouchable(\f' + touchable);
  }
}
