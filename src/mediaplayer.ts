import { SObj } from './sobj';

export class MediaPlayer extends SObj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'MediaPlayer';
  }
  public SetFile(file: string) {
    prompt(this.id, 'Aud.SetFile(' + file);
  }
  public SetLooping(loop: string) {
    prompt(this.id, 'Aud.SetLooping(\f' + loop);
  }
  public Close() {
    prompt(this.id, 'Aud.Close(');
  }
  public Release() {
    prompt(this.id, 'Aud.Release(');
    _map[this.id] = null;
  }
  public Destroy() {
    prompt(this.id, 'Aud.Release(');
    _map[this.id] = null;
  }
  public Play(from: string) {
    prompt(this.id, 'Aud.Play(\f' + from);
  }
  public Pause() {
    prompt(this.id, 'Aud.Pause(');
  }
  public Stop() {
    prompt(this.id, 'Aud.Stop(');
  }
  public IsReady() {
    return prompt(this.id, 'Aud.IsReady(') === 'true';
  }
  public IsPlaying() {
    return prompt(this.id, 'Aud.IsPlaying(') === 'true';
  }
  public IsLooping() {
    return prompt(this.id, 'Aud.IsLooping(') === 'true';
  }
  public SeekTo(time: string) {
    prompt(this.id, 'Aud.SeekTo(' + time);
  }
  public GetPosition() {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Aud.GetPosition('));
  }
  public GetDuration() {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Aud.GetDuration('));
  }
  public SetVolume(left: string, right: string) {
    prompt(this.id, 'Aud.SetVolume(' + left + '\f' + right);
  }
  public SetOnReady(callback: any) {
    prompt(this.id, 'Aud.SetOnReady(' + _Cbm(callback));
  }
  public SetOnComplete(callback: any) {
    prompt(this.id, 'Aud.SetOnComplete(' + _Cbm(callback));
  }
  public SetOnSeekDone(callback: any) {
    prompt(this.id, 'Aud.SetOnSeekDone(' + _Cbm(callback));
  }
}
