import { Obj } from './obj';

export class List extends Obj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'List';
  }
  public SetList(list: string, delim: string) {
    prompt(this.id, 'Lst.SetList(\f' + list + '\f' + delim);
  }
  public GetList(delim: string) {
    // @ts-ignore TS2345
    return JSON.parse(prompt(this.id, 'Lst.GetList(' + delim));
  }
  public AddItem(title: string, body: string, image: string) {
    prompt(this.id, 'Lst.AddItem(\f' + title + '\f' + body + '\f' + image);
  }
  public InsertItem(index: string, title: string, body: string, image: string) {
    prompt(
      this.id,
      'Lst.InsertItem(\f' + index + '\f' + title + '\f' + body + '\f' + image
    );
  }
  public SetItem(
    title: string,
    newTitle: string,
    newBody: string,
    newImage: string
  ) {
    prompt(
      this.id,
      'Lst.SetItem(\f' +
        title +
        '\f' +
        newTitle +
        '\f' +
        newBody +
        '\f' +
        newImage
    );
  }
  public SetItemByIndex(
    index: string,
    newTitle: string,
    newBody: string,
    newImage: string
  ) {
    prompt(
      this.id,
      'Lst.SetItemByIndex(\f' +
        index +
        '\f' +
        newTitle +
        '\f' +
        newBody +
        '\f' +
        newImage
    );
  }
  public RemoveItem(title: string) {
    prompt(this.id, 'Lst.RemoveItem(\f' + title);
  }
  public RemoveItemByIndex(index: string) {
    prompt(this.id, 'Lst.RemoveItemByIndex(\f' + index);
  }
  public RemoveAll() {
    prompt(this.id, 'Lst.RemoveAll(');
  }
  public SelectItem(title: string, body: string, scroll: string) {
    const p = 'Lst.SelectItem(\f' + title + '\f' + body + '\f' + scroll;
    prompt(this.id, p);
  }
  public SelectItemByIndex(index: string, scroll: string) {
    const p = 'Lst.SelectItemByIndex(\f' + index + '\f' + scroll;
    prompt(this.id, p);
  }
  public GetItem(title: string) {
    const p = 'Lst.GetItem(\f' + title;
    // @ts-ignore TS2345
    return JSON.parse(prompt(this.id, p));
  }
  public GetItemByIndex(index: string) {
    const p = 'Lst.GetItemByIndex(\f' + index;
    // @ts-ignore TS2345
    return JSON.parse(prompt(this.id, p));
  }
  public GetLength() {
    // @ts-ignore TS2345
    return parseInt(prompt(this.id, 'Lst.GetLength('), 10);
  }
  public ScrollToItem(title: string, body: string) {
    const p = 'Lst.ScrollToItem(\f' + title + '\f' + body;
    prompt(this.id, p);
  }
  public ScrollToItemByIndex(index: string) {
    const p = 'Lst.ScrollToItemByIndex(\f' + index;
    prompt(this.id, p);
  }
  public SetOnTouch(callback: any) {
    prompt(this.id, 'Lst.SetOnClick(' + _Cbm(callback));
  }
  public SetOnLongTouch(callback: any) {
    prompt(this.id, 'Lst.SetOnLongClick(' + _Cbm(callback));
  }
  public SetTextColor(clr: string) {
    prompt(this.id, 'Lst.SetTextColor1(' + clr);
  }
  public SetTextColor1(clr: string) {
    prompt(this.id, 'Lst.SetTextColor1(' + clr);
  }
  public SetTextColor2(clr: string) {
    prompt(this.id, 'Lst.SetTextColor2(' + clr);
  }
  public SetHiTextColor1(clr: string) {
    prompt(this.id, 'Lst.SetHiTextColor1(' + clr);
  }
  public SetHiTextColor2(clr: string) {
    prompt(this.id, 'Lst.SetHiTextColor2(' + clr);
  }
  public SetTextSize(size: string, mode: string) {
    prompt(this.id, 'Lst.SetTextSize1(\f' + size + '\f' + mode);
  }
  public SetTextSize1(size: string, mode: string) {
    prompt(this.id, 'Lst.SetTextSize1(\f' + size + '\f' + mode);
  }
  public SetTextSize2(size: string, mode: string) {
    prompt(this.id, 'Lst.SetTextSize2(\f' + size + '\f' + mode);
  }
  public GetTextSize(mode: string) {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Lst.GetTextSize(\f' + mode));
  }
  public SetColumnWidths(
    icon: string,
    title: string,
    body: string,
    mode: string
  ) {
    prompt(
      this.id,
      'Lst.SetColumnWidths(\f' + icon + '\f' + title + '\f' + body + '\f' + mode
    );
  }
  public SetTextMargins(
    left: string,
    top: string,
    right: string,
    bottom: string,
    mode: string,
    options: string
  ) {
    prompt(
      this.id,
      'Lst.SetTextMargins(\f' +
        left +
        '\f' +
        top +
        '\f' +
        right +
        '\f' +
        bottom +
        '\f' +
        mode +
        '\f' +
        options
    );
  }
  public SetIconMargins(
    left: string,
    top: string,
    right: string,
    bottom: string,
    mode: string
  ) {
    prompt(
      this.id,
      'Lst.SetIconMargins(\f' +
        left +
        '\f' +
        top +
        '\f' +
        right +
        '\f' +
        bottom +
        '\f' +
        mode
    );
  }
  public SetIconSize(size: string, mode: string) {
    prompt(this.id, 'Lst.SetIconSize(\f' + size + '\f' + mode);
  }
  public SetEllipsize(mode: string) {
    prompt(this.id, 'Lst.SetEllipsize1(\f' + mode);
  }
  public SetEllipsize1(mode: string) {
    prompt(this.id, 'Lst.SetEllipsize1(\f' + mode);
  }
  public SetEllipsize2(mode: string) {
    prompt(this.id, 'Lst.SetEllipsize2(\f' + mode);
  }
  public SetTextShadow(radius: string, dx: string, dy: string, color: string) {
    prompt(
      this.id,
      'Lst.SetTextShadow1(\f' + radius + '\f' + dx + '\f' + dy + '\f' + color
    );
  }
  public SetTextShadow1(radius: string, dx: string, dy: string, color: string) {
    prompt(
      this.id,
      'Lst.SetTextShadow1(\f' + radius + '\f' + dx + '\f' + dy + '\f' + color
    );
  }
  public SetTextShadow2(radius: string, dx: string, dy: string, color: string) {
    prompt(
      this.id,
      'Lst.SetTextShadow2(\f' + radius + '\f' + dx + '\f' + dy + '\f' + color
    );
  }
  public SetDivider(height: string, color: string) {
    prompt(this.id, 'Lst.SetDivider(\f' + height + '\f' + color);
  }
  public SetFontFile(file: string) {
    prompt(this.id, 'Lst.SetFontFile(\f' + file);
  }
  public AdjustColor(hue: string, sat: string, bright: string, cont: string) {
    prompt(
      this.id,
      'Lst.AdjustColor(\f' + hue + '\f' + sat + '\f' + bright + '\f' + cont
    );
  }
}
