import { SObj } from './sobj';

export class BluetoothSerial extends SObj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'BluetoothSerial';
  }
  public Connect(name: string, channel: string) {
    return prompt(this.id, 'Bts.Connect(' + name + '\f' + channel) === 'true';
  }
  public Listen(enabled: string) {
    prompt(this.id, 'Bts.Listen(\f' + enabled);
  }
  public SetOnConnect(callback: any) {
    prompt(this.id, 'Bts.SetOnConnect(' + _Cbm(callback));
  }
  public SetOnDisconnect(callback: any) {
    prompt(this.id, 'Bts.SetOnDisconnect(\f' + _Cbm(callback));
  }
  public Disconnect() {
    prompt(this.id, 'Bts.Disconnect(');
  }
  public IsBluetoothEnabled() {
    return prompt(this.id, 'Bts.IsEnabled(') === 'true';
  }
  public RequestEnable() {
    prompt(this.id, 'Bts.RequestEnable(');
  }
  public IsPaired(name: string) {
    return prompt(this.id, 'Bts.IsPaired(' + name) === 'true';
  }
  public IsConnected() {
    return prompt(this.id, 'Bts.IsConnected(') === 'true';
  }
  public Write(data: string) {
    prompt(this.id, 'Bts.Write(' + data);
  }
  public SetOnReceive(callback: any) {
    prompt(this.id, 'Bts.SetOnReceive(' + _Cbm(callback));
  }
  public SetSplitMode(mode: string, p2: string, p3: string) {
    prompt(this.id, 'Bts.SetSplitMode(' + mode + '\f' + p2 + '\f' + p3);
  }
  public SetTimeout(ms: string) {
    prompt(this.id, 'Bts.SetTimeout(' + ms);
  }
  public Clear() {
    prompt(this.id, 'Bts.Clear(');
  }
}
