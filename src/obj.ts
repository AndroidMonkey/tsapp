import { IControl } from './icontrol';

export class Obj implements IControl {
  public data: any;
  protected _left: number;
  protected _top: number;
  constructor(public id: string) {
    _map[id] = this;
    // this.id = id;
    this.data = {};
    this._left = 0;
    this._top = 0;
  }
  public Destroy() {
    prompt(this.id, 'Obj.Release(');
    _map[this.id] = null;
  }
  public Release() {
    prompt(this.id, 'Obj.Release(');
    _map[this.id] = null;
  }
  public Method(
    name: string,
    types: string,
    p1: string,
    p2: string,
    p3: string,
    p4: string
  ) {
    return prompt(
      this.id,
      `Obj.Method(\f${name}\f${types}\f${p1}\f${p2}\f${p3}\f${p4}`
    );
  }
  public SetVisibility(mode: string) {
    prompt(this.id, 'Obj.SetVisibility(' + mode);
  }
  public GetVisibility() {
    return prompt(this.id, 'Obj.GetVisibility(');
  }
  public Hide() {
    prompt(this.id, 'Obj.SetVisibility(Hide');
  }
  public Gone() {
    prompt(this.id, 'Obj.SetVisibility(Gone');
  }
  public Show() {
    prompt(this.id, 'Obj.SetVisibility(Show');
  }
  public IsVisible() {
    return prompt(this.id, 'Obj.GetVisibility(') === 'Show';
  }
  public IsEnabled() {
    return prompt(this.id, 'Obj.IsEnabled(') === 'true';
  }
  public SetEnabled(enable: string) {
    prompt(this.id, 'Obj.SetEnabled(\f' + enable);
  }
  public SetPadding(
    left: number,
    top?: number,
    right?: number,
    bottom?: number,
    mode?: string
  ) {
    prompt(
      this.id,
      `Obj.SetPadding(\f${left}\f${top}\f${right}\f${bottom}\f${mode}`
    );
  }
  public SetMargins(
    left: string,
    top: string,
    right: string,
    bottom: string,
    mode: string
  ) {
    prompt(
      this.id,
      `Obj.SetMargins(\f${left}\f${top}\f${right}\f${bottom}\f${mode}`
    );
  }
  public SetBackground(file: string, options: string) {
    prompt(this.id, 'Obj.SetBackground(' + file + '\f' + options);
  }
  public SetBackAlpha(alpha: string) {
    prompt(this.id, 'Obj.SetBackAlpha(\f' + alpha);
  }
  public SetBackColor(clr: string) {
    prompt(this.id, 'Obj.SetBackColor(\f' + clr);
  }
  public SetBackGradient(
    colour1: string,
    colour2: string,
    colour3: string,
    options: string
  ) {
    prompt(
      this.id,
      `Obj.SetBackGradient(Linear\f${colour1}\f${colour2}\f${colour3}\f${options}\f${null}\f${null}\f${null}`
    );
  }
  public SetBackGradientRadial(
    x: string,
    y: string,
    radius: string,
    colour1: string,
    colour2: string,
    colour3: string,
    options: string
  ) {
    prompt(
      this.id,
      `Obj.SetBackGradient(Radial\f${x}\f${y}\f${radius}\f${colour1}\f${colour2}\f${colour3}\f${options}`
    );
  }
  public SetColorFilter(clr: string, mode: string) {
    prompt(this.id, 'Obj.SetColorFilter(\f' + clr + '\f' + mode);
  }
  public AdjustColor(hue: string, sat: string, bright: string, cont: string) {
    prompt(
      this.id,
      'Obj.AdjustColor(\f' + hue + '\f' + sat + '\f' + bright + '\f' + cont
    );
  }
  public SetPosition(
    left: number,
    top: number,
    width: number,
    height: number,
    options: string
  ) {
    prompt(
      this.id,
      `Obj.SetPosition(\f${left}\f${top}\f${width}\f${height}\f${options}`
    );
    this._left = left;
    this._top = top;
  }
  public SetSize(width: string, height: string, options: string) {
    prompt(this.id, 'Obj.SetSize(\f' + width + '\f' + height + '\f' + options);
  }
  public GetWidth(options: string) {
    return prompt(this.id, 'Obj.GetWidth(\f' + options);
  }
  public GetHeight(options: string) {
    return prompt(this.id, 'Obj.GetHeight(\f' + options);
  }
  public GetAbsWidth() {
    return prompt(this.id, 'Obj.GetAbsWidth(');
  }
  public GetAbsHeight() {
    return prompt(this.id, 'Obj.GetAbsHeight(');
  }
  public GetLeft(options: string) {
    return prompt(this.id, 'Obj.GetLeft(\f' + options);
  }
  public GetTop(options: string) {
    return prompt(this.id, 'Obj.GetTop(\f' + options);
  }
  public GetPosition(options: string) {
    // @ts-ignore TS2345
    return JSON.parse(prompt(this.id, 'Obj.GetPosition(\f' + options));
  }
  public SetScale(x: string, y: string) {
    prompt(this.id, 'Obj.SetScale(\f' + x + '\f' + y);
  }
  public Focus() {
    prompt(this.id, 'Obj.Focus(\f');
  }
  public ClearFocus() {
    prompt(this.id, 'Obj.ClearFocus(\f');
  }
  public Tween(
    target: any,
    duration: any,
    type: any,
    repeat: any,
    yoyo: any,
    callback: any
  ) {
    _Tween.apply(this, [target, duration, type, repeat, yoyo, callback]);
  }
}
