import { IControl } from './icontrol';
import { Obj } from './obj';

export class Layout extends Obj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'Layout';
  }
  public SetOrientation(orient: string) {
    prompt(this.id, 'Lay.SetOrientation(\f' + orient);
  }
  public SetGravity(gravity: string) {
    prompt(this.id, 'Lay.SetGravity(\f' + gravity);
  }
  public AddChild(child:IControl, order?: string) {
    prompt(
      this.id,
      'Lay.AddChild(\f' + (child ? child.id : null) + '\f' + order );
    if (child) {
      child._parent = this;
    }
  }
  public RemoveChild(child: { id: any; _parent: null }) {
    prompt(this.id, 'Lay.RemoveChild(' + (child ? child.id : null));
    if (child) {
      child._parent = null;
    }
  }
  public DestroyChild(child: { id: any; _parent: null }) {
    prompt(this.id, 'Lay.DestroyChild(' + (child ? child.id : null));
    if (child) {
      child._parent = null;
    }
  }
  public ChildToFront(child: { id: any }) {
    prompt(this.id, 'Lay.ChildToFront(' + (child ? child.id : null));
  }
  public GetChildOrder(child: { id: any }) {
    return parseInt(
      // @ts-ignore TS2345
      prompt(this.id, 'Lay.GetChildOrder(\f' + (child ? child.id : null)),
      10
    );
  }
  public Animate(type: string, callback: any, time: string) {
    prompt(
      this.id,
      'Lay.Animate(\f' + type + '\f' + _Cbm(callback) + '\f' + time
    );
  }
  public SetTouchable(touchable: string) {
    prompt(this.id, 'Lay.SetTouchable(\f' + touchable);
  }
  public SetTouchThrough(through: string) {
    prompt(this.id, 'Lay.SetTouchThrough(\f' + through);
  }
  public SetOnTouch(callback: any) {
    prompt(this.id, 'Lay.SetOnTouch(\f' + _Cbm(callback));
  }
  public SetOnTouchUp(callback: any) {
    prompt(this.id, 'Lay.SetOnTouchUp(\f' + _Cbm(callback));
  }
  public SetOnTouchMove(callback: any) {
    prompt(this.id, 'Lay.SetOnTouchMove(\f' + _Cbm(callback));
  }
  public SetOnTouchDown(callback: any) {
    prompt(this.id, 'Lay.SetOnTouchDown(\f' + _Cbm(callback));
  }
  public SetOnLongTouch(callback: any) {
    prompt(this.id, 'Lay.SetOnLongTouch(\f' + _Cbm(callback));
  }
  public SetOnChildChange(callback: any) {
    prompt(this.id, 'Lay.SetOnChildChange(\f' + _Cbm(callback));
  }
}
