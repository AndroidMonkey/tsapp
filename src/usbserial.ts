import { SObj } from './sobj';

export class USBSerial extends SObj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'USBSerial';
  }
  public Start() {
    prompt(this.id, 'Usb.Start(');
  }
  public Stop() {
    prompt(this.id, 'Usb.Stop(');
  }
  public Write(txt: string, encoding: string) {
    prompt(this.id, 'Usb.Write(\f' + txt + '\f' + encoding);
  }
  public SetOnReceive(callback: any) {
    prompt(this.id, 'Usb.SetOnReceive(' + _Cbm(callback));
  }
  public SetDTR(onOff: string) {
    prompt(this.id, 'Usb.SetDTR(\f' + onOff);
  }
  public SetRTS(onOff: string) {
    prompt(this.id, 'Usb.SetRTS(\f' + onOff);
  }
  public SetMaxRead(bytes: string) {
    prompt(this.id, 'Usb.SetMaxRead(\f' + bytes);
  }
  public SetMaxWrite(bytes: string) {
    prompt(this.id, 'Usb.SetMaxWrite(\f' + bytes);
  }
  public SetTimeout(ms: string) {
    prompt(this.id, 'Usb.SetTimeout(\f' + ms);
  }
  public IsConnected() {
    return prompt(this.id, 'Usb.IsConnected(') === 'true';
  }
  public SetDataMode(mode: string) {
    prompt(this.id, 'Usb.SetDataMode(\f' + mode);
  }
  public SetSplitMode(mode: string, p2: string, p3: string) {
    prompt(this.id, 'Usb.SetSplitMode(\f' + mode + '\f' + p2 + '\f' + p3);
  }
}
