
_dbg = true; _map = []; _scripts = [];
_languages = null; _curLang = "en"; _started=false;   
_cbMap = []; _cbId=0; _docs = false; _busy=false; _isV8=false;
_btl = null; _lvw = null; _ldg = null; 
_ynd = null; _nxt = null; _smw = null;
_inf = null; _rec = null; _pst = null;
_sms = null; _eml = null; _wpr = null;
_crp = null; _spr = null;


function App() 
{				
	/*#app*/ this.GetType = function() { return "App"; }
	/*#app*/ this.GetObjects = function() { return _map; }
	/*#app*/ this.IsStarted = function(){ return _started; }
	/*#app*/ this.Exit = function( kill ) { prompt( "#", "App.Exit("+kill ); }
	/*#app*/ this.ToBack = function() { prompt( "#", "App.ToBack(" ); }
	/*#app*/ this.ToFront = function() { prompt( "#", "App.ToFront(" ); }
	/*#app*/ this.Execute = function( js ) { prompt( "#", "App.Execute("+js ); } 
	/*#app*/ this.StartApp = function( file,options,intent ) { prompt( "#", "App.StartApp(\f"+file+"\f"+options+"\f"+intent ); }
	/*#app*/ this.StopApp = function( name ) { prompt( "#", "App.StopApp("+name ); }
	/*#app*/ this.SetScreenMode = function( mode ) { prompt( "#", "App.SetScreenMode(\f"+mode ); }
	/*#app*/ this.SetOptions = function( options ) { prompt( "#", "App.SetOptions(\f"+options ); }
	/*#app*/ this.SetTheme = function( theme ) { prompt( "#", "App.SetTheme(\f"+(theme?theme.id:null) ); }
	/*#app*/ this.SetDensity = function( dpi ) { prompt( "#", "App.SetDensity(\f"+dpi ); }
	/*#app*/ this.SetPosition = function( left,top,width,height,options ) { prompt( "#", "App.SetPosition(\f"+left+"\f"+top+"\f"+width+"\f"+height+"\f"+options ); }
	/*#app*/ this.SetBackColor = function( clr ) { prompt( "#", "App.SetBackColor(\f"+clr ); }  
	/*#app*/ this.StartService = function( packageName,className ) { prompt( "#", "App.StartService(\f"+packageName+"\f"+className ); }
	/*#app*/ this.StopService = function() { prompt( "#", "App.StopService(" ); }
	/*#app*/ this.StartDebugServer = function() { prompt( "#", "App.StartDebugServer(" ); }
	/*#app*/ this.StopDebugServer = function() { prompt( "#", "App.StopDebugServer(" ); }
	/*#app*/ this.SendIntent = function( packageName,className,action,category,uri,type,extras,options,callback ) { prompt( "#", "App.SendIntent(\f"+packageName+"\f"+className+"\f"+action+"\f"+category+"\f"+uri+"\f"+type+"\f"+extras+"\f"+options+"\f"+_Cbm(callback) ); }
	/*#app*/ this.BroadcastIntent = function( action,category,data,type,extras,options ) { prompt( "#", "App.BroadcastIntent(\f"+action+"\f"+category+"\f"+data+"\f"+type+"\f"+extras+"\f"+options ); }
	/*#app*/ this.SendMessage = function( msg ) { prompt( "#", "App.SendMessage(\f"+msg ); }
	/*#app*/ this.SetInForeground = function( title,text,largeIcon,smallIcon,importance ) { prompt( "#", "App.SetInForeground(\f"+title+"\f"+text+"\f"+largeIcon+"\f"+smallIcon+"\f"+importance ); }
	/*#app*/ this.SetInBackground = function() { prompt( "#", "App.SetInBackground(\f" ); }
	/*#app*/ this.Script = function( file ) { _LoadScriptSync( file ); }
	/*#app*/ this.LoadScript = function( url, callback ) { _LoadScript( url, callback ); }
	/*#app*/ this.LoadPlugin = function( url ) { _LoadPlugin( url ); }
	/*#app*/ this.SysExec = function( cmd,options,maxRead,timeout ) { return prompt( "#", "App.SysExec(\f"+cmd+"\f"+options+"\f"+maxRead+"\f"+timeout ); } 
	/*#app*/ this.ExtExec = function( name,file,args,options ) { return prompt( "#", "App.ExtExec(\f"+name+"\f"+file+"\f"+args+"\f"+options ); } 
	/*#app*/ this.SetPriority = function( level ) { prompt( "#", "App.SetPriority(\f"+level ); } 
	/*#app*/ this.Odroid = function( p1,p2,p3 ) { return prompt( "#", "App.Odroid(\f"+p1+"\f"+p2+"\f"+p3 ); }
	/*#app*/ this.GetName = function() { return prompt( "#", "App.GetName(" ); }
	/*#app*/ this.GetPath = function() { return prompt( "#", "App.GetPath(" ); }
	/*#app*/ this.GetAppName = function() { return prompt( "#", "App.GetAppName(" ); }
	/*#app*/ this.GetAppPath = function() { return prompt( "#", "App.GetAppPath(" ); }
	/*#app*/ this.GetVersion = function() { return parseFloat(prompt( "#", "App.GetVersion(" )); }
	/*#app*/ this.GetDSVersion = function() { return parseFloat(prompt( "#", "App.GetDSVersion(" )); }	
	/*#app*/ this.IsNewVersion = function() { return prompt( "#", "App.IsNewVersion(" )=="true"; }
	/*#app*/ this.InIDE = function() { return prompt( "#", "App.InIDE(" )=="true"; }	
	/*#app*/ this.IsAPK = function() { return prompt( "#", "App.IsAPK(" )=="true"; }	
	/*#app*/ this.IsService = function() { return prompt( "#", "App.IsService(" )=="true"; }	
	/*#app*/ this.IsPremium = function() { return prompt( "#", "App.IsPremium(" )=="true"; }	
	/*#app*/ this.IsEngine = function() { return prompt( "#", "App.IsEngine(" )=="true"; }	
	/*#app*/ this.GetPackageName = function() { return prompt( "#", "App.GetPackageName(" ); }
	/*#app*/ this.CheckLicense = function( key ) { prompt( "#", "App.CheckLicense(\f"+key ); }
	/*#app*/ this.GetAccounts = function() { return prompt( "#", "App.GetAccounts(" ); }
	/*#app*/ this.GetUser = function() { return prompt( "#", "App.GetUser(" ); }
	/*#app*/ this.GetDeviceId = function() { return prompt( "#", "App.GetDeviceId(" ); }
	/*#app*/ this.GetCountryCode = function() { return prompt( "#", "App.GetCountryCode(" ); }
	/*#app*/ this.GetLanguageCode = function() { return prompt( "#", "App.GetLanguageCode(" ); }
	/*#app*/ this.GetCountry = function() { return prompt( "#", "App.GetCountry(" ); }
	/*#app*/ this.GetLanguage = function() { return prompt( "#", "App.GetLanguage(" ); }
	/*#app*/ this.GetOptions = function() { return prompt( "#", "App.GetOptions(" ); }	
	/*#app*/ this.GetSharedText = function( index ) { return prompt( "#", "App.GetSharedText("+index ); }
	/*#app*/ this.GetSharedFiles = function() { var s = prompt( "#", "App.GetSharedFiles(" ); if(s.length) return s.split(","); else return null; }
	/*#app*/ this.GetActivities = function( options ) { return eval(prompt( "#", "App.GetActivities(\f"+options )); }
	/*#app*/ this.IsAppInstalled = function( packageName ) { return prompt( "#", "App.IsAppInstalled(\f"+packageName )=="true"; }	
	/*#app*/ this.GetInstalledApps = function() { return eval(prompt( "#", "App.GetInstalledApps(\f" )); }
	/*#app*/ this.GetRunningApps = function() { return eval(prompt( "#", "App.GetRunningApps(\f" )); }
	/*#app*/ this.GetRunningServices = function() { return eval(prompt( "#", "App.GetRunningServices(\f" )); }
	/*#app*/ this.GetMemoryInfo = function() { return eval(prompt( "#", "App.GetMemoryInfo(\f" )); }
	/*#app*/ this.GetIntent = function() { var s = prompt( "#", "App.GetIntent(" ); if(s.length) return JSON.parse(s); else return null; }
	/*#app*/ this.GetNotifyId = function() { return prompt( "#", "App.GetNotifyId(" ); }
	/*#app*/ this.SetSharedApp = function( name ) { prompt( "#", "App.SetSharedApp("+name ); }
	/*#app*/ this.GetMediaFile = function( appName,ext ) { return prompt( "#", "App.GetMediaFile(\f"+appName+"\f"+ext ); }
	/*#app*/ this.KillApp = function( procId ) { prompt( "#", "App.KillApp("+procId ); }
	/*#app*/ this.CreateShortcut = function( name,iconFile,file,options ) { prompt( "#", "App.CreateShortcut(\f"+name+"\f"+iconFile+"\f"+file+"\f"+options ); }
	/*#app*/ this.GetBuildNum = function() { return parseInt( prompt( "#", "App.GetBuildNum(" )); }	
	/*#app*/ this.GetOSVersion = function() { return parseInt( prompt( "#", "App.GetBuildNum(" )); }	
	/*#app*/ this.GetModel = function() { return prompt( "#", "App.GetModel(" ); }	
	/*#app*/ this.IsTablet = function() { return prompt( "#", "App.IsTablet(" )=="true"; }	
	/*#app*/ this.IsChrome = function() { return prompt( "#", "App.IsChrome(" )=="true"; }	
	/*#app*/ this.IsThings = function() { return prompt( "#", "App.IsThings(" )=="true"; }	
	/*#app*/ this.SetOnError = function( callback ) { prompt( "#", "App.SetOnError(\f"+_Cbm(callback) ); }
	/*#app*/ this.SetOnDebug = function( callback ) { prompt( "#", "App.SetOnDebug(\f"+_Cbm(callback) ); }
	/*#app*/ this.SetOnKey = function( callback ) { prompt( "#", "App.SetOnKey(\f"+_Cbm(callback) ); }
	/*#app*/ this.SetOnShowKeyboard = function( callback ) { prompt( "#", "App.SetOnShowKeyboard(\f"+_Cbm(callback) ); }
	/*#app*/ this.SetOnWifiChange = function( callback ) { prompt( "#", "App.SetOnWifiChange(\f"+_Cbm(callback) ); }
	/*#app*/ this.WifiScan = function( callback ) { prompt( "#", "App.WifiScan(\f"+_Cbm(callback) ); }
	/*#app*/ this.DisableKeys = function( keyList ) { prompt( "#", "App.DisableKeys(\f"+keyList ); }
	/*#app*/ this.DisableTouch = function( disable ) { prompt( "#", "App.DisableTouch(\f"+disable ); }
	/*#app*/ this.GetIPAddress = function() { return prompt( "#", "App.GetIPAddress(" ); }
	/*#app*/ this.GetMacAddress = function() { return prompt( "#", "App.GetMacAddress(" ); }
	/*#app*/ this.GetSSID = function() { return prompt( "#", "App.GetSSID(" ); }
	/*#app*/ this.GetRSSI = function() { return parseInt(prompt( "#", "App.GetRSSI(" )); }
	/*#app*/ this.Broadcast = function( type,msg ) { prompt( "#", "App.Broadcast("+type+"\f"+msg ); }
	/*#app*/ this.SetOnBroadcast = function( callback ) { prompt( "#", "App.SetOnBroadcast("+_Cbm(callback) ); }
	/*#app*/ this.SetData = function( name,value ) { prompt( "#", "App.SetData(\f"+name+"\f"+value ); }
	/*#app*/ this.GetData = function( name ) { return prompt( "#", "App.GetData(\f"+name ); }	
	/*#app*/ this.SetClipboardText = function( txt ) { prompt( "#", "App.SetClipboardText("+txt ); }
	/*#app*/ this.GetClipboardText = function() { return prompt( "#", "App.GetClipboardText(" ); }
	/*#app*/ this.EnableBackKey = function( enable ) { prompt( "#", "App.EnableBackKey("+enable ); }		
	/*#app*/ this.Wait = function( secs ) { prompt( "#", "App.Wait("+secs ); }
	/*#app*/ this.Alert = function( msg,title,options,hue ) { prompt( "#", "App.Alert(\f"+msg+"\f"+title+"\f"+options+"\f"+hue ); }
	/*#app*/ this.HideKeyboard = function( hide ) { prompt( "#", "App.HideKeyboard("+hide ); }
	/*#app*/ this.ShowKeyboard = function( obj ) { return prompt( "#", "App.ShowKeyboard(\f"+obj.id )=="true"; }
	/*#app*/ this.IsKeyboardShown = function() { return prompt( "#", "App.IsKeyboardShown(" )=="true"; }
	/*#app*/ this.GetKeyboardHeight = function() { return prompt( "#", "App.GetKeyboardHeight(" ); }
	/*#app*/ this.TextToSpeech = function( text,pitch,rate,callback,stream,locale,engine ) { prompt( "#", "App.TextToSpeech(\f"+text+"\f"+pitch+"\f"+rate+"\f"+_Cbm(callback)+"\f"+stream+"\f"+locale+"\f"+engine ); }
	/*#app*/ this.Debug = function( msg ) { prompt( "#", "App.Debug(\f"+msg ); }
	/*#app*/ this.Error = function( msg,line,file,quit ) { prompt( "#", "App.Error(\f"+msg+"\f"+line+"\f"+file+"\f"+quit ); }
	/*#app*/ this.SetDebugEnabled = function( enable ) { prompt( "#", "App.SetDebugEnabled("+enable ); _dbg=enable; }
	/*#app*/ this.IsDebugEnabled = function() { return _dbg; }
	/*#app*/ this.CreateDebug = function() { prompt( "#", "App.CreateDebug(" ); }
	/*#app*/ this.ShowDebug = function( show ) { prompt( "#", "App.ShowDebug("+show ); }
	/*#app*/ this.SendMail = function( address,subject,body,attach,type,options ) { prompt( "#", "App.SendMail(\f"+address+"\f"+subject+"\f"+body+"\f"+attach+"\f"+type+"\f"+options ); }		
	/*#app*/ this.SendFile = function( file,subject,text,choose ) { prompt( "#", "App.SendFile(\f"+file+"\f"+subject+"\f"+text+"\f"+choose ); }		
	/*#app*/ this.SendText = function( text,subject,choose ) { prompt( "#", "App.SendText(\f"+text+"\f"+subject+"\f"+choose ); }		
	// this._Extract = function( p1 ) { prompt( "#", "App._Extract("+p1 ); }
	/*#app*/ this.ExtractAssets = function( src,dest,overwrite ) { prompt( "#", "App.ExtractAssets(\f"+src+"\f"+dest+"\f"+overwrite ); }
	/*#app*/ this.ExtractPlugins = function() { prompt( "#", "App.ExtractPlugins(\f" ); }
	/*#app*/ this.GetResourceId = function( name,options ) { return parseInt(prompt( "#", "App.GetResourceId(\f"+name+"\f"+options )); }	
	/*#app*/ this.Vibrate = function( pattern ) { prompt( "#", "App.Vibrate("+pattern ); }
	/*#app*/ this.ShowPopup = function( msg,options ) { prompt( "#", "App.ShowPopup("+msg+"\f"+options ); }
	/*#app*/ this.ShowProgress = function( msg,options ) { prompt( "#", "App.ShowProgress(\f"+msg+"\f"+options ); }	
	/*#app*/ this.HideProgress = function() { prompt( "#", "App.HideProgress(" ); }	
	/*#app*/ this.ShowProgressBar = function( title,percent,options ) { prompt( "#", "App.ShowProgressBar(\f"+title+"\f"+percent+"\f"+options ); }	
	/*#app*/ this.UpdateProgressBar = function( percent ) { prompt( "#", "App.UpdateProgressBar(\f"+percent ); }	
	/*#app*/ this.HideProgressBar = function() { prompt( "#", "App.HideProgressBar(" ); }	
	/*#app*/ this.LoadText = function( name,dflt,file ) { return prompt( "#", "App.LoadText("+name+"\f"+dflt+"\f"+file ); }
	/*#app*/ this.LoadNumber = function( name,dflt,file ) { return parseFloat(prompt( "#", "App.LoadNumber("+name+"\f"+dflt+"\f"+file )); }	
	/*#app*/ this.LoadBoolean = function( name,dflt,file ) { return (prompt( "#", "App.LoadBoolean("+name+"\f"+dflt+"\f"+file )=="true"); }
	/*#app*/ this.SaveText = function( name,value,file ) { prompt( "#", "App.SaveText("+name+"\f"+value+"\f"+file ); }
	/*#app*/ this.SaveNumber = function( name,value,file ) { prompt( "#", "App.SaveNumber("+name+"\f"+value+"\f"+file ); }	
	/*#app*/ this.SaveBoolean = function( name,value,file ) { prompt( "#", "App.SaveBoolean("+name+"\f"+value+"\f"+file ); }	
	/*#app*/ this.ClearData = function( file ) { prompt( "#", "App.ClearData(\f"+file ); }
	/*#app*/ this.ClearValue = function( name,file ) { prompt( "#", "App.ClearValue(\f"+name+"\f"+file ); }
	/*#app*/ this.GetTop = function() { return parseFloat(prompt( "#", "App.GetTop(" )); }
	/*#app*/ this.HasSoftNav = function() { return prompt( "#", "App.HasSoftNav(" )=="true"; }
	/*#app*/ this.GetScreenWidth = function( options ) { return parseFloat(prompt( "#", "App.GetScreenWidth(\f"+options )); }
	/*#app*/ this.GetScreenHeight = function( options ) { return parseFloat(prompt( "#", "App.GetScreenHeight(\f"+options )); }
	/*#app*/ this.GetScreenDensity = function() { return parseFloat(prompt( "#", "App.GetScreenDensity(" )); }
	/*#app*/ this.GetDisplayWidth = function() { return parseFloat(prompt( "#", "App.GetDisplayWidth(" )); }
	/*#app*/ this.GetDisplayHeight = function() { return parseFloat(prompt( "#", "App.GetDisplayHeight(" )); }
	/*#app*/ this.GetDefaultOrientation = function() { return prompt( "#", "App.GetDefaultOrientation(" ); }	
	/*#app*/ this.GetOrientation = function() { return prompt( "#", "App.GetOrientation(" ); }	
	/*#app*/ this.SetOrientation = function( orient,callback ) { prompt( "#", "App.SetOrientation(\f"+orient+"\f"+_Cbm(callback) ); }	
	/*#app*/ this.GetRotation = function() { return parseInt(prompt( "#", "App.GetRotation(" )); }	
	/*#app*/ this.GetBatteryLevel = function() { return parseFloat(prompt( "#", "App.GetBatteryLevel(\f" )); }
	/*#app*/ this.IsCharging = function() { return prompt( "#", "App.IsCharging(" )=="true"; }
	/*#app*/ this.GetChargeType = function() { return prompt( "#", "App.GetChargeType(\f" ); }
	/*#app*/ this.PreventScreenLock = function( mode ) { prompt( "#", "App.PreventScreenLock("+mode ); }	
	/*#app*/ this.PreventWifiSleep = function() { prompt( "#", "App.PreventWifiSleep(" ); }
	/*#app*/ this.SetWifiEnabled = function( enable ) { prompt( "#", "App.SetWifiEnabled(\f"+enable ); }
	/*#app*/ this.IsWifiEnabled = function() { return prompt( "#", "App.IsWifiEnabled(" )=="true"; }
	/*#app*/ this.SetWifiApEnabled = function( enable,ssid,key ) { prompt( "#", "App.SetWifiApEnabled(\f"+enable+"\f"+ssid+"\f"+key ); }
	/*#app*/ this.IsWifiApEnabled = function() { return prompt( "#", "App.IsWifiApEnabled(" )=="true"; }
	/*#app*/ this.WifiConnect = function( ssid,key ) { prompt( "#", "App.WifiConnect(\f"+ssid+"\f"+key ); }
	/*#app*/ this.IsConnected = function() { return prompt( "#", "App.IsConnected(" )=="true"; }
	/*#app*/ this.SetBluetoothEnabled = function( enable ) { prompt( "#", "App.SetBluetoothEnabled(\f"+enable ); }
	/*#app*/ this.IsBluetoothEnabled = function() { return prompt( "#", "App.IsBluetoothEnabled(" )=="true"; }
	/*#app*/ this.GetPairedBtDevices = function() { return eval(prompt( "#", "App.GetPairedBTDevices(\f" )); }
	/*#app*/ this.IsBtDevicePaired = function( name ) { return prompt( "#", "App.IsBtDevicePaired(\f"+name )=="true"; } 
	/*#app*/ this.DiscoverBtDevices = function( filter,onFound,onComplete ) { prompt( "#", "App.DiscoverBtDevices(\f"+filter+"\f"+_Cbm(onFound)+"\f"+_Cbm(onComplete) ); }
	/*#app*/ this.PairBtDevice = function( address,callback ) { prompt( "#", "App.PairBtDevice(\f"+address+"\f"+_Cbm(callback) ); }
	/*#app*/ this.UnpairBtDevice = function( address,callback ) { prompt( "#", "App.UnpairBtDevice(\f"+address+"\f"+_Cbm(callback) ); }
	/*#app*/ this.GetBtProfileState = function( type ) { return prompt( "#", "App.GetBtProfileState(\f"+type ); }
	/*#app*/ this.GetBluetoothName = function() { return prompt( "#", "App.GetBluetoothName(" ); }	
	/*#app*/ this.GetBluetoothAddress = function() { return prompt( "#", "App.GetBluetoothAddress(" ); }	
	/*#app*/ this.IsLocationEnabled = function( types ) { return prompt( "#", "App.IsLocationEnabled(\f"+types )=="true"; }
	/*#app*/ this.PlayRingtone = function( type ) { prompt( "#", "App.PlayRingtone(\f"+type ); }
	/*#app*/ this.SetRingerMode = function( mode ) { prompt( "#", "App.SetRingerMode(\f"+mode ); }
	/*#app*/ this.GetRingerMode = function() { return prompt( "#", "App.GetRingerMode(" ); }
	/*#app*/ this.SetSpeakerPhone = function( on ) { prompt( "#", "App.SetSpeakerPhone(\f"+on ); }
	/*#app*/ this.GetSpeakerPhone = function() { return prompt( "#", "App.GetSpeakerPhone(" )=="true"; }
	/*#app*/ this.SetVolume = function( stream,level,options ) { prompt( "#", "App.SetVolume(\f"+stream+"\f"+level+"\f"+options ); }
	/*#app*/ this.GetVolume = function( stream ) { return parseFloat(prompt( "#", "App.GetVolume(\f"+stream )); }
	/*#app*/ this.SetTitle = function( title ) { prompt( "#", "App.SetTitle("+title ); }	
	/*#app*/ this.SetMenu = function( list,iconPath ) { prompt( "#", "App.SetMenu("+list+"\f"+iconPath ); }
	/*#app*/ this.ShowMenu = function() { prompt( "#", "App.ShowMenu(" ); }		
	/*#app*/ this.AddLayout = function( layout ) { prompt( "#", "App.AddLayout("+layout.id ); }	
	/*#app*/ this.RemoveLayout = function( layout ) { prompt( "#", "App.RemoveLayout("+ layout.id ); }
	/*#app*/ this.DestroyLayout = function( layout ) { prompt( "#", "App.DestroyLayout("+ layout.id ); }	
	/*#app*/ this.AddDrawer = function( layout,side,width,grabWidth ) { prompt( "#", "App.AddDrawer(\f"+layout.id+"\f"+side+"\f"+width+"\f"+grabWidth ); }	
	/*#app*/ this.RemoveDrawer = function( side ) { prompt( "#", "App.RemoveDrawer(\f"+ side ); }
	/*#app*/ this.OpenDrawer = function( side ) { prompt( "#", "App.OpenDrawer(\f"+side ); }	
	/*#app*/ this.CloseDrawer = function( side ) { prompt( "#", "App.CloseDrawer(\f"+side ); }	
	/*#app*/ this.LockDrawer = function( side ) { prompt( "#", "App.LockDrawer(\f"+side ); }	
	/*#app*/ this.UnlockDrawer = function( side ) { prompt( "#", "App.UnlockDrawer(\f"+side ); }	
	/*#app*/ this.GetDrawerState = function( side ) { return prompt( "#", "App.GetDrawerState(\f"+side ); }
	/*#app*/ this.MakeFolder = function( fldr ) { prompt( "#", "App.MakeFolder("+fldr ); }	
	/*#app*/ this.GetPrivateFolder = function( name ) { return prompt( "#", "App.GetPrivateFolder(\f"+name ); }	
	/*#app*/ this.GetDatabaseFolder = function() { return prompt( "#", "App.GetDatabaseFolder(" ); }
	/*#app*/ this.DeleteDatabase = function( name ) { prompt( "#", "App.DeleteDatabase(\f"+name); }
	/*#app*/ this.FolderExists = function( fldr ) { return prompt( "#", "App.FolderExists("+fldr )=="true"; }
	/*#app*/ this.FileExists = function( file ) { return prompt( "#", "App.FileExists("+file )=="true"; }
	/*#app*/ this.IsFolder = function( fldr ) { return prompt( "#", "App.IsFolder("+fldr )=="true"; }
	/*#app*/ this.ListFolder = function( path,filter,limit,options ) { return eval(prompt( "#", "App.ListFolder(\f"+path+"\f"+filter+"\f"+limit+"\f"+options )); }
	/*#app*/ this.GetExternalFolder = function() { return prompt( "#", "App.GetExternalFolder(" ); }
	/*#app*/ this.GetInternalFolder = function() { return prompt( "#", "App.GetInternalFolder(" ); }
	/*#app*/ this.GetSpecialFolder = function( name ) { return prompt( "#", "App.GetSpecialFolder(\f"+name ); }
	/*#app*/ this.GetEnv = function( name ) { return prompt( "#", "App.GetEnv(\f"+name ); }
	/*#app*/ this.GetPermission = function( type,callback ) { prompt( "#", "App.GetPermission(\f"+type+"\f"+_Cbm(callback) ); }
	/*#app*/ this.CheckPermission = function( type ) { return prompt( "#", "App.CheckPermission(\f"+type ); }
	/*#app*/ this.ReadFile = function( file,encoding ) { return prompt( "#", "App.ReadFile(\f"+file+"\f"+encoding ); }
	/*#app*/ this.WriteFile = function( file,text,mode,encoding ) { prompt( "#", "App.WriteFile(\f"+file+"\f"+text+"\f"+mode+"\f"+encoding ); }	
	/*#app*/ this.OpenFile = function( file,type,choose ) { prompt( "#", "App.OpenFile(\f"+file+"\f"+type+"\f"+choose ); }	
	/*#app*/ this.OpenUrl = function( url,type,choose ) { prompt( "#", "App.OpenUrl(\f"+url+"\f"+type+"\f"+choose ); }
	/*#app*/ this.DownloadFile = function( src,dest,title,desc,options ) { prompt( "#", "App.DownloadFile(\f"+src+"\f"+dest+"\f"+title+"\f"+desc+"\f"+options ); }
	/*#app*/ this.ChooseFile = function( msg,type,callback ) { prompt( "#", "App.ChooseFile(\f"+msg+"\f"+type+"\f"+_Cbm(callback) ); }
	/*#app*/ this.ChooseContact = function( type,callback ) { prompt( "#", "App.ChooseContact(\f"+type+"\f"+_Cbm(callback) ); }
	/*#app*/ this.ChooseImage = function( options,callback ) { prompt( "#", "App.ChooseImage(\f"+options+"\f"+_Cbm(callback) ); }
	/*#app*/ this.ChooseAccount = function( callback ) { prompt( "#", "App.ChooseAccount(\f"+_Cbm(callback) ); }
	/*#app*/ this.DeleteFile = function( file ) { prompt( "#", "App.DeleteFile("+file); }
	/*#app*/ this.CopyFile = function( src,dest ) { prompt( "#", "App.CopyFile("+src+"\f"+dest); }
	/*#app*/ this.CopyFolder = function( src,dest,overwrite,filter ) { prompt( "#", "App.CopyFolder(\f"+src+"\f"+dest+"\f"+overwrite+"\f"+filter); }
	/*#app*/ this.DeleteFolder = function( fldr ) { prompt( "#", "App.DeleteFolder("+fldr); }
	/*#app*/ this.RenameFile = function( src,dest ) { prompt( "#", "App.RenameFile(\f"+src+"\f"+dest); }
	/*#app*/ this.RenameFolder = function( src,dest ) { prompt( "#", "App.RenameFile(\f"+src+"\f"+dest); }
	/*#app*/ this.ReplaceInFile = function( file,txt,rep,options ) { prompt( "#", "App.ReplaceInFile(\f"+file+"\f"+txt+"\f"+rep+"\f"+options); }
	/*#app*/ this.UnzipFile = function( src,dest ) { prompt( "#", "App.UnzipFile(\f"+src+"\f"+dest); }
	/*#app*/ this.ZipFile = function( src,dest ) { prompt( "#", "App.ZipFile(\f"+src+"\f"+dest); }
	/*#app*/ this.ZipFolder = function( src,dest ) { prompt( "#", "App.ZipFile(\f"+src+"\f"+dest); }
	/*#app*/ this.GetFreeSpace = function( mode ) { return parseFloat(prompt( "#", "App.GetFreeSpace(\f"+mode)); }
	/*#app*/ this.GetFileDate = function( file ) { var d = parseInt(prompt( "#", "App.GetFileDate(\f"+file)); if( d ) return new Date(d); else return null; }
	/*#app*/ this.GetFileSize = function( file ) { return parseInt(prompt( "#", "App.GetFileSize(\f"+file)); }
	/*#app*/ this.GetThumbnail = function( src,dest,width,height ) { prompt( "#", "App.GetThumbnail(\f"+src+"\f"+dest+"\f"+width+"\f"+height); }
	/*#app*/ this.ScanFile = function( file ) { prompt( "#", "App.ScanFile(\f"+file); }
	/*#app*/ this.GetLastButton = function() { var ret = prompt( "#", "App.GetLastButton(" ); if( ret ) return (_map[ret]); else return null; }
	/*#app*/ this.GetLastToggle = function() { var ret = prompt( "#", "App.GetLastToggle(" ); if( ret ) return (_map[ret]); else return null; }
	/*#app*/ this.GetLastCheckBox = function() { var ret = prompt( "#", "App.GetLastCheckBox(" ); if( ret ) return (_map[ret]); else return null; }
	/*#app*/ this.GetLastImage = function() { var ret = prompt( "#", "App.GetLastImage(" ); if( ret ) return (_map[ret]); else return null; }
	/*#app*/ this.IsBluetoothOn = function() { return prompt( "#", "App.IsBluetoothOn(" )=="true"; }
	/*#app*/ this.IsScreenOn = function() { return prompt( "#", "App.IsScreenOn(" )=="true"; }
	/*#app*/ this.WakeUp = function() { prompt( "#", "App.WakeUp(" ); }
	/*#app*/ this.GoToSleep = function() { prompt( "#", "App.GoToSleep(" ); }
	/*#app*/ this.Unlock = function() { prompt( "#", "App.Unlock(" ); }	
	/*#app*/ this.Lock = function() { prompt( "#", "App.Lock(" ); }	
	/*#app*/ this.SetScreenBrightness = function( level ) { prompt( "#", "App.SetScreenBrightness(\f"+level); }
	/*#app*/ this.SetKioskMode = function( mode,enable,options ) { prompt( "#", "App.SetKioskMode(\f"+mode+"\f"+enable+"\f"+options); }
	/*#app*/ this.GetMetadata = function( file,keys ) { return prompt( "#", "App.GetMetadata(\f"+file+"\f"+keys); }
	/*#app*/ this.SetAlarm = function( type,id,callback,time,interval,options ) { return prompt( "#", "App.SetAlarm(\f"+type+"\f"+id+"\f"+_Cbm(callback)+"\f"+time+"\f"+interval+"\f"+options); }
	/*#app*/ this.Call = function( number ) { prompt( "#", "App.Call(\f"+number ); }
	/*#app*/ this.SimulateTouch = function( obj,x,y,dir ) { prompt( "#", "App.SimulateTouch(\f"+obj.id+"\f"+x+"\f"+y+"\f"+dir ); }
	/*#app*/ this.SimulateDrag = function( obj,x1,y1,x2,y2,step,pause ) { prompt( "#", "App.SimulateDrag(\f"+obj.id+"\f"+x1+"\f"+y1+"\f"+x2+"\f"+y2+"\f"+step+"\f"+pause ); }
	/*#app*/ this.SimulateScroll = function( obj,x,y,dx,dy,count,fling ) { prompt( "#", "App.SimulateScroll(\f"+obj.id+"\f"+x+"\f"+y+"\f"+dx+"\f"+dy+"\f"+count+"\f"+fling ); }
	/*#app*/ this.SimulateKey = function( obj,keyName,modifiers,pause ) { prompt( "#", "App.SimulateKey(\f"+obj.id+"\f"+keyName+"\f"+modifiers+"\f"+pause ); }
	/*#app*/ this.GetJoystickState = function( id,key ) { return parseFloat(prompt( "#", "App.GetJoyState(\f"+id+"\f"+key)); }
	/*#app*/ this.GetJoystickName = function( id ) { return prompt( "#", "App.GetJoyName(\f"+id); }
	/*#app*/ this.SetJoystickOptions = function( options ) { prompt( "#", "App.SetJoystickOptions(\f"+options ); }
	/*#app*/ this.SetAutoBoot = function( auto ) { prompt( "#", "App.SetAutoBoot(\f"+auto); }
	/*#app*/ this.SetAutoWifi = function( auto ) { prompt( "#", "App.SetAutoWifi(\f"+auto); }
	/*#app*/ this.SetAutoStart = function( appName ) { prompt( "#", "App.SetAutoStart(\f"+appName); }
	/*#app*/ this.HttpRequest = function( type,baseUrl,path,params,callback,headers ) { prompt( "#", "App.HttpRequest(\f"+type+"\f"+baseUrl+"\f"+path+"\f"+params+"\f"+_Cbm(callback)+"\f"+headers); }
	/*#app*/ this.UploadFile = function( url,file,name,callback ) { prompt( "#", "App.UploadFile(\f"+url+"\f"+file+"\f"+name+"\f"+_Cbm(callback) ); }
	/*#app*/ this.SaveCookies = function() { prompt( "#", "App.SaveCookies(" ); }	
	/*#app*/ this.ClearCookies = function( session ) { prompt( "#", "App.ClearCookies(\f"+session ); }
	/*#app*/ this.SetUserAgent = function( agent ) { prompt( "#", "App.SetUserAgent(\f"+agent ); } 
    /*#app*/ this.SetUserCreds = function( name,password ) { prompt( "#", "App.SetUserCreds(\f"+name+"\f"+password ); }
    /*#app*/ this.QueryContent = function( uri,columns,select,args,sort ) { return eval(prompt( "#", "App.QueryContent(\f"+uri+"\f"+columns+"\f"+select+"\f"+args+"\f"+sort)); }
	/*#app*/ this.Uri2Path = function( uri ) { return prompt( "#", "App.Uri2Path(\f"+uri); }
	/*#app*/ this.ScreenShot = function( fileName,quality ) { prompt( "#", "App.ScreenShot(\f"+fileName+"\f"+quality ); }
	/*#app*/ this.InstallWallpaper = function( packageName,className ) { prompt( "#", "App.InstallWallpaper\f"+packageName+"\f"+className ); }
	
	//These objects auto-release when layout is destroyed.		
	/*#app*/ this.CreateLayout = function( type,options ) { var ret = prompt( "#", "App.CreateLayout("+type+"\f"+options ); if( ret ) return new Lay(ret); else return null; }	
	/*#app*/ this.CreateImage = function( file,width,height,options,w,h ) { var ret = prompt( "#", "App.CreateImage("+file+"\f"+width+"\f"+height+"\f"+options+"\f"+w+"\f"+h );  if( ret ) return new Img(ret); else return null; }	
	/*#app*/ this.CreateButton = function( text,width,height,options ) { var ret = prompt( "#", "App.CreateButton("+text+"\f"+width+"\f"+height+"\f"+options ); if( ret ) return new Btn(ret); else return null;  }		
	/*#app*/ this.CreateToggle = function( text,width,height,options ) { var ret = prompt( "#", "App.CreateToggle("+text+"\f"+width+"\f"+height+"\f"+options ); if( ret ) return new Tgl(ret); else return null;  }		
	/*#app*/ this.CreateCheckBox = function( text,width,height,options ) { var ret = prompt( "#", "App.CreateCheckBox("+text+"\f"+width+"\f"+height+"\f"+options ); if( ret ) return new Chk(ret); else return null;  }		
	/*#app*/ this.CreateSpinner = function( list,width,height,options ) { var ret = prompt( "#", "App.CreateSpinner("+list+"\f"+width+"\f"+height+"\f"+options ); if( ret ) return new Spn(ret); else return null; }		
	/*#app*/ this.CreateSeekBar = function( width,height,options ) { var ret = prompt( "#", "App.CreateSeekBar("+width+"\f"+height+"\f"+options ); if( ret ) return new Skb(ret); else return null; }		
	/*#app*/ this.CreateText = function( text,width,height,options ) { var ret = prompt( "#", "App.CreateText("+text+"\f"+width+"\f"+height+"\f"+options ); if( ret ) return new Txt(ret); else return null; }		
	/*#app*/ this.CreateTextEdit = function( text,width,height,options ) { var ret = prompt( "#", "App.CreateTextEdit("+text+"\f"+width+"\f"+height+"\f"+options ); if( ret ) return new Txe(ret); else return null; }		
	/*#app*/ this.CreateList = function( list,width,height,options,delim ) { var ret = prompt( "#", "App.CreateList(\f"+list+"\f"+width+"\f"+height+"\f"+options+"\f"+delim ); if( ret ) return new Lst(ret); else return null; }	
	/*#app*/ this.CreateWebView = function( width,height,options,zoom ) { var ret = prompt( "#", "App.CreateWeb(\f"+width+"\f"+height+"\f"+options+"\f"+zoom ); if( ret ) return new Web(ret); else return null; }	
	/*#app*/ this.CreateScroller = function( width,height,options ) { var ret = prompt( "#", "App.CreateScroller("+width+"\f"+height+"\f"+options ); if( ret ) return new Scr(ret); else return null; }	
	/*#app*/ this.CreateCameraView = function( width,height,options ) { var ret = prompt( "#", "App.CreateCameraView("+width+"\f"+height+"\f"+options );  if( ret ) return new Cam(ret); else return null; }	
	/*#app*/ this.CreateVideoView = function( width,height,options ) { var ret = prompt( "#", "App.CreateVideoView(\f"+width+"\f"+height+"\f"+options );  if( ret ) return new Vid(ret); else return null; }	
	/*#app*/ this.CreateGameView = function( width,height,options ) { var ret = prompt( "#", "App.CreateGameView(\f"+width+"\f"+height+"\f"+options );  if( ret ) return new WGL(ret); else return null; }	
	/*#app*/ this.CreateCodeEdit = function( text,width,height,options ) { var ret = prompt( "#", "App.CreateCodeEdit(\f"+text+"\f"+width+"\f"+height+"\f"+options ); if( ret ) return new Cde(ret); else return null; }		
	/*#app*/ this.CreateTheme = function( baseTheme ) { var ret = prompt( "#", "App.CreateTheme(\f"+baseTheme ); if( ret ) return new Thm(ret); else return null;  }		
	
	//These objects auto-release (ie. single instance)
	/*#app*/ this.CreateYesNoDialog = function( msg,options ) { if( _ynd ) _ynd.Release(); var ret = prompt( "#", "App.CreateYesNoDialog(\f"+msg+"\f"+options ); if( ret ) _ynd = new Ynd(ret); else _ynd = null; return _ynd; }		
	/*#app*/ this.CreateListDialog = function( title,list,options ) { if( _ldg ) _ldg.Release(); var ret = prompt( "#", "App.CreateListDialog(\f"+title+"\f"+list+"\f"+options ); if( ret ) _ldg = new Ldg(ret); else _ldg = null; return _ldg; }	
	/*#app*/ this.CreateListView = function( list,title,options ) { if( _lvw ) _lvw.Release(); var ret = prompt( "#", "App.CreateListView(\f"+list+"\f"+title+"\f"+options ); if( ret ) _lvw = new Lvw(ret); else _lvw = null; return _lvw; }	
	/*#app*/ this.CreateBluetoothList = function( filter ) { if( _btl ) _btl.Release(); var ret = prompt( "#", "App.CreateBluetoothList("+filter ); if( ret) _btl = new Btl(ret); else _btl = null; return _btl; }	
	/*#app*/ this.CreateAudioRecorder = function() { if( _rec ) _rec.Release(); var ret = prompt( "#", "App.CreateAudioRecorder(" ); if( ret) _rec = new Rec(ret); else _rec = null; return _rec; }
	/*#app*/ this.CreateSMS = function() { if( _sms ) _sms.Release(); var ret = prompt( "#", "App.CreateSMS(" ); if( ret) _sms = new SMS(ret); else _sms = null; return _sms; }
	/*#app*/ this.CreateEmail = function( account,password ) { if( _eml ) _eml.Release(); var ret = prompt( "#", "App.CreateEmail("+account+"\f"+password ); if( ret) _eml = new EMAIL(ret); else _eml = null; return _eml; }
	/*#app*/ this.CreateSmartWatch = function( type ) { if( _smw ) _smw.Release(); var ret = prompt( "#", "App.CreateSmartWatch(\f"+type ); if( ret) _smw = new SMW(ret); else _smw = null; return _smw; }
	/*#app*/ this.CreateCrypt = function( options ) { if( _crp ) _crp.Release(); var ret = prompt( "#", "App.CreateCrypt(\f"+options ); if( ret) _crp = new Crp(ret); else _crp = null; return _crp; }
	/*#app*/ this.CreateSpeechRec = function( options ) { if( _spr ) _spr.Release(); var ret = prompt( "#", "App.CreateSpeechRec(\f"+options ); if( ret) _spr = new Spr(ret); else _spr = null; return _spr; }
	/*#app*/ this.CreatePhoneState = function( types ) { if( _pst ) _pst.Release(); var ret = prompt( "#", "App.CreatePhoneState(\f"+types ); if( ret) _pst = new Pst(ret); else _pst = null; return _pst; }
	/*#app*/ this.CreateWallpaper = function( options ) { if( _wpr ) _wpr.Release(); var ret = prompt( "#", "App.CreateWallpaper(\f"+options ); if( ret) _wpr = new Wpr(ret); else _wpr = null; return _wpr; }
	
	//These objects need releasing manually.
	/*#app*/ this.CreateDialog = function( title,options ) { var ret = prompt( "#", "App.CreateDialog(\f"+title+"\f"+options ); if( ret ) return new Dlg(ret); else return null; }		
	/*#app*/ this.CreateOverlay = function( options ) { var ret = prompt( "#", "App.CreateOverlay(\f"+options ); if( ret ) return new Ovl(ret); else return null; }		
	/*#app*/ this.CreateMediaPlayer = function() { var ret = prompt( "#", "App.CreateMediaPlayer(" ); if( ret ) return new Aud(ret); else return null; }
	/*#app*/ this.CreateSensor = function( type,options ) { var ret = prompt( "#", "App.CreateSensor("+type+"\f"+options ); if( ret ) return new Sns(ret); else return null; }		
	/*#app*/ this.CreateLocator = function( type,options ) { var ret = prompt( "#", "App.CreateLocator("+type+"\f"+options ); if( ret ) return new Loc(ret); else return null; }		
	/*#app*/ this.CreateNetClient = function( type ) { var ret = prompt( "#", "App.CreateNetClient("+type ); if( ret ) return new Net(ret); else return null; }
	/*#app*/ this.CreateNxtRemote = function() { var ret = prompt( "#", "App.CreateNxtRemote(" ); if( ret ) return new Nxt(ret,null); else return null; }	
	/*#app*/ this.CreateWebServer = function( port,options ) { var ret = prompt( "#", "App.CreateWebServer("+port+"\f"+options ); if( ret ) return new Wbs(ret); else return null; }	
	/*#app*/ this.CreateUSBSerial = function( baudRate,dataBits,stopBits,parity,device ) { var ret = prompt( "#", "App.CreateUSBSerial(\f"+baudRate+"\f"+dataBits+"\f"+stopBits+"\f"+parity+"\f"+device ); if( ret ) return new Usb(ret); else return null; }	
	/*#app*/ this.CreateSysProc = function( cmd,env,dir,options ) { var ret = prompt( "#", "App.CreateSysProc(\f"+cmd+"\f"+env+"\f"+dir+"\f"+options ); if( ret ) return new Sys(ret); else return null; }	
	/*#app*/ this.CreateService = function( packageName,className,callback,options ) { var ret = prompt( "#", "App.CreateService(\f"+packageName+"\f"+className+"\f"+options+"\f"+_Cbm(callback) ); if( ret ) return new Svc(ret); else return null; }	
	/*#app*/ this.CreateSynth = function( type ) { var ret = prompt( "#", "App.CreateSynth("+type ); if( ret ) return new Syn(ret); else return null; }	
	/*#app*/ this.CreateBluetoothSerial = function( mode ) { var ret = prompt( "#", "App.CreateBluetoothSerial(\f"+mode ); if( ret ) return new Bts(ret); else return null; }	
	/*#app*/ this.CreateZipUtil = function( mode ) { var ret = prompt( "#", "App.CreateZipUtil(\f"+mode ); if( ret ) return new Zip(ret); else return null; }	
	/*#app*/ this.CreateDownloader = function( options ) { var ret = prompt( "#", "App.CreateDownloader(\f"+options ); if( ret ) return new Dwn(ret); else return null; }	
	/*#app*/ this.CreateMediaStore = function() { var ret = prompt( "#", "App.CreateMediaStore(" ); if( ret ) return new Med(ret); else return null; }	
	/*#app*/ this.CreatePlayStore = function() { var ret = prompt( "#", "App.CreatePlayStore(" ); if( ret ) return new Ply(ret); else return null; }	
	/*#app*/ this.CreateNotification = function( options ) { var ret = prompt( "#", "App.CreateNotification(\f"+options ); if( ret ) return new Not(ret); else return null; }	
	/*#app*/ this.CreateFile = function( file,mode ) { var ret = prompt( "#", "App.CreateFile(\f"+file+"\f"+mode ); if( ret ) return new Fil(ret); else return null; }	
	
	//Special methods.
	this.Start = function() { if(typeof OnStart=='function') { OnStart(); prompt("#","_Start"); _started=true; } }
	
	/*#app*/ this.CreateObject = function( name, type ) 
	{ 
		if( !type ) try { return eval( "new "+name+"()" ); } catch(e) { return null; } 
		else { var ret = prompt( "#", "_Obj(\f"+type+"\f"+name ); if( ret ) return new SObj(ret); else return null; }
	}	
	
	/*#app*/ this.GA = function( cmd )
	{
		try {
			if( app.FileExists("/Sys/ga.js") ) 
			{
				if( cmd.toLowerCase()=='create' ) {
					_LoadScriptSync( "/Sys/ga.js" );
					window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
					var dbg = _dbg; _UseDbg( false );
					ga('create', arguments[1], {'storage':'none', 'clientId':app.GetDeviceId()});
					ga('set', { checkProtocolTask: null, checkStorageTask: null });
					_UseDbg( dbg );
				}
				else ga.apply( this, arguments );
			}
		}
		catch(e){}
	}
	
	var _anim_t = 0;
	function _animatev8() {if(_cbAnimate) {var t=new Date().getTime(); _cbAnimate(t,t-_anim_t); _anim_t=t;}}
	function _animate() {if(_cbAnimate) {var t=new Date().getTime(); _cbAnimate(t,t-_anim_t); _anim_t=t; requestAnimationFrame(_animate);}}
	/*#app*/ this.Animate = function( callback,fps )
	{
		_cbAnimate = callback;
		_anim_t = new Date().getTime();
		if( _isV8 ) {
			_fps=(fps?fps:30);
			if( _cbAnimate ) _tmAnimate = setInterval( _animatev8, 1000/_fps );
			else if( _tmAnimate ) clearInterval( _tmAnimate );
		}
		else {
			window._fps=(fps?fps:30); 
			requestAnimationFrame(_animate); 
		}
	}
	
	/*#app*/ this.Language2Code = function(name) { 
		if( name ) return _languages.codes[name.toLowerCase()]; 
		else return _curLang; 
	}

	/*#app*/ this.SetLanguage = function( name ) { 
		var json = app.ReadFile( "lang.json" )
		_languages = JSON.parse(json);
		_curLang = _languages.codes[name.toLowerCase()];
	}

	//Helper classes.
	/*#app*/ this.CreateNxt = function() { var nxtHelp = new _NxtHelp(); return nxtHelp.nxt_CreateNxt(); }
	/*#app*/ this.CreateTabs = function( list,width,height,options ) { return new _Tabs( list,width,height,options ); }
	/*#app*/ this.CreateWebSocket = function( id,ip,port,options ) { return new _WebSock( id,ip,port,options ); }
	/*#app*/ this.CreateWizard = function( title,width,height,callback ) { return new _Wizard( title,width,height,callback ) }
	
	//Externally defined methods.
	/*#app*/ this.ShowTextDialog = function( title,deflt,callback ) { _ShowTextDialog( title,deflt,callback ) }
	/*#app*/ this.ShowTip = function( msg,left,top,timeOut,options ) { _ShowTip( msg,left,top,timeOut,options ) }
	/*#app*/ this.PlaySound = function( file ) { _PlaySound( file ) }
	
	//Hybrid objects.
	/*#app*/ this.CreateGLView = function( width,height,options ) 
	{
	    var glv = null;
	    if( options.toLowerCase().indexOf("fast2d") > -1 )
	    {
			_LoadScriptSync( "/Sys/cp.js" );
    		_LoadScriptSync( "/Sys/gl.js" );
    		glv = new GLV( prompt( "#", "App.CreateGLView(\f"+width+"\f"+height+"\f"+options ));
    		glv.canvas = FastCanvas.create(); 
    		glv.ctx = glv.canvas.getContext("2d");
    		glv.width = Math.round(app.GetDisplayWidth()*width);
    		glv.height = Math.round(app.GetDisplayHeight()*height);
    		glv.aspect = glv.width / glv.height;
    		glv.GetType = function() { return "GLView"; }
	    }
		return glv;
	}
	
	/*#app*/ this.OpenDatabase = function( name ) 
	{
		_LoadScriptSync( "/Sys/cp.js" );
		_LoadScriptSync( "/Sys/sql.js" );
		_CreateCP( "sqliteplugin" );
		
		var db = sqlitePlugin.openDatabase( name );
		db.name = name;
	    
	    db.GetType = function() { return "Database"; }
	    db.GetName = function() { return db.name; }
		db.ExecuteSql = function( sql, params, success, error ) 
		{
			if( !success ) success = null;
			if( !error ) error = _Err;
	      
			db.transaction( function(tx) { 
				tx.executeSql( sql, params, 
					function(tx,res) { if(success) success.apply(db,[res]) }, 
					function(t,e) { error.apply(db,[e.message]); } 
				); }, error
			);
		}
		db.Close = function() { db.close( _Log, _Err ); }
		db.Delete = function() { sqlitePlugin.deleteDatabase(db.name,_Log,_Err); }
		return db;
	}
}/*end of App*/

function _Try( p1,p2,p3,p4 ) { return prompt( "#", "App.Try(\f"+p1+"\f"+p2+"\f"+p3+"\f"+p4 ); } 
function _Call( id, func, params ) { if( func ) func.apply( _map[id], params ); }
function _Cb( obj, func ) { return new _ObjCb(obj, func); }
function _ObjCb( obj, func ) { _cbMap[++_cbId] = obj; this.name = "_cbMap['"+_cbId+"']."+func; }
function _Cbm( func ) { return ( func ? (func.name ? func.name : new _ObjCbm(func).name) : null ); }
function _ObjCbm( func ) { var hs = _hash(func.toString()); _cbMap[hs] = func; this.name = "_cbMap['"+hs+"']"; }
function _hash( str ) { var hash=5381, i=str.length; while(i) { hash=(hash*33)^str.charCodeAt(--i); } return hash>>>0; }

function _UseDbg( b ) { prompt( "#", "_UseDbg(\f"+b ); _dbg=b; }
function _ExecV8( file ) { return prompt( "#", "_ExecV8(\f"+file ); }
function _Thread( file ) { return prompt( "#", "_Thread(\f"+file ); }
function _DoEvents( ms ) { return prompt( "#", "_DoEvents(\f"+ms ); }
function _CreateCP( service ) { return prompt( "#", "_CreateCP(\f"+service ); }
function _ExecCP( callbackId,service,action,argsJson ) { 
	return prompt( "#", "_ExecCP(\f"+callbackId+"\f"+service+"\f"+action+"\f"+argsJson ); 
}

function _LoadScript( url, callback )
{
    if( _scripts[url] ) { 
		if( callback ) callback(); return; 
	}
	if( url.slice(-4)==".dsj" ) url += ".js";
	var dbg = _dbg; _UseDbg( false );
	if( url.indexOf(":")<0 && !app.FileExists(url) ) 
	    alert("Error: "+url+" not found!" + 
	        (app.IsAPK?"\n\n(Note: Assets are case sensitive)":"") );
	_UseDbg( dbg );
	if( _isV8 ) _ExecV8(url);
	else {
		var head = document.getElementsByTagName('head')[0];
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = url;
		script.onload = callback;
		head.appendChild(script);
    }
    _scripts[url] = true;
}

function _LoadScriptSync( url )
{
    if( _scripts[url] ) return; 
    if( url.slice(-4)==".dsj" ) url += ".js";
    var dbg = _dbg; _UseDbg( false );
    if( url.indexOf(":")<0 && !app.FileExists(url) ) 
	    alert("Error: "+url+" not found!" + 
	        (app.IsAPK?"\n\n(Note: Assets are case sensitive)":"") );
	_UseDbg( dbg );
    if( _isV8 ) _ExecV8(url);
	else {
		var head = document.getElementsByTagName('head')[0];
		var script = document.createElement('script');
		script.type = 'text/javascript';
		var dbg = _dbg; _UseDbg( false );
		script.text = app.ReadFile( url ); 
		_UseDbg( dbg );
		head.appendChild(script);
    }
    _scripts[url] = true;
}

function _LoadPlugin( name )
{
    if( !name ) return;
    var privDir = app.GetPrivateFolder( "Plugins" );
    _LoadScriptSync( privDir+"/"+name.toLowerCase()+"/"+name+".inc" );
}

function _CreatePlugin( name, options ) 
{ 
	var ret = prompt( "#", "App.CreatePlugin("+name+"\f"+options ); 
	if( ret ) return new Plg(ret); 
	else throw "Failed to create plugin:" + name; 
}	

function _Run(s) { 
	_busy = true; eval( s ); _busy = false;
}

function _SafeRun(s) {
	try { _busy = true; _UseDbg(false); eval( s ); _UseDbg(true); _busy = false; }
	catch(e) {}
}

function T( id, lang ) {
	var tr = _languages ? _languages.trans[id] : null;
	if( tr ) tr = tr[lang?lang:_curLang]; else return "[No Translation]";
	return (tr ? tr : _languages.trans[id]["en"]);
}

function OnCreate(extract,debug) 
{       
	if( typeof _CheckFolderName=='function' ) _CheckFolderName();
	//if( extract ) app._Extract( true );
	if(typeof OnStart=='function') { OnStart(); prompt("#","_Start"); _started=true; }
	if( debug ) app.CreateDebug();
}

app = new App();

function _Log( msg ) { app.Debug( msg ); }
function _Err( msg ) { app.Debug( "ERROR: " + msg ); }
function _GetStack() { return new Error().stack }
function _AddPermissions() {}
function _RemovePermissions() {}
function _AddOptions() {}

if( typeof navigator=="undefined" ) { navigator = {userAgent:"Android"} };
if( navigator.userAgent.indexOf("Android")>-1 )
{
	//Provide toast popup on Remix.
	_dlgPop = null;
	if( app.GetModel().indexOf("Remix")>-1 )
	{
		app.ShowPopup = function( msg, options )
		{
			if( options ) options = options.toLowerCase();
			else options = "";
			if( _dlgPop==null ) 
			{ 
				_dlgPop = app.CreateDialog( null, "NoDim,NoTouch,NoFocus" ); 
				_dlgPop.SetBackColor( "#cc000000" );
				_dlgPop.SetPosition( -1, options.indexOf("bottom")>-1 ? 0.75 : 0.25  );
				var lay = app.CreateLayout( "linear", "vcenter" );
				lay.SetPadding( 0.02, 0.02, 0.02, 0.02 );
				_dlgPop.AddLayout( lay );
				_txtDlgPop = app.CreateText( msg );
				_txtDlgPop.SetTextSize( 22 );
				_txtDlgPop.SetTextColor( "#ffffff" );
				lay.AddChild( _txtDlgPop );
			}
			else _txtDlgPop.SetText( msg );
			_dlgPop.Show();
			if( _dlgPop.timer ) clearTimeout( _dlgPop.timer ); 
			var time = ( options.indexOf("short") ? 2000 : 4000 );
			_dlgPop.timer = setTimeout( function() { _dlgPop.Hide(); }, time );
		}
	}

	//Init app.
	prompt( "#", "_Init" );
}
