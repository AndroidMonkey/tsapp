import { Obj } from './obj';

export class GLView extends Obj {
  constructor(id: string) {
    super(id);
  }
  public GetType() {
    return 'GLView';
  }
  public Release() {
    prompt(this.id, 'GLV.Release(');
    _map[this.id] = null;
  }
  public Destroy() {
    prompt(this.id, 'GLV.Release(');
    _map[this.id] = null;
  }
  public Execute(p1: string, p2: string, p3: string, p4: string) {
    prompt(this.id, 'GLV.Execute(\f' + p1 + '\f' + p2 + '\f' + p3 + '\f' + p4);
  }
  public Exec(p1: any, p2: any, p3: any, p4: any) {
    _gfx.Exec(p1, p2, p3, p4);
  }
  public SetOnTouch(callback: any) {
    prompt(this.id, 'GLV.SetOnTouch(\f' + _Cbm(callback));
  }
  public SetOnTouchUp(callback: any) {
    prompt(this.id, 'GLV.SetOnTouchUp(\f' + _Cbm(callback));
  }
  public SetOnTouchMove(callback: any) {
    prompt(this.id, 'GLV.SetOnTouchMove(\f' + _Cbm(callback));
  }
  public SetOnTouchDown(callback: any) {
    prompt(this.id, 'GLV.SetOnTouchDown(\f' + _Cbm(callback));
  }
  public SetTouchable(touchable: string) {
    prompt(this.id, 'GLV.SetTouchable(\f' + touchable);
  }
}
