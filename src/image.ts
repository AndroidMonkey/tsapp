import { Obj } from './obj';

export class Image extends Obj {
  private _auto: boolean;
  private _gfb: string;
  constructor(id: string) {
    super(id);
    this._auto = true;
    this._gfb = '';
  }
  public GetType() {
    return 'Image';
  }
  public Clear() {
    if (this._auto) {
      prompt(this.id, 'Img.Clear(');
    } else {
      this.Draw('c');
    }
  }
  public Update() {
    if (this._auto) {
      prompt(this.id, 'Img.Update(');
    } else {
      prompt(this.id, 'Img.Batch(' + this._gfb);
      this._gfb = '';
    }
  }
  public SetAutoUpdate(onoff: boolean) {
    this._auto = onoff;
    prompt(this.id, 'Img.SetAutoUpdate(\f' + onoff);
  }
  public SetPixelMode(onoff: string) {
    prompt(this.id, 'Img.SetPixelMode(\f' + onoff);
  }
  public SetName(name: string) {
    prompt(this.id, 'Img.SetName(' + name);
  }
  public GetName() {
    return prompt(this.id, 'Img.GetName(');
  }
  public SetImage(
    image: string,
    width: string,
    height: string,
    options: string
  ) {
    if (typeof image === 'string') {
      prompt(
        this.id,
        'Img.LoadImage(\f' +
          image +
          '\f' +
          width +
          '\f' +
          height +
          '\f' +
          options
      );
    } else {
      prompt(
        this.id,
        'Img.CopyImage(\f' +
          (image ? image.id : null) +
          '\f' +
          width +
          '\f' +
          height +
          '\f' +
          options
      );
    }
  }
  public GetPixelData(
    format: string,
    left: string,
    top: string,
    width: string,
    height: string
  ) {
    return prompt(
      this.id,
      'Img.GetPixelData(\f' +
        format +
        '\f' +
        left +
        '\f' +
        top +
        '\f' +
        width +
        '\f' +
        height
    );
  }
  public SetPixelData(
    data: string,
    width: string,
    height: string,
    options: string
  ) {
    return prompt(
      this.id,
      'Img.SetPixelData(\f' +
        data +
        '\f' +
        width +
        '\f' +
        height +
        '\f' +
        options
    );
  }
  public GetPixelColor(x: string, y: string) {
    // @ts-ignore TS2345
    return JSON.parse(prompt(this.id, 'Img.GetPixelColor(\f' + x + '\f' + y));
  }
  public SetSize(width: string, height: string, options: string) {
    prompt(this.id, 'Img.SetSize(\f' + width + '\f' + height + '\f' + options);
  }
  public GetHeight() {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Img.GetHeight('));
  }
  public GetWidth() {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Img.GetWidth('));
  }
  public GetAbsHeight() {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Img.GetAbsHeight('));
  }
  public GetAbsWidth() {
    // @ts-ignore TS2345
    return parseFloat(prompt(this.id, 'Img.GetAbsWidth('));
  }
  public SetOnTouch(callback: any) {
    prompt(this.id, 'Img.SetOnTouch(' + _Cbm(callback));
  }
  public SetOnTouchUp(callback: any) {
    prompt(this.id, 'Img.SetOnTouchUp(' + _Cbm(callback));
  }
  public SetOnTouchMove(callback: any) {
    prompt(this.id, 'Img.SetOnTouchMove(' + _Cbm(callback));
  }
  public SetOnTouchDown(callback: any) {
    prompt(this.id, 'Img.SetOnTouchDown(' + _Cbm(callback));
  }
  public SetOnLongTouch(callback: any) {
    prompt(this.id, 'Img.SetOnLongTouch(' + _Cbm(callback));
  }
  public SetOnLoad(callback: any) {
    prompt(this.id, 'Img.SetOnLoad\f' + _Cbm(callback));
  }
  public SetTouchable(touchable: string) {
    prompt(this.id, 'Img.SetTouchable(' + touchable);
  }
  public SetMaxRate(ms: string) {
    prompt(this.id, 'Img.SetMaxRate(' + ms);
  }
  public SetColorFilter(clr: string, mode: string) {
    prompt(this.id, 'Img.SetColorFilter(\f' + clr + '\f' + mode);
  }
  public AdjustColor(hue: string, sat: string, bright: string, cont: string) {
    prompt(
      this.id,
      'Img.AdjustColor(\f' + hue + '\f' + sat + '\f' + bright + '\f' + cont
    );
  }
  public MeasureText(txt: string) {
    // @ts-ignore TS2345
    return JSON.parse(prompt(this.id, 'Img.MeasureText(\f' + txt));
  }
  public DrawImage(
    image: { id: any },
    x: string,
    y: string,
    w: string,
    h: string,
    angle: string,
    mode: string
  ) {
    if (this._auto) {
      prompt(
        this.id,
        'Img.DrawImage\f' +
          (image ? image.id : null) +
          '\f' +
          x +
          '\f' +
          y +
          '\f' +
          w +
          '\f' +
          h +
          '\f' +
          angle +
          '\f' +
          mode
      );
    } else {
      this.Draw(
        'i',
        image ? image.id : null,
        x,
        y,
        w ? w : -1,
        h ? h : -1,
        angle,
        mode
      );
    }
  }
  public DrawImageMtx(image: { id: any }, matrix: string) {
    if (this._auto) {
      prompt(
        this.id,
        'Img.DrawImageMtx\f' + (image ? image.id : null) + '\f' + matrix
      );
    } else {
      this.Draw('m', image ? image.id : null, matrix);
    }
  }
  public DrawPoint(x: string, y: string) {
    if (this._auto) {
      prompt(this.id, 'Img.DrawPoint(' + x + '\f' + y);
    } else {
      this.Draw('p', null, x, y);
    }
  }
  public DrawCircle(x: string, y: string, radius: string) {
    if (this._auto) {
      prompt(this.id, 'Img.DrawCircle(' + x + '\f' + y + '\f' + radius);
    } else {
      this.Draw('e', null, x, y, radius);
    }
  }
  public DrawArc(
    x1: string,
    y1: string,
    x2: string,
    y2: string,
    start: string,
    sweep: string
  ) {
    if (this._auto) {
      prompt(
        this.id,
        'Img.DrawArc(' +
          x1 +
          '\f' +
          y1 +
          '\f' +
          x2 +
          '\f' +
          y2 +
          '\f' +
          start +
          '\f' +
          sweep
      );
    } else {
      this.Draw('a', null, x1, y1, x2, y2, start, sweep);
    }
  }
  public DrawLine(x1: string, y1: string, x2: string, y2: string) {
    if (this._auto) {
      prompt(this.id, 'Img.DrawLine(' + x1 + '\f' + y1 + '\f' + x2 + '\f' + y2);
    } else {
      this.Draw('l', null, x1, y1, x2, y2);
    }
  }
  public DrawRectangle(x1: string, y1: string, x2: string, y2: string) {
    if (this._auto) {
      prompt(this.id, 'Img.DrawRect(' + x1 + '\f' + y1 + '\f' + x2 + '\f' + y2);
    } else {
      this.Draw('r', null, x1, y1, x2, y2);
    }
  }
  public DrawText(txt: string, x: string, y: string) {
    if (this._auto) {
      prompt(this.id, 'Img.DrawText(' + txt + '\f' + x + '\f' + y);
    } else {
      this.Draw('t', txt, x, y, 0, 0, 0);
    }
  }
  public DrawSamples(data: string, range: string) {
    if (this._auto) {
      prompt(this.id, 'Img.DrawSamples(\f' + data + '\f' + range);
    } else {
      this.Draw('g', data, range, 0, 0, 0, 0);
    }
  }
  public SetAlpha(alpha: string) {
    if (this._auto) {
      prompt(this.id, 'Img.SetAlpha(' + alpha);
    } else {
      this.Draw('k', null, alpha);
    }
  }
  public SetColor(clr: string) {
    if (this._auto) {
      prompt(this.id, 'Img.SetColor(' + clr);
    } else {
      this.Draw('o', clr);
    }
  }
  public SetTextSize(size: string) {
    if (this._auto) {
      prompt(this.id, 'Img.SetTextSize(' + size);
    } else {
      this.Draw('x', null, size);
    }
  }
  public SetFontFile(file: string) {
    if (this._auto) {
      prompt(this.id, 'Img.SetFontFile(\f' + file);
    } else {
      this.Draw('f', file);
    }
  }
  public SetLineWidth(width: string) {
    if (this._auto) {
      prompt(this.id, 'Img.SetLineWidth(' + width);
    } else {
      this.Draw('w', null, width);
    }
  }
  public SetPaintColor(clr: string) {
    if (this._auto) {
      prompt(this.id, 'Img.SetPaintColor(' + clr);
    } else {
      this.Draw('n', clr);
    }
  }
  public SetPaintStyle(style: string) {
    if (this._auto) {
      prompt(this.id, 'Img.SetPaintStyle(' + style);
    } else {
      this.Draw('s', style);
    }
  }
  public Rotate(angle: string, pivX: string, pivY: string) {
    prompt(this.id, 'Img.Rotate(' + angle + '\f' + pivX + '\f' + pivY);
  }
  public Move(x: string, y: string) {
    prompt(this.id, 'Img.Move(' + x + '\f' + y);
  }
  public Scale(x: string, y: string) {
    prompt(this.id, 'Img.Scale(' + x + '\f' + y);
  }
  public Skew(x: string, y: string) {
    prompt(this.id, 'Img.Skew(' + x + '\f' + y);
  }
  public Transform(matrix: string) {
    prompt(this.id, 'Img.Transform(\f' + matrix);
  }
  public Reset() {
    prompt(this.id, 'Img.Reset(');
  }
  public Flatten() {
    prompt(this.id, 'Img.Flatten(');
  }
  public Save(fileName: string, quality: string) {
    prompt(this.id, 'Img.Save\f' + fileName + '\f' + quality);
  }
  public DrawFrame(ms: string) {
    prompt(this.id, 'Img.DrawFrame\f' + ms);
  }
  public Draw(
    func: string,
    p1?: string | null | undefined,
    p2?: string | undefined,
    p3?: string | number | undefined,
    p4?: string | number | undefined,
    p5?: string | number | undefined,
    p6?: string | number | undefined,
    p7?: string | undefined
  ) {
    if (this._gfb.length > 2) {
      this._gfb += '\f';
    }
    this._gfb +=
      func +
      '~' +
      p1 +
      '~' +
      p2 +
      '~' +
      p3 +
      '~' +
      p4 +
      '~' +
      p5 +
      '~' +
      p6 +
      '~' +
      p7;
  }
}
